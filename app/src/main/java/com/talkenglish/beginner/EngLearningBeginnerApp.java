package com.talkenglish.beginner;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.talkenglish.beginner.env.DBHelper;
import com.talkenglish.beginner.env.FileProvider;
import com.talkenglish.beginner.model.LessonData;
import com.talkenglish.beginner.util.ConnectionMonitor;
import com.talkenglish.beginner.util.SharedPref;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.CancellationException;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Alex
 */
public class EngLearningBeginnerApp extends MultiDexApplication {

    private final String TAG = "EngLearningBeginnerApp";
    public static EngLearningBeginnerApp singleton = null;

    public static int guideVideoPlayed = 0;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {

        singleton = this;
        final long httpCacheSize = 12 * 1024 * 1024;
        File cacheDir = null;
        if (Build.VERSION.SDK_INT >= 8) cacheDir = getExternalCacheDir();
        if (cacheDir == null) cacheDir = getCacheDir();
        File httpCacheDir = new File(cacheDir, "cache");
        Log.v(TAG, "Cache Install at " + httpCacheDir);

        // use candrew for 4.1 or less
        boolean useCandrews = Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN;
        if (!useCandrews) {
            try {
                Class.forName("android.net.http.HttpResponseCache")
                        .getMethod("install", File.class, long.class)
                        .invoke(null, httpCacheDir, httpCacheSize);
                Log.v(TAG, "android.net.http.HttpResponseCache installed");
            } catch (Throwable httpResponseCacheNotAvailable) {
                Log.v(TAG, "android.net.http.HttpResponseCache not available. Falling back to Candrews");
                useCandrews = true;
            }
        }
        if (useCandrews) {
            try {
                com.integralblue.httpresponsecache.HttpResponseCache.install(httpCacheDir, httpCacheSize);
                Log.v(TAG, "com.integralblue.httpresponsecache.HttpResponseCache installed");
            } catch (Throwable e) {
                Log.v(TAG, "Failed to install HttpResponseCache");
                e.printStackTrace();
            }
        }

        super.onCreate();

        if (Build.VERSION.SDK_INT >= 9) {
            try {
                // Use log instead of crash in case of strict mode violation.
                // HoneyComb enables Network detection by default!
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().penaltyLog().build());
            } catch (Throwable t) {
                // StrictMode not supported
            }
        }

        // HttpURLConnection has bug before Froyo. Disable Polling.
        // cf. android-developers.blogspot.com/2011/09/androids-http-clients.html
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }

        String useragent = System.getProperty("http.agent");
        String name;
        try {
            PackageInfo pkginfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
            name = BuildConfig.APPLICATION_ID + pkginfo.versionName;
        } catch (Throwable e) {
            name = BuildConfig.APPLICATION_ID;
        }
        if (useragent != null) {
            if (useragent.endsWith(")"))
                useragent = useragent.substring(0, useragent.length() - 1) + "; " + name + ")";
            else
                useragent = useragent + " (" + name + ")";
        } else {
            useragent = name + " Android";
        }
        System.setProperty("http.agent", useragent);

        try {
            Class.forName("android.os.AsyncTask");
        } catch (Throwable ignore) {
        }

        Fabric.with(this, new Crashlytics());

        startDownload();
    }

    public static int screenWidth(Activity activity) {
        DisplayMetrics dMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
        float density = dMetrics.density;
        return dMetrics.widthPixels;
    }

    public static int screenHeight(Activity activity) {
        DisplayMetrics dMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
        float density = dMetrics.density;
        return dMetrics.heightPixels;
    }

    private Tracker mTracker = null;

    synchronized public Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.analytics);
        }

        return mTracker;
    }

    public DownloadFileAsync downloader = null;
    public int numOfLessonsPrepared = 2;
    public void preprocess() {
//        DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        db.execSQL("UPDATE Study SET VocabListen11_Image='L05/look1.jpg' WHERE Number=67 AND `Order`=9");
//        db.close();
    }

    public void startDownload() {
        preprocess();
        loadLessonsPrepared();
        if (numOfLessonsPrepared == 90) {
            return;
        }
        if (downloader == null || downloader.isCancelled()) {
            downloader = new DownloadFileAsync(numOfLessonsPrepared + 1);
            if (Build.VERSION.SDK_INT >= 11) {
                downloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                downloader.execute();
            }
        }
    }

    public void loadLessonsPrepared() {
        numOfLessonsPrepared = SharedPref.getInt(getApplicationContext(), "prepared_count", 2);
        SharedPref.putBoolean(getApplicationContext(), "prepared_l01", true);
        SharedPref.putBoolean(getApplicationContext(), "prepared_l02", true);
    }

    public void saveLessonsPrepared() {
        SharedPref.putInt(getApplicationContext(), "prepared_count", numOfLessonsPrepared);
        SharedPref.putBoolean(getApplicationContext(), String.format("prepared_l%02d", numOfLessonsPrepared), true);
    }
    public boolean wasLessonPrepared(int lesson_number) {
        return SharedPref.getBoolean(getApplicationContext(), String.format("prepared_l%02d", lesson_number), false);
    }
    public boolean isDownloaderRunning() {
        if (downloader == null) {
            return false;
        }
        return true;
    }
    public void setDownloadListener(DownloadListener listener) {
        if (downloader != null) {
            downloader.setDownloadListener(listener);
        }
    }

    public static interface DownloadListener {
        void onComplete(int lesson_number);

        void onProgress(int max, int current);

        void onFail(String error);
    }

    class DownloadFileAsync extends AsyncTask<String, Integer, String> {

        private int number;
        private DownloadListener listener = null;

        public void setDownloadListener(DownloadListener listener) {
            this.listener = listener;
        }

        public void removeDownloadListener() {
            this.listener = null;
        }

        public DownloadFileAsync(int number) {
            this.number = number;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listener != null) {
                listener.onProgress(1, 0);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Log.e(TAG, "Downloading Lesson" + this.number + " --- doInBackground...");
                if (!ConnectionMonitor.isNetworkAvailable(getApplicationContext())) {
                    Log.e(TAG, "No internet connection");
                    return "Please check your Internet connection.";
                }
                DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                LessonData data = new LessonData(this.number).load(db);
                String prefix = String.format("l%02d", this.number);
                for (int i = 0; i < data.numOfFiles(); i++) {
                    LessonData.Data item = data.fileAt(i);
                    if (item.getPrefix() != null && item.getPrefix() != prefix) {
                        if (true == SharedPref.getBoolean(getApplicationContext(), "prepared_" + item.getPrefix(), false)) {
                            File file = new File(FileProvider.getPath(getApplicationContext(), item.getFileName()));
                            if (file.exists()) {
                                publishProgress(data.numOfFiles(), i + 1);
                                continue;
                            }
                        }
                    }
                    if (downloadFile(item.getFileUrl(), FileProvider.getPath(getApplicationContext(), item.getFileName()))) {
                        publishProgress(data.numOfFiles(), i + 1);
                    } else {
                        return "Downloading " + prefix + "/" + item.getFileName() + " Failed.";
                    }
                }
            } catch (CancellationException e) {
                Log.e(TAG, "Downloading Lesson" + this.number + " cancelled...");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return "SUCCESS";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e(TAG, "Downloading Lesson" + this.number + " --- onPostExecute");
            if (s == null) {
                if (!isCancelled()) {
                    Log.e(TAG, "Downloading " + this.number + " retry...");
                    downloader = new DownloadFileAsync(this.number);
                    downloader.setDownloadListener(this.listener);
                    if (Build.VERSION.SDK_INT >= 11) {
                        downloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        downloader.execute();
                    }
                    return;
                }
            } else if (s.equals("SUCCESS")) {
                if (listener != null) {
                    listener.onComplete(this.number);
                }
                numOfLessonsPrepared++;
                saveLessonsPrepared();
                if (numOfLessonsPrepared >= 90) {
                    downloader = null;
                } else {
                    final DownloadListener listener = this.listener;
                    downloader = new DownloadFileAsync(numOfLessonsPrepared + 1);
                    downloader.setDownloadListener(listener);
                    if (Build.VERSION.SDK_INT >= 11) {
                        downloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        downloader.execute();
                    }
                }
            } else {
                if (listener != null) {
                    listener.onFail(s);
                }
                downloader = null;
                startDownload();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (listener != null) {
                listener.onProgress(values[0], values[1]);
            }
        }

        private boolean downloadFile(String strUrl, String outputSource) {
            try {

                //if (AudioDownloader.isDownloaded(getApplicationContext(), outputSource)) return true;

                URL url = new URL(strUrl);
                HttpURLConnection connection = null;
                connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(2000);
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage());
                    throw new IOException();
                }

                File f = new File(outputSource);
                InputStream input = connection.getInputStream();
                OutputStream output = new FileOutputStream(f);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        throw new CancellationException();
                    }
                    total += count;
                    //Log.d(TAG, outputSource + ":" + total);
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return true;
            } catch (CancellationException e) {
                Log.e(TAG, "Downloading " + strUrl + " ---> cancelled...");
                return false;
            } catch (Exception e) {

                Log.e(TAG, "Downloading " + strUrl + " ---> cancelled");
                e.printStackTrace();
                return false;
            }
        }
    }
}

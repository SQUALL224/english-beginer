package com.talkenglish.beginner.env;

import android.content.Context;

import java.io.File;

/**
 * Created by alex
 */
public class FileProvider {

    private final String TAG = "FileProvider";


    private static final String DIRECTORY = "bigenner_cache";

    private FileProvider() {
    }

    public static String getDirPath(Context context) {
        File f0 = context.getExternalFilesDir(null);

        String path = f0.getPath() + "/" + DIRECTORY;
        File f = new File(path);
        if (!f.exists()) {
            boolean bf = f.mkdir();
            if (bf) {

            } else {

            }
        }
        return path;
    }

    public static String getPath(Context context, String filename) {
        return getDirPath(context) + "/" + filename.trim();
    }
}

package com.talkenglish.beginner.env;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by alex
 */
public class MultipleAudioPlayer {

    protected static final String TAG = "MultipleAudioPlayer";

    private AudioPlayer currentPlayer;
    private ProgressUpdateListener listener;
    private Activity activity;

    private float duration;
    private float current;
    private ArrayList<AssetFileDescriptor> descriptors;
    private ArrayList<String> paths;
    public int numOfAudios = 0;
    private ArrayList<Float> durations;
    public int currentPos = 0;
    private boolean paused = true;

    public interface ProgressUpdateListener {
        public void onProgress(float progress);
        public void onComplete();
    }

    public static void stop(MultipleAudioPlayer player) {
        if (player != null) {
            player.stop();
        }
    }
    public static boolean start(MultipleAudioPlayer player) {
        if (player != null) {
            return player.start();
        }
        return false;
    }
    public static void pause(MultipleAudioPlayer player) {
        if (player != null) {
            player.pause();
        }
    }
    public static void resume(MultipleAudioPlayer player) {
        if (player != null) {
            player.resume();
        }
    }
    public static void skip(MultipleAudioPlayer player) {
        if (player != null) {
            player.skip();
        }
    }

    public static MultipleAudioPlayer loadWithPaths(ArrayList<String> paths, Activity activity, ProgressUpdateListener listener) {
        MultipleAudioPlayer player = null;
        try {
            player = new MultipleAudioPlayer(activity, listener);
            player.initWithPaths(paths);
            return player;
        } catch (Exception e) {
        }
        return null;
    }
    public static MultipleAudioPlayer loadWithDescriptors(ArrayList<AssetFileDescriptor> fds, Activity activity, ProgressUpdateListener listener) {
        MultipleAudioPlayer player = null;
        try {
            player = new MultipleAudioPlayer(activity, listener);
            player.initWithDescriptors(fds);
            return player;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public MultipleAudioPlayer(Activity activity, ProgressUpdateListener listener) {
        this.activity = activity;
        this.listener = listener;
    }
    public void initWithPaths(ArrayList<String> paths) throws Exception {
        int i = 0;
        try {
            this.paused = true;
            this.paths = paths;
            this.numOfAudios = paths.size();
            MediaPlayer player = new MediaPlayer();
            durations = new ArrayList<>();
            float duration = 0;
            for (i = 0; i < paths.size(); i ++) {
                String path = paths.get(i);
                player.reset();
                player.setDataSource(path);
                player.prepare();
                float dur = (float)player.getDuration() / 1000.0f;
//                if (i < paths.size()-1) {
                dur += 2;
//                }
                durations.add(dur);
                duration += dur;
            }

            this.duration = duration;
        } catch (IOException e) {
            Log.e(TAG, String.valueOf(i) + "-th audio file seems to be invalid");
            this.currentPlayer = null;
            this.listener = null;
            throw e;
        }
    }
    public void initWithDescriptors(ArrayList<AssetFileDescriptor> fds) throws Exception {
        int i = 0;
        try {
            this.paused = true;
            this.descriptors = fds;
            this.numOfAudios = fds.size();
            MediaPlayer player = new MediaPlayer();
            durations = new ArrayList<>();
            float duration = 0;
            for (i = 0; i < this.descriptors.size(); i ++) {
                AssetFileDescriptor fd = descriptors.get(i);
                player.reset();
                long offset = fd.getStartOffset();
                long length = fd.getLength();
                FileDescriptor d = fd.getFileDescriptor();
                player.setDataSource(d, offset, length);
                player.prepare();
                float dur = (float)player.getDuration() / 1000.0f;
//                if (i < descriptors.size()-1) {
                dur += 2;
//                }
                durations.add(dur);
                duration += dur;
            }

            this.duration = duration;
        } catch (IOException e) {
            Log.e(TAG, String.valueOf(i) + "-th audio file seems to be invalid");
            this.currentPlayer = null;
            this.listener = null;
            throw e;
        }
    }
    public int getCurrentPos() {
        return this.currentPos;
    }
    public String getDurationFormat() {
        int minutes = (int)this.duration / 60;
        int secs = (int) this.duration % 60;
        int minutes0 = (int)this.current / 60;
        int secs0 = (int) this.current % 60;
        return String.format("%02d:%02d/%02d:%02d", minutes0, secs0, minutes, secs);
    }
    public boolean canPlay() {
        return (this.currentPlayer != null &&
                ((paths != null && paths.size() > 0) || (descriptors != null && descriptors.size() > 0)));
    }
    public boolean isPlaying() {
        return ((this.currentPlayer != null/* && this.currentPlayer.isPlaying()*/) || this.thread_running);
    }
    public boolean isPaused() {
        return this.paused;
    }
    public boolean start() {
        this.currentPos = 0;
        if (play()) {
            this.paused = false;
            return true;
        } else {
            this.paused = true;
            return false;
        }
    }
    private boolean play() {
        if (this.descriptors != null && this.descriptors.size() > 0) {
            AssetFileDescriptor afd = descriptors.get(currentPos);
            FileDescriptor fd = afd.getFileDescriptor();
            long offset = afd.getStartOffset();
            long length = afd.getLength();
            this.currentPlayer = AudioPlayer.play(fd, offset, length, new AudioProgressUpdateListener());
            return (this.currentPlayer != null);
        } else if (this.paths != null && this.paths.size() > 0) {
            String path = this.paths.get(currentPos);
            this.currentPlayer = AudioPlayer.play(path, new AudioProgressUpdateListener());
            return (this.currentPlayer != null);
        }
        return false;
    }
    public void skip() {
        fire_count = 0;
        if (currentPlayer != null) {
            currentPlayer.stop();
            currentPlayer = null;
        }
        if (this.thread_running) {
            this.thread_running = false;
            this.thread.interrupt();
            this.thread = null;
        }
        if (currentPos < numOfAudios-1) {
            currentPos++;
            if (play()) {
                this.paused = false;
            } else {
                this.paused = true;
            }
        } else {
            current = 0;
            paused = true;
            currentPos = 0;
            if (listener != null) {
                listener.onComplete();
            }
        }
    }
    public void skipTo(int toPos) {
        fire_count = 0;
        if (currentPlayer != null) {
            currentPlayer.stop();
            currentPlayer = null;
        }
        if (this.thread_running) {
            this.thread_running = false;
            this.thread.interrupt();
            this.thread = null;
        }
//        if (currentPos < numOfAudios-1) {
//            currentPos++;
//            if (play()) {
//                this.paused = false;
//            } else {
//                this.paused = true;
//            }
//        } else {
//            current = 0;
//            paused = true;
//            currentPos = 0;
//            if (listener != null) {
//                listener.onComplete();
//            }
//        }
        currentPos = (int)(((float)toPos / 100) * numOfAudios);
        if (play()) {
            this.paused = false;
        } else {
            this.paused = true;
        }
    }
    public void resume() {
        if (currentPlayer != null) {
            currentPlayer.resume();
        }
        this.paused = false;
    }
    public void pause() {
        if (currentPlayer != null) {
            currentPlayer.pause();
        }
        this.paused = true;
    }
    public void stop() {
        this.currentPos = 0;
        this.paused = true;
        if (currentPlayer != null) {
            currentPlayer.stop();
            currentPlayer = null;
        }
        if (this.thread_running) {
            this.thread_running = false;
            this.thread.interrupt();
        }
    }
    public float getProgress() {
        float progress;
        if (duration == 0) {
            progress = 0f;
        } else {
            progress = Math.max(0f, Math.min(1f, current / duration));
        }
        return progress;
    }
    float calcProgress(float curInSec) {
        float duration = this.duration;
        float current = calcCurrentDuration(curInSec);
        float progress;
        if (duration == 0) {
            progress = 0f;
        } else {
            progress = Math.max(0f, Math.min(1f, current / duration));
        }
        return progress;
    }
    float calcCurrentDuration(float cur) {
        this.current = 0;
        for (int i = 0; i < currentPos; i ++) {
            this.current += durations.get(i);
        }
        this.current += cur;
        return this.current;
    }
    private int fire_count = 0;
    private boolean thread_running = false;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            while (thread_running) {
                if (!paused) {
                    Log.e("multiple player", "thread fired --- " + fire_count);
                    float cur = durations.get(currentPos);
                    final float progress = calcProgress(cur-2f+0.2f*fire_count);
                    if (listener != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onProgress(progress);
                            }
                        });
                    }
                    fire_count ++;
                    if (fire_count == 10) {
                        if (currentPos < numOfAudios-1) {
                            currentPos ++;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (play()) {
                                        paused = false;
                                    } else {
                                        paused = true;
                                    }
                                }
                            });
                        } else {
                            if (listener != null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onComplete();
                                    }
                                });
                            }
                        }
                        thread_running = false;
                        thread = null;
                        return;
                    }
                }
                try {
                    Thread.sleep(200L);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
    };
    private Thread thread = null;
    class AudioProgressUpdateListener implements AudioPlayer.ProgressUpdateListener {

        @Override
        public void onProgress(int cur, int dur) {
            float progress = calcProgress(cur/1000.0f);

            if (listener != null) {
                listener.onProgress(progress);
            }
        }
        @Override
        public void onProgress(float progress) {

        }
        @Override
        public void onComplete() {
            float progress = calcProgress(durations.get(currentPos)-2f);

            if (listener != null) {
                listener.onProgress(progress);
            }
            currentPlayer.stop();
            fire_count = 0;
            thread_running = true;
            thread = new Thread(runnable);
            thread.start();
        }
    }
}

package com.talkenglish.beginner.env;

import android.app.Application;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.talkenglish.beginner.EngLearningBeginnerApp;

/**
 * Created by alex ...
 */
public class Analytics {
    public static void sendScreenName(Application application, String screenName) {
        Tracker t = ((EngLearningBeginnerApp)application).getTracker();
        t.setScreenName(screenName);
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public static void sendEvent(Application application, String action, String label) {
        (((EngLearningBeginnerApp)application).getTracker()).send(new HitBuilders.EventBuilder()
                .setCategory("Navigate")
                .setAction(action)
                .setLabel(label)
                .setValue(1)
                .build());
    }
}

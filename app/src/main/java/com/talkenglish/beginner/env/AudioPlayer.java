package com.talkenglish.beginner.env;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by alex
 */
public class AudioPlayer {

    protected static final String TAG = "AudioPlayer";

    private MediaPlayer audioPlayer;
    private Handler handler;
    private AudioProgressHandler audioProgressHandler;
    private ProgressUpdateListener listener;

    public interface ProgressUpdateListener {
        public void onProgress(int current, int duration);
        public void onProgress(float progress);
        public void onComplete();
    }

    private static class AudioProgressHandler extends Handler {
        private final WeakReference<AudioPlayer> player;

        public AudioProgressHandler(AudioPlayer player) {
            this.player = new WeakReference<>(player);
        }
        @Override
        public void handleMessage(Message msg) {
            AudioPlayer player = this.player.get();
            if(player == null) return;
            switch (msg.what) {
                case 0:
                    player.updateAudioProgress();
                    if(player.isPlaying()) {
                        msg = obtainMessage(0);
                        sendMessageDelayed(msg, 250);
                    }
                    break;
            }
        }
    }

    private AudioPlayer(ProgressUpdateListener listener) throws Exception {
        this.handler = new Handler();
        this.audioProgressHandler = new AudioProgressHandler(this);
        this.listener = listener;
    }
    public static void stop(AudioPlayer player) {
        if (player != null) {
            player.stop();
            player = null;
        }
    }
    public static AudioPlayer playEffectSound(Context context, ProgressUpdateListener listener) {
        AssetFileDescriptor descriptor = null;
        try {
            descriptor = context.getAssets().openFd("congratulations.mp3");
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            return AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, listener);

        } catch (Exception e) {
            Log.e(TAG, "Opening asset file failed - congratulations.mp3");
            return null;
        }
    }
    public static AudioPlayer play(String path, ProgressUpdateListener listener) {
        AudioPlayer player = null;
        try {
            player = new AudioPlayer(path, listener);
            if (player.play()) {
                return player;
            }
            player.stop();
        } catch (Exception e) {
        }
        return null;
    }
    public static AudioPlayer play(FileDescriptor fd, long offset, long length, ProgressUpdateListener listener) {
        AudioPlayer player = null;
        try {
            player = new AudioPlayer(fd, offset, length, listener);
            if (player.play()) {
                return player;
            }
            player.stop();
        } catch (Exception e) {
        }
        return null;
    }
    public AudioPlayer(String path, ProgressUpdateListener listener) throws Exception {
        try {
            this.audioPlayer = new MediaPlayer();
            this.audioPlayer.setDataSource(path);
            this.audioPlayer.prepare();
            this.handler = new Handler();
            this.audioProgressHandler = new AudioProgressHandler(this);
            this.listener = listener;
        } catch (IOException e) {
            this.audioPlayer = null;
            this.handler = null;
            this.audioProgressHandler = null;
            this.listener = null;
            throw e;
        }
    }
    public AudioPlayer(FileDescriptor fd, long offset, long length, ProgressUpdateListener listener) throws Exception {
        try {
            this.audioPlayer = new MediaPlayer();
            this.audioPlayer.setDataSource(fd, offset, length);
            this.audioPlayer.prepare();
            this.handler = new Handler();
            this.audioProgressHandler = new AudioProgressHandler(this);
            this.listener = listener;
        } catch (IOException e) {
            this.audioPlayer = null;
            this.handler = null;
            this.audioProgressHandler = null;
            this.listener = null;
            throw e;
        }
    }
    public boolean isPlaying() {
        return (this.audioPlayer != null && this.audioPlayer.isPlaying());
    }
    public boolean canPlay() {
        return (this.audioPlayer != null);
    }
    public boolean play() {

        if(this.audioPlayer != null && this.handler != null) {
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG, "play finished");
                            stop();
                            if (listener != null) {
                                listener.onComplete();
                            }
                        }
                    });
                }
            });
            audioProgressHandler.sendEmptyMessage(0);
            audioPlayer.start();
            return true;
        }
        return false;
    }
    public void stop() {
        stopAudio();
    }
    private void playAudio(FileDescriptor fd, int offset, int length) {
        stopAudio();

        audioPlayer = new MediaPlayer();
        try {
            audioPlayer.setDataSource(fd, offset, length);
            audioPlayer.prepare();
        } catch (IOException e) {
            audioPlayer = null;
        }

        if(audioPlayer != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e(TAG, "play finished");
                                    rewindAudio();
                                }
                            });
                        }
                    });
                    audioProgressHandler.sendEmptyMessage(0);
                    audioPlayer.start();
                }
            });
        }
    }
    private void playAudio(String path) {
        stopAudio();

        audioPlayer = new MediaPlayer();
        try {
            audioPlayer.setDataSource(path);
            audioPlayer.prepare();
        } catch (IOException e) {
            audioPlayer = null;
        }

        if(audioPlayer != null) {
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG, "play finished");
                            rewindAudio();
                        }
                    });
                }
            });
            audioProgressHandler.sendEmptyMessage(0);
            audioPlayer.start();
        }
    }
    private void updateAudioProgress() {
        if(audioPlayer == null) {
            if (listener != null) {
                listener.onProgress(0);
            }
        } else {
            int duration = audioPlayer.getDuration();
            int current = audioPlayer.getCurrentPosition();

            if (listener != null) {
                listener.onProgress(current, duration);
            }
        }
    }
    public void pause() {
        this.pauseAudio(true);
    }
    public void resume() {
        this.resumeAudio();
    }
    private void pauseAudio(boolean update) {
        if(audioPlayer != null && audioProgressHandler != null) {
            audioProgressHandler.removeMessages(0);
            audioPlayer.pause();
        }
    }
    private void resumeAudio() {
        if(audioPlayer != null && !audioPlayer.isPlaying()) {
            audioProgressHandler.sendEmptyMessage(0);
            audioPlayer.start();
        }
    }
    private void seekAudio(float progress) {
        if(audioPlayer != null) {
            int duration = audioPlayer.getDuration();
            if(duration == 0) return;
            int position = (int)(duration * progress);
            audioPlayer.seekTo(position);
            if(!audioPlayer.isPlaying()) {
                resumeAudio();
            } else {
                updateAudioProgress();
            }
        }
    }
    private void rewindAudio() {
        if(audioPlayer != null) {
            audioPlayer.seekTo(0);
            audioPlayer.pause();
            audioProgressHandler.removeMessages(0);
            updateAudioProgress();
        }
    }
    private void stopAudio() {
        if (audioPlayer != null) {
            audioProgressHandler.removeMessages(0);
            audioPlayer.stop();
            audioPlayer.release();
            audioPlayer = null;
        }
    }
}

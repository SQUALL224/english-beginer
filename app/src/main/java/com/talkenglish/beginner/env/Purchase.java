package com.talkenglish.beginner.env;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;

import com.talkenglish.beginner.R;

/**
 * Created by alex on 6/14/2016.
 */
public class Purchase {

    private static final String PURCHASE_COUNTDOWN_KEY = "purchase_countdown";
    private static final int INITIAL_PURCHASE_COUNTDOWN = 30;
    private static final int PURCHASE_COUNTDOWN = 20;

    public static void checkPurchase(Application application, Context context, Intent intent) {
        final Context finalContext = context;
        final Application finalApplication = application;
        final Intent finalIntent = intent;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        int countdown = preferences.getInt(PURCHASE_COUNTDOWN_KEY, -1);
        if(countdown < 0) {
            preferences.edit().putInt(PURCHASE_COUNTDOWN_KEY, INITIAL_PURCHASE_COUNTDOWN).apply();
        }
        else  if(countdown == 1) {
            Analytics.sendEvent(finalApplication, "Show Purchase Dialog", "");
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.purchase_app_01)
                    .setPositiveButton(R.string.purchase_app_02, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            preferences.edit().putInt(PURCHASE_COUNTDOWN_KEY, 0).apply();

                            Analytics.sendEvent(finalApplication, "Purchase App", "Yes");

                            finalContext.startActivity(finalIntent);
                        }
                    })
                    .setNeutralButton(R.string.purchase_app_04, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Analytics.sendEvent(finalApplication, "Purchase App", "Later");
                            preferences.edit().putInt(PURCHASE_COUNTDOWN_KEY, PURCHASE_COUNTDOWN).apply();
                        }
                    })
                    .setNegativeButton(R.string.purchase_app_03, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Analytics.sendEvent(finalApplication, "Purchase App", "No");
                            preferences.edit().putInt(PURCHASE_COUNTDOWN_KEY, 0).apply();

                        }
                    })
                    .show();
            preferences.edit().putInt(PURCHASE_COUNTDOWN_KEY, PURCHASE_COUNTDOWN).apply();
        }
        else if(countdown > 0){
            preferences.edit().putInt(PURCHASE_COUNTDOWN_KEY, countdown - 1).apply();
        }
    }
}

package com.talkenglish.beginner.env;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import com.talkenglish.beginner.BuildConfig;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.util.SharedPref;

/**
 * Created by kangung on 6/14/2016.
 */
public class RateApp {
    private static final String RATE_COUNTDOWN_KEY = "rate_countdown";
    private static final int INITIAL_RATE_COUNTDOWN = 20;
    private static final int RATE_COUNTDOWN = 20;

    public static void checkRate(final Application application0, final Context context0) {
        final Application application = application0;
        final Context context = context0;

        int countdown = SharedPref.getInt(context, RATE_COUNTDOWN_KEY, -1);
        if(countdown < 0) {
            SharedPref.putInt(context, RATE_COUNTDOWN_KEY, INITIAL_RATE_COUNTDOWN);
        }
        else  if(countdown == 1) {
            Analytics.sendEvent(application, "Show Rate Dialog", "");

//            ContextThemeWrapper themedContext;
//            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
//                themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
//            }
//            else {
//                themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Light_NoTitleBar);
//            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.rate_app_01)
                    .setPositiveButton(R.string.rate_app_02, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPref.putInt(context, RATE_COUNTDOWN_KEY, 0);

                            Analytics.sendEvent(application, "Rate App", "Yes");

                            final String appPackageName = BuildConfig.APPLICATION_ID;
                            try {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    })
                    .setNeutralButton(R.string.rate_app_04, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Analytics.sendEvent(application, "Rate App", "Later");
                            SharedPref.putInt(context, RATE_COUNTDOWN_KEY, RATE_COUNTDOWN);
                        }
                    })
                    .setNegativeButton(R.string.rate_app_03, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Analytics.sendEvent(application, "Rate App", "No");
                            SharedPref.putInt(context, RATE_COUNTDOWN_KEY, 0);

                        }
                    })
                    .show();
            SharedPref.putInt(context, RATE_COUNTDOWN_KEY, RATE_COUNTDOWN);
        } else if(countdown > 0){
            SharedPref.putInt(context, RATE_COUNTDOWN_KEY, countdown - 1);
        }
    }
}

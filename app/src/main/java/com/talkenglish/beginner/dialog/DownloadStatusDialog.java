package com.talkenglish.beginner.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.widget.ProgressIndicator;


/**
 * Created by alex on 4/20/2016.
 */
public class DownloadStatusDialog extends Dialog {

    private ProgressIndicator downloadProgress;
    public DownloadStatusDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_download_status);

        this.setCancelable(false);
        downloadProgress = (ProgressIndicator) findViewById(R.id.pi_download_status);
    }

    public void setProgress(float progress) {
        downloadProgress.setProgress(progress);
    }
    public static DownloadStatusDialog showDialog(Context context) {
        DownloadStatusDialog dialog = new DownloadStatusDialog(context);

        if (EngLearningBeginnerApp.singleton != null) {
            Analytics.sendScreenName(EngLearningBeginnerApp.singleton, "Recommended App Selected");
        }
        dialog.show();
        return dialog;
    }

    public static void hideDialog(DownloadStatusDialog dialog) {
        if (dialog == null)
            return;
        dialog.dismiss();
    }
}

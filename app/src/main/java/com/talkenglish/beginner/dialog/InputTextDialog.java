package com.talkenglish.beginner.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.util.FontCache;


/**
 * Created by alex on 4/20/2016.
 */
public class InputTextDialog extends Dialog {

    private static InputTextDialog dialog = null;
    Context context;
    EditText editText;
    String text;
    OnDoneClickListener listener;

    public InputTextDialog(Context context, String text, OnDoneClickListener listener) {
        super(context);
        this.context = context;
        this.text = text;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_input_text);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        editText = (EditText) findViewById(R.id.et_text);
        editText.setTypeface(FontCache.getTypefaceNotoSans_Regular(this.context));
        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (listener != null) {
                    listener.onDone(editText.getText().toString());
                }
            }
        });
        this.setCancelable(true);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                } catch (Exception e) {}
            }
        });
    }

    public static InputTextDialog show(Context context, String text, OnDoneClickListener listener) {
        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = new InputTextDialog(context, text, listener);

        dialog.show();
        return dialog;
    }

    public static void hideDialog() {
        if (dialog == null)
            return;
        dialog.dismiss();
    }
    public interface OnDoneClickListener {
        void onDone(String text);
    }
}

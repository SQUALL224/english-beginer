package com.talkenglish.beginner.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.talkenglish.beginner.activity.PurchaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * Created by alex ...
 */
public class PurchaseInfo {

    private static final String TAG = "PurchaseInfo";

    public static String SKU_PREMIUM = "beginner.premium.publish";
    public static String SKU_PREMIUM1 = "beginner.premium_plus1.publish3";
    public static String SKU_PREMIUM3 = "beginner.premium_plus3.publish";

    final private static String[] SKU_REMOVE_AD = {
            SKU_PREMIUM,
            SKU_PREMIUM1,
            SKU_PREMIUM3
    };

    public interface OnPurchaseInfoListener {
        void onPurchaseInfoRetrieved(Hashtable<String, PurchaseInfo> purchaseInfos);
        void onPurchaseInfoFailed();
    }

    private static Hashtable<String, PurchaseInfo> sCache;

    private static void updateCache(Hashtable<String, PurchaseInfo> purchaseInfos) {
        sCache = purchaseInfos;
    }

    public static int doRemoveAd(Hashtable<String, PurchaseInfo> purchaseInfos) {
        if (purchaseInfos == null) return 0;
        int removeAd = 0;
        for (PurchaseInfo i : purchaseInfos.values()) {
            removeAd = ((removeAd << 1) | (i.purchased == true ? 1 : 0));
        }
        return removeAd;
    }

    public static int doRemoveAd() {
        return doRemoveAd(sCache);
    }

    public static Hashtable<String, PurchaseInfo> retrievePurchaseInfoList(final Context context, final IInAppBillingService service, final OnPurchaseInfoListener listener) {
        if(service == null) {
            listener.onPurchaseInfoFailed();
            return null;
        }

        AsyncTask<Void, Void, Hashtable<String, PurchaseInfo>> task = new AsyncTask<Void, Void, Hashtable<String, PurchaseInfo>>() {
            @Override
            protected Hashtable<String, PurchaseInfo> doInBackground(Void... params) {
                ArrayList<String> skuList = new ArrayList<> ();
                for(String sku : SKU_REMOVE_AD) skuList.add(sku);
                Bundle querySkus = new Bundle();
                querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

                try {
                    Bundle skuDetails = service.getSkuDetails(3, context.getPackageName(), "inapp", querySkus);
                    int response = skuDetails.getInt("RESPONSE_CODE");
                    Log.v(TAG, "getSkuDetails returns: " + response);
                    if (response != 0) {

                        return null;
                    }

                    ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
                    if(responseList == null) return null;

                    HashSet<String> purchasedSkus = getPurchasedSkus(context, service);
                    Log.v(TAG, "purchased skus: " + purchasedSkus);

                    Hashtable<String, PurchaseInfo> cache = new Hashtable<>();

                    for (String thisResponse : responseList) {
                        Log.v(TAG, "getSkuDetails: " + thisResponse);

                        JSONObject object = new JSONObject(thisResponse);
                        String sku = object.getString("productId");
                        {
                            PurchaseInfo info = new PurchaseInfo();
                            info.purchased = purchasedSkus != null && purchasedSkus.contains(sku);
                            if (sku.equals(SKU_PREMIUM1)) {
                                if (info.purchased) {
                                    final String token = object.getString("purchaseToken");
                                    service.consumePurchase(3, context.getPackageName(), token);
                                }
                            }
                            info.priceString = object.getString("price");
                            Currency currency = Currency.getInstance(object.getString("price_currency_code"));
                            info.currencySymbol = currency.getSymbol();
                            info.priceValue = ((double) object.getLong("price_amount_micros")) / 1000000.0;
                            cache.put(sku, info);

                        }
                    }

                    Log.v(TAG, "purchase info: " + cache);

                    return cache;
                } catch (RemoteException | JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Hashtable<String, PurchaseInfo> cache) {
                if(cache != null) {
                    updateCache(cache);
                    listener.onPurchaseInfoRetrieved(cache);
                }
                else {
                    listener.onPurchaseInfoFailed();
                }
            }
        };
        task.execute();

        return sCache;
    }

    private static HashSet<String> getPurchasedSkus(final Context context, final IInAppBillingService service) throws RemoteException {
        Bundle ownedItems = service.getPurchases(3, context.getPackageName(), "inapp", null);

        int response = ownedItems.getInt("RESPONSE_CODE");
        if (response == 0) {
            ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
            if(ownedSkus == null) return null;

            return new HashSet<>(ownedSkus);
        }
        else {
            Log.v(TAG, "getPurchases returns: " + response);
        }
        return null;
    }

    final private static int REQUEST_CODE_PURCHASE = 12315;

    public static void purchase(Activity activity, IInAppBillingService service, String sku) {
        if(service == null) return;

        String piece1 = "GRGBDFFBGGGFG3DFUISDFVWE";
        String piece2 = "$#VADVNAJ8bjFFASDFN";
        String piece3 = "DFBFN4F8FFDFFPAAF";
        String piece4 = "AFVAD0FEEFPFAFVdRFEF";

        try {
            Bundle buyIntentBundle = service.getBuyIntent(3, activity.getPackageName(),
                    sku, "inapp", piece1 + piece2 + piece3 + piece4);
            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
            activity.startIntentSenderForResult(pendingIntent.getIntentSender(), REQUEST_CODE_PURCHASE, new Intent(), 0, 0, 0);
        } catch (Throwable e) {
        }
    }

    public static boolean onActivityResult(Activity activity, int requestCode, int resultCode, Intent data, final IInAppBillingService service) {
        if(requestCode == REQUEST_CODE_PURCHASE) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == Activity.RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    String payload = jo.getString("developerPayload");
                    final String packagetName = jo.getString("packageName");
                    boolean purchased = jo.getInt("purchaseState") == 0;
                    final String token = jo.getString("purchaseToken");

                    String piece1 = "GRGBDFFBGGGFG3DFUISDFVWE";
                    String piece2 = "$#VADVNAJ8bjFFASDFN";
                    String piece3 = "DFBFN4F8FFDFFPAAF";
                    String piece4 = "AFVAD0FEEFPFAFVdRFEF";

                    if((piece1 + piece2 + piece3 + piece4).equals(payload) &&
                            activity.getPackageName().equals(packagetName) &&
                            purchased) {
                        Log.v(TAG, "onActivityResult: " + "purchased");
                        if(sCache != null) {
                            PurchaseInfo p = sCache.get(sku);
                            p.purchased = true;
                            if (sku.equals(SKU_PREMIUM1)) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            service.consumePurchase(3, packagetName, token);
                                        } catch (RemoteException e) {

                                        }
                                    }
                                }).start();
                            }

                            Log.v(TAG, "onActivityResult: " + sCache);
                            if (activity instanceof PurchaseActivity) {
                                ((PurchaseActivity)activity).onPurchaseInfoRetrieved(sCache);
                            }
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                Log.v(TAG, "onActivityResult: " + resultCode);
            }

            return true;
        }
        else {
            return false;
        }
    }

    public String priceString;
    public boolean purchased;
    public String currencySymbol;
    public double priceValue;

    @Override
    public String toString() {
        return "[" + priceString + ", " + purchased + "]";
    }

    @Override
    public boolean equals(Object o) {
        if(this != o) return false;
        if(!o.getClass().equals(PurchaseInfo.class)) return false;

        PurchaseInfo p = (PurchaseInfo)o;
        return (this.priceString != null && this.priceString.equals(p.priceString) || this.priceString == p.priceString) &&
                this.purchased == p.purchased &&
                (this.currencySymbol != null && this.currencySymbol.equals(p.currencySymbol) || this.currencySymbol == p.currencySymbol) &&
                this.priceValue == p.priceValue;
    }
}

package com.talkenglish.beginner.util;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;

/**
 * Created by inkyfox on 15. 12. 27..
 */
public class SpannableUtils {

    public static Spannable getSpannableStringArg(String str, CharSequence arg, Object... spans) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(str);
        return replaceSpannable(ssb, arg, spans);
    }

    public static Spannable getSpannableStringArg(String str, int index, CharSequence arg, Object... spans) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(str);
        return replaceSpannable(ssb, index, arg, spans);
    }

    public static SpannableStringBuilder replaceSpannable(String str, CharSequence arg, Object... spans) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(str);
        return replaceSpannable(ssb, arg, spans);
    }

    public static SpannableStringBuilder replaceSpannable(String str, int index, CharSequence arg, Object... spans) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(str);
        return replaceSpannable(ssb, index, arg, spans);
    }

    public static SpannableStringBuilder replaceSpannable(SpannableStringBuilder ssb, CharSequence arg, Object... spans) {
        int p = TextUtils.indexOf(ssb, "%s");
        if (p >= 0) {
            ssb.replace(p, p+2, arg);
            for (int i = 0; i < spans.length; ++i)
                ssb.setSpan(spans[i], p, p + arg.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return ssb;
    }

    public static SpannableStringBuilder replaceSpannable(SpannableStringBuilder ssb, int index, CharSequence arg, Object... spans) {
        String format = "%" + index + "$s";
        int p = TextUtils.indexOf(ssb, format);
        if (p >= 0) {
            ssb.replace(p, p + format.length(), arg);
            for (int i = 0; i < spans.length; ++i)
                ssb.setSpan(spans[i], p, p + arg.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return ssb;
    }

    public static SpannableStringBuilder replaceSpannable(SpannableStringBuilder ssb, String format, CharSequence arg, Object... spans) {
        int p = TextUtils.indexOf(ssb, format);
        if (p >= 0) {
            ssb.replace(p, p + format.length(), arg);
            for (int i = 0; i < spans.length; ++i)
                ssb.setSpan(spans[i], p, p + arg.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return ssb;
    }


    public static SpannableStringBuilder appendSpannable(SpannableStringBuilder ssb, CharSequence str, Object... spans) {
        int p = ssb.length();
        ssb.append(str);
        for (int i = 0; i < spans.length; ++i)
            ssb.setSpan(spans[i], p, p + str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ssb;
    }

}

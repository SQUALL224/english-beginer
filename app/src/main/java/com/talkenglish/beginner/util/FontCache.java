package com.talkenglish.beginner.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by alex on 7/4/2016.
 */
public class FontCache {

    private static Typeface typefaceNotoSans_Bold = null;
    private static Typeface typefaceNotoSans_BoldItalic = null;
    private static Typeface typefaceNotoSans_Regular = null;
    private static Typeface typefaceNotoSans_Italic = null;

    public static Typeface getTypefaceNotoSans_Bold(Context context) {
        if (typefaceNotoSans_Bold == null) {
            typefaceNotoSans_Bold = Typeface.createFromAsset(context.getAssets(), "NotoSans-Bold.ttf");
        }
        return typefaceNotoSans_Bold;
    }
    public static Typeface getTypefaceNotoSans_BoldItalic(Context context) {
        if (typefaceNotoSans_BoldItalic == null) {
            typefaceNotoSans_BoldItalic = Typeface.createFromAsset(context.getAssets(), "NotoSans-BoldItalic.ttf");
        }
        return typefaceNotoSans_BoldItalic;
    }
    public static Typeface getTypefaceNotoSans_Regular(Context context) {
        if (typefaceNotoSans_Regular == null) {
            typefaceNotoSans_Regular = Typeface.createFromAsset(context.getAssets(), "NotoSans-Regular.ttf");
        }
        return typefaceNotoSans_Regular;
    }
    public static Typeface getTypefaceNotoSans_Italic(Context context) {
        if (typefaceNotoSans_Italic == null) {
            typefaceNotoSans_Italic = Typeface.createFromAsset(context.getAssets(), "NotoSans-Italic.ttf");
        }
        return typefaceNotoSans_Italic;
    }
}

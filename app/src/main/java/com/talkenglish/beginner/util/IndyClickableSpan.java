package com.talkenglish.beginner.util;

import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created by inkyfox on 15. 12. 27..
 */
public class IndyClickableSpan extends android.text.style.ClickableSpan {
    private boolean mIsPressed;
    private int mPressedBackgroundColor = 0;
    private int mNormalTextColor = 0;
    private int mPressedTextColor = 0;
    private View.OnClickListener mOnClickListener;

    public IndyClickableSpan(int normalTextColor, int pressedTextColor, int pressedBackgroundColor) {
        this(normalTextColor, pressedTextColor, pressedBackgroundColor, null);
    }

    public IndyClickableSpan(int normalTextColor, int pressedTextColor, int pressedBackgroundColor, View.OnClickListener onClickListener) {
        mNormalTextColor = normalTextColor;
        mPressedTextColor = pressedTextColor;
        mPressedBackgroundColor = pressedBackgroundColor;
        mOnClickListener = onClickListener;
    }

    public void setPressed(boolean isSelected) {
        mIsPressed = isSelected;
    }

    @Override
    public void onClick(View v) {
        if(mOnClickListener != null) mOnClickListener.onClick(v);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        if(mIsPressed) {
            ds.setColor(mPressedTextColor);
            ds.bgColor = mPressedBackgroundColor;
        }
        else {
            ds.setColor(mNormalTextColor);
            ds.bgColor = 0;
        }
        ds.setUnderlineText(false);
    }

    public static void useClickableSpan(TextView tv) {
        tv.setMovementMethod(new ClickableLinkMovementMethod());
    }

    private static class ClickableLinkMovementMethod extends LinkMovementMethod {
        private IndyClickableSpan mPressedSpan;

        @Override
        public boolean onTouchEvent(TextView textView, Spannable spannable, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mPressedSpan = getPressedSpan(textView, spannable, event);
                if (mPressedSpan != null) {
                    mPressedSpan.setPressed(true);
                    Selection.setSelection(spannable, spannable.getSpanStart(mPressedSpan),
                            spannable.getSpanEnd(mPressedSpan));
                }
            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                IndyClickableSpan touchedSpan = getPressedSpan(textView, spannable, event);
                if (mPressedSpan != null && touchedSpan != mPressedSpan) {
                    mPressedSpan.setPressed(false);
                    mPressedSpan = null;
                    Selection.removeSelection(spannable);
                }
            } else {
                if (mPressedSpan != null) {
                    mPressedSpan.setPressed(false);
                    super.onTouchEvent(textView, spannable, event);
                }
                mPressedSpan = null;
                Selection.removeSelection(spannable);
            }
            return true;
        }

        private IndyClickableSpan getPressedSpan(TextView textView, Spannable spannable, MotionEvent event) {

            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= textView.getTotalPaddingLeft();
            y -= textView.getTotalPaddingTop();

            x += textView.getScrollX();
            y += textView.getScrollY();

            Layout layout = textView.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            IndyClickableSpan[] link = spannable.getSpans(off, off, IndyClickableSpan.class);
            IndyClickableSpan touchedSpan = null;
            if (link.length > 0) {
                touchedSpan = link[0];
            }
            return touchedSpan;
        }

    }
}
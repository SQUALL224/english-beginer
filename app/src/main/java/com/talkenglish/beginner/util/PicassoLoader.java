package com.talkenglish.beginner.util;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class PicassoLoader {

	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error, int width, int height)
	{
		loadImage(context, iv, url, placeholder, error, false, width, height, 0 );
	}
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error, boolean centerCrop)
	{
		loadImage(context, iv, url, placeholder, error, centerCrop, -1, -1, 0 );
	}
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder, int error)
	{
		loadImage(context, iv, url, placeholder, error, false, -1, -1, 0 );
	}
	
	public static void loadImage(Context context, ImageView iv, String url, int placeholder)
	{
		loadImage(context, iv, url, placeholder, placeholder, false, -1, -1, 0 );
	}
	
	public static void loadImage(Context context, ImageView iv, String url)
	{
		loadImage(context, iv, url, -1, -1, false, -1, -1, 0 );
	}
	
	public static void loadImage(Context context, ImageView iv, String url, float degrees)
	{
		loadImage(context, iv, url, -1, -1, false, -1, -1, degrees );
	}
	
	
	
	public static void loadImage(Context context, ImageView iv, String url,
			int placeholder, int error, boolean centerCrop, int width, int height
			, float degrees)
	{
		if ( TextUtils.isEmpty(url) )
		{
//			if ( error > 0 )
//				iv.setImageResource(error);
//			return;
			url = "http://default";
		}
		final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
		String urlEncoded = Uri.encode(url, ALLOWED_URI_CHARS);
		RequestCreator creator = Picasso.with(context).load(urlEncoded);
		
		if ( placeholder > 0 )
			creator.placeholder(placeholder);
		
		if ( error > 0 )
			creator.error(error);
		
		if ( centerCrop )
			creator.centerCrop();
		
		if ( width > 0 )
			creator.resize(width, height);
		
		creator.rotate(degrees);
		
//		creator.into(new ImageTarget(iv));
		creator.into(iv);
	}

}

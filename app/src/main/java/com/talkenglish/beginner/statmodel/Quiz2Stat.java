package com.talkenglish.beginner.statmodel;

/**
 * Created by alex on 7/22/2016.
 */
public class Quiz2Stat {
    public int currentPos;
    public boolean busy;

    public Quiz2Stat() {
        init(0);
    }
    private void init(int pos) {
        currentPos = pos;
        busy = false;
    }
    public void next() {
        init(currentPos+1);
    }
}

package com.talkenglish.beginner.statmodel;

/**
 * Created by alex on 7/22/2016.
 */
public class ExerciseStat {
    public int currentPos;
    public int selected;
    public boolean busy;
    public boolean correct;
    public boolean completed;

    public ExerciseStat() {
        init(0);
    }
    private void init(int pos) {
        currentPos = pos;
        selected = -1;
        busy = false;
        correct = false;
        completed = false;
    }
    public void next() {
        init(currentPos+1);
    }
}

package com.talkenglish.beginner.statmodel;

/**
 * Created by alex on 7/22/2016.
 */
public class ListeningStat {
    public boolean isAudioLoaded;
    public boolean paused;
    public String durationLabel;
    public int currentPos;
    public float progress;
    public boolean completed;
    public ListeningStat() {
        isAudioLoaded = false;
        paused = true;
        durationLabel = "--:--/--:--";
        currentPos = 0;
        progress = 0;
        completed = false;
    }
}

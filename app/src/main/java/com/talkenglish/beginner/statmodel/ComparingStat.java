package com.talkenglish.beginner.statmodel;

/**
 * Created by alex on 7/22/2016.
 */
public class ComparingStat {
    public static final int NOTHING = 0x00;
    public static final int LISTENING = 1;
    public static final int RECORDING = 2;
    public static final int RECORDED = 3;
    public static final int COMPARING = 4;
    public int currentPos;
    public int status;
    public float listenProgress;
    public float compareProgress;
    public int compareCount;
    public boolean completed;

    public ComparingStat() {
        init(0);
        completed = false;
    }
    public void init(int pos) {
        currentPos = pos;
        status = NOTHING;
        listenProgress = 0;
        compareProgress = 0;
        compareCount = 0;
    }

    public void next() {
        init(currentPos+1);
    }
}

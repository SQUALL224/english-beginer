package com.talkenglish.beginner.ad;

import android.content.Context;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.talkenglish.beginner.R;


/**
 * Created by alex
 */
public class InterstitialAdController {

    private static final long MIN_INTERVAL = 180 * 1000;
    private static final long MIN_RETRY_INTERVAL = 360 * 1000;

    private Context mContext;
    private InterstitialAd mInterstitialAd;

    private static long mLastTimeAdShown = 0;
    private static long mLastTimeLoadTried = 0;

    public InterstitialAdController(Context context) {
        mContext = context;
        init();
    }

    private void init() {
        // Create the InterstitialAd and set the adUnitId.
        mInterstitialAd = new InterstitialAd(mContext);
        // Defined in values/strings.xml
        mInterstitialAd.setAdUnitId(mContext.getString(R.string.interstitial_ad_unit_id));

        loadAd();
    }

    private void loadAd() {
        if (mInterstitialAd == null) {
            return;
        }
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        mInterstitialAd.loadAd(adRequestBuilder.build());
        mLastTimeLoadTried = System.currentTimeMillis();
    }

    public void show(final Runnable runnable) {
        long timestamp = System.currentTimeMillis();

        if (mInterstitialAd != null && mInterstitialAd.isLoaded() && timestamp > mLastTimeAdShown + MIN_INTERVAL) {
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {

                    int k = 0;
                }

                @Override
                public void onAdClosed() {
                    if(runnable != null) runnable.run();
                    loadAd();
                }
            });
            mInterstitialAd.show();
            mLastTimeAdShown = timestamp;
        }
        else {
            if(runnable != null) runnable.run();

            if(timestamp > mLastTimeLoadTried + MIN_RETRY_INTERVAL) {
                loadAd();
            }
        }
    }

}

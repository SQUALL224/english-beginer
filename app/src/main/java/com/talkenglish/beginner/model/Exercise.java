package com.talkenglish.beginner.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;

/**
 * Created by alex on 7/20/2016.
 */
public class Exercise extends Study {
    protected static final String TAG = "Exercise";

    public Exercise() {
        super();
    }

    public static Exercise newInstance(Context context, int session, Cursor cursor) {

        String p = String.format("Exercise%02x_", session);
        Exercise listen = new Exercise();
        listen.setOrder(cursor.getInt(cursor.getColumnIndex("Order")));
        listen.setImage(cursor.getString(cursor.getColumnIndex(p + "Image")));
        listen.setText(cursor.getString(cursor.getColumnIndex(p + "Word")));
        listen.setAudio(cursor.getString(cursor.getColumnIndex(p + "Audio")));

        return listen;
    }

    public static ArrayList<Exercise> load(Context context, SQLiteDatabase db, int number, int session) {
        ArrayList<Exercise> list = new ArrayList<>();
        {
            String p = String.format("Exercise%02x_", session);
            Cursor cursor = db.rawQuery(
                    "SELECT `Order`, " + p + "Image, " + p + "Word, " + p + "Audio" +
                            " FROM study WHERE Number=" + String.valueOf(number) +
                            " AND " + p + "Audio not null AND " + p + "Image not null AND " + p + "Word not null;",
                    null);

            if (cursor.moveToFirst()) {
                do {
                    list.add(Exercise.newInstance(context, session, cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    protected Exercise(Parcel in) {
        super(in);
    }

    public static final Creator<Exercise> CREATOR = new Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel source) {
            return new Exercise(source);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };
}

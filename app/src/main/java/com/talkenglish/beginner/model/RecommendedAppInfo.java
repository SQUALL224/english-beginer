package com.talkenglish.beginner.model;

import android.content.Context;

import com.talkenglish.beginner.R;

/**
 * Created by alex
 */
public class RecommendedAppInfo {

    private int appIcon;
    private String appTitle;
    private String appDescription;
    private String appPackage;

    public static RecommendedAppInfo[] getRecommendedApps(Context context) {
        return new RecommendedAppInfo[]{
                new RecommendedAppInfo(R.drawable.recommend_app1,
                        context.getString(R.string.recommend_app_title_01),
                        context.getString(R.string.recommend_app_description_01),
                        "talkenglish.com.standard"),
                new RecommendedAppInfo(R.drawable.recommend_app2,
                        context.getString(R.string.recommend_app_title_02),
                        context.getString(R.string.recommend_app_description_02),
                        "com.talkenglish.conversation"),
                new RecommendedAppInfo(R.drawable.recommend_app3,
                        context.getString(R.string.recommend_app_title_03),
                        context.getString(R.string.recommend_app_description_03),
                        "com.talkenglish.englishvocab"),
                new RecommendedAppInfo(R.drawable.recommend_app4,
                        context.getString(R.string.recommend_app_title_04),
                        context.getString(R.string.recommend_app_description_04),
                        "com.talkenglish.listening"),
                new RecommendedAppInfo(R.drawable.recommend_app5,
                        context.getString(R.string.recommend_app_title_05),
                        context.getString(R.string.recommend_app_description_05),
                        "com.talkenglish.player")

        };
    }

    public RecommendedAppInfo(int appIcon, String appTitle, String appDescription, String appPackage) {
        this.appIcon = appIcon;
        this.appTitle = appTitle;
        this.appDescription = appDescription;
        this.appPackage = appPackage;
    }
    public int getAppIcon() {
        return this.appIcon;
    }
    public String getAppTitle() {
        return this.appTitle;
    }
    public String getAppDescription() {
        return this.appDescription;
    }
    public String getAppPackage() {
        return this.appPackage;
    }

}

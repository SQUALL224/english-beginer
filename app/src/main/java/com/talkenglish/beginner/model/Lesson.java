package com.talkenglish.beginner.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.talkenglish.beginner.env.DBHelper;
import com.talkenglish.beginner.env.FileProvider;
import com.talkenglish.beginner.util.SharedPref;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by alex...
 */
public class Lesson implements Parcelable {

    protected static final String TAG = "Lesson";
    public static final int STUDY_SESSION1 = 0x10;
    public static final int STUDY_SESSION2 = 0x20;
    public static final int QUIZ = 0x30;
    public static final int FINAL_CHECK = 0x40;

    private static int[] indices_10;
    private static int[] indices_11;
    private static int[] indices_20;
    private static int[] indices_30;
    private static int[] indices_31;
    private static int[] indices_32;

    private int level;
    private int number;
    private String title;
    private String mainImage;
    private String mainText;
    private String mainAudio;
    private Lesson next;

    private int section;
    private int listened_count = 0;
    private boolean expanded = false;
    private boolean completed = false;

    private ArrayList<Study> listens_10 = null;
    private ArrayList<Study> listens_11 = null;
    private ArrayList<Study> listens_20 = null;
    private ArrayList<Study> listens_21 = null;
    private ArrayList<Study> compares_10 = null;
    private ArrayList<Study> compares_11 = null;
    private ArrayList<Study> compares_20 = null;
    private ArrayList<Study> compares_21 = null;
    private ArrayList<Exercise> exercises_10 = null;
    private ArrayList<Exercise> exercises_11 = null;
    private ArrayList<Exercise> exercises_20 = null;
    private ArrayList<Quiz> quizzes1 = null;
    private ArrayList<Quiz> quizzes2 = null;
    private Score score;

    public Lesson() {
    }

    private void shuffleArray(int[] ar) {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = new Random(System.currentTimeMillis());
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    @Override
    public String toString() {
        return String.valueOf(number) + " - " + title + " - " + mainImage + " - " + mainAudio;
    }

    public int getLevel() {
        return this.level;
    }

    public String getTitle() {
        return this.title;
    }

    public int getNumber() {
        return this.number;
    }

    public boolean isAudioInAssets() {
        return (this.getNumber() == 1 || this.getNumber() == 2);
    }

    public String getMainImage() {
        if (this.number >= 1 && this.number < 10) {
            return String.valueOf(this.number) + ".jpg";
        }
        return this.mainImage;
    }

    public String getMainAudio() {
        return this.mainAudio;
    }

    public String getImageUri(Context context, String image) {
        String[] images = image.split("/");
        String prefix = "";
        String filename = image;
        if (images.length == 1) {
            prefix = String.format("l%02d", this.number);
            filename = image;
        } else {
            prefix = images[0];
            filename = images[1];
        }
        if (prefix.equals("l01") || prefix.equals("l02")) {
            return "file:///android_asset/lessonimages/" + filename;
        } else {
            return "file:///" + FileProvider.getPath(context, prefix + "_" + filename);
        }
    }

    public String getMainText() {
        String[] lines = this.mainText.split("\\|");
        String sentence = "";
        for (int i = 0; i < lines.length - 1; i++) {
            sentence += lines[i] + "\n";
        }
        sentence += lines[lines.length - 1];
        return sentence;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public void setMainAudio(String mainAudio) {
        this.mainAudio = mainAudio;
    }

    public void setMainText(String text) {
        this.mainText = text;
    }

    public void setNext(Lesson lesson) {
        this.next = lesson;
    }

    public Lesson getNext() {
        return this.next;
    }

    public void increaseListenedCount() {
        this.listened_count++;
    }

    public int getListenedCount() {
        return this.listened_count;
    }

    public boolean getBookmark(Context context, int bookmark_type) {
        String key = String.format("bookmark_%02d_%02x", this.number, bookmark_type);
        boolean bookmark = SharedPref.getBoolean(context, key, false);
        return bookmark;
    }

    public void setBookmark(Context context, int bookmark_type, boolean bookmark) {
        String key = String.format("bookmark_%02d_%02x", this.number, bookmark_type);
        SharedPref.putBoolean(context, key, bookmark);
    }

    public int getSection() {
        return this.section;
    }

    public void setSection(Context context, int section) {
        this.section = section;
        SharedPref.putInt(context, String.format("SECTION_L%02d", this.number), section);
    }

    public void loadSection(Context context) {
        this.section = SharedPref.getInt(context, String.format("SECTION_L%02d", this.number), 0);
    }

    public boolean isLessonStudying(Context context) {
        int number = SharedPref.getInt(context, String.format("CURRENT_STUDYING_LV%02d", this.level), (this.level - 1) * 10 + 1);
        return (this.number == number);
    }

    public void setStudying(Context context) {
        SharedPref.putInt(context, String.format("CURRENT_STUDYING_LV%02d", this.level), this.number);
        this.expand(context);
    }

    public void expand(Context context) {
        SharedPref.putBoolean(context, String.format("EXPANDED_%02d", this.number), true);
        this.expanded = true;
    }

    public void contract(Context context) {
        SharedPref.putBoolean(context, String.format("EXPANDED_%02d", this.number), false);
        this.expanded = false;
    }

    public boolean isExpanded(Context context) {
        this.expanded = SharedPref.getBoolean(context, String.format("EXPANDED_%02d", this.number), false);
        return this.expanded;
    }

    public void loadCompleted(Context context) {
        this.completed = SharedPref.getBoolean(context, String.format("COMPLETED_%02d", this.number), false);
    }
    public void complete(Context context) {
        this.completed = true;
        SharedPref.putBoolean(context, String.format("COMPLETED_%02d", this.number), true);
    }

    public boolean wasLessonCompleted(Context context) {
        this.completed = SharedPref.getBoolean(context, String.format("COMPLETED_%02d", this.number), false);
        return this.completed;
    }

    public boolean canStudy(Context context) {
        if (this.number == 1) {
            return true;
        }
        boolean completed = SharedPref.getBoolean(context, String.format("COMPLETED_%02d", this.number - 1), false);
        return completed;
    }

    public Study compareAt(int session, int index) {
        ArrayList<Study> compares = this.getCompares(session);
        int count = this.numOfCompares(session);
        if (compares == null || index > count - 1 || index < 0) {
            return null;
        }
        return compares.get(index);
    }

    public Study listenAt(int session, int index) {
        ArrayList<Study> listens = this.getListens(session);
        int count = this.numOfListens(session);
        if (listens == null || index > count - 1 || index < 0) {
            return null;
        }
        return listens.get(index);
    }

    public int numOfCompares(int session) {
        ArrayList<Study> compares = this.getCompares(session);
        if (compares != null) {
            return compares.size();
        }
        return 0;
    }

    public int numOfListens(int session) {
        ArrayList<Study> listens = this.getListens(session);
        if (listens != null) {
            return listens.size();
        }
        return 0;
    }

    public ArrayList<Study> getCompares(int session) {
        if (session == 0x20) {
            return compares_20;
        } else if (session == 0x10) {
            return compares_10;
        } else if (session == 0x21) {
            return compares_21;
        } else if (session == 0x11) {
            return compares_11;
        } else {
            return compares_10;
        }
    }

    public ArrayList<Study> getListens(int session) {
        if (session == 0x20) {
            return listens_20;
        } else if (session == 0x10) {
            return listens_10;
        } else if (session == 0x21) {
            return listens_21;
        } else if (session == 0x11) {
            return listens_11;
        } else {
            return listens_10;
        }
    }

    public void setListens_10(ArrayList<Study> listens_10) {
        this.listens_10 = listens_10;
        this.compares_10 = new ArrayList<>();
        for (int i = 0; i < listens_10.size(); i++) {
            Study study0 = listens_10.get(i);
            boolean exist = false;
            for (int j = 0; j < i; j++) {
                Study study1 = listens_10.get(j);
                if (study0.getAudio().equals(study1.getAudio())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                this.compares_10.add(study0);
            }
        }
    }

    public void setListens_11(ArrayList<Study> listens_11) {
        this.listens_11 = listens_11;
        this.compares_11 = new ArrayList<>();
        for (int i = 0; i < listens_11.size(); i++) {
            Study study0 = listens_11.get(i);
            boolean exist = false;
            for (int j = 0; j < i; j++) {
                Study study1 = listens_11.get(j);
                if (study0.getAudio().equals(study1.getAudio())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                this.compares_11.add(study0);
            }
        }
    }

    public void setListens_20(ArrayList<Study> listens_20) {
        this.listens_20 = listens_20;
        this.compares_20 = new ArrayList<>();
        for (int i = 0; i < listens_20.size(); i++) {
            Study study0 = listens_20.get(i);
            boolean exist = false;
            for (int j = 0; j < i; j++) {
                Study study1 = listens_20.get(j);
                if (study0.getAudio().equals(study1.getAudio())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                this.compares_20.add(study0);
            }
        }
    }

    public void setListens_21(ArrayList<Study> listens_21) {
        this.listens_21 = listens_21;
        this.compares_20 = new ArrayList<>();
        this.compares_21 = new ArrayList<>();
        for (int i = 0; i < listens_21.size(); i++) {
            Study study0 = listens_21.get(i);
            boolean exist = false;
            for (int j = 0; j < i; j++) {
                Study study1 = listens_21.get(j);
                if (study0.getAudio().equals(study1.getAudio())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                this.compares_21.add(study0);
            }
        }
    }

    public Exercise exerciseAt(int session, int index) {
        if (session == 0x20) {
            if (index > this.exercises_20.size() - 1) {
                return null;
            }
            if (indices_20 == null) {
                indices_20 = new int[this.exercises_20.size()];
                for (int i = 0; i < this.exercises_20.size(); i++) {
                    indices_20[i] = i;
                }
                shuffleArray(indices_20);
            }
            return this.exercises_20.get(indices_20[index]);
        } else if (session == 0x11) {
            if (index > this.exercises_11.size() - 1) {
                return null;
            }
            if (indices_11 == null) {
                indices_11 = new int[this.exercises_11.size()];
                for (int i = 0; i < this.exercises_11.size(); i++) {
                    indices_11[i] = i;
                }
                shuffleArray(indices_11);
            }
            return this.exercises_11.get(indices_11[index]);
        }
        if (index > this.exercises_10.size() - 1) {
            return null;
        }
        if (indices_10 == null) {
            indices_10 = new int[this.exercises_10.size()];
            for (int i = 0; i < this.exercises_10.size(); i++) {
                indices_10[i] = i;
            }
            shuffleArray(indices_10);
        }
        return this.exercises_10.get(indices_10[index]);
    }

    public ArrayList<Exercise> getExercises(int session) {
        if (session == 0x20) {
            return exercises_20;
        } else if (session == 0x11) {
            return exercises_11;
        }
        return exercises_10;
    }

    public void setExercises_11(ArrayList<Exercise> exercises_11) {
        this.exercises_11 = exercises_11;
    }

    public void setExercises_10(ArrayList<Exercise> exercises_10) {
        this.exercises_10 = exercises_10;
    }

    public void setExercises_20(ArrayList<Exercise> exercises_20) {
        this.exercises_20 = exercises_20;
    }

    public void loadListens(Context context, int session) {
        DBHelper dbHelper = DBHelper.getInstance(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (session == 0x10) {
            this.setListens_10(Listen.load(context, db, this.getNumber(), session));
        } else if (session == 0x11) {
            this.setListens_11(Listen.load(context, db, this.getNumber(), session));
        } else if (session == 0x20) {
            this.setListens_20(Listen.load(context, db, this.getNumber(), session));
        } else if (session == 0x21) {
            this.setListens_21(Listen.load(context, db, this.getNumber(), session));
        }
    }

    public void loadExercises(Context context, int session) {
        if (session == 0x10) {
            DBHelper dbHelper = DBHelper.getInstance(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            this.setExercises_10(Exercise.load(context, db, this.getNumber(), session));
            if (indices_10 == null) {
                indices_10 = new int[this.exercises_10.size()];
                for (int i = 0; i < this.exercises_10.size(); i++) {
                    indices_10[i] = i;
                }
            }
            shuffleArray(this.indices_10);
        } else if (session == 0x11) {
            DBHelper dbHelper = DBHelper.getInstance(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            this.setExercises_11(Exercise.load(context, db, this.getNumber(), session));
            if (indices_11 == null) {
                indices_11 = new int[this.exercises_11.size()];
                for (int i = 0; i < this.exercises_11.size(); i++) {
                    indices_11[i] = i;
                }
            }
            shuffleArray(this.indices_11);
        } else if (session == 0x20) {
            DBHelper dbHelper = DBHelper.getInstance(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            this.setExercises_20(Exercise.load(context, db, this.getNumber(), session));
            if (indices_20 == null) {
                indices_20 = new int[this.exercises_20.size()];
                for (int i = 0; i < this.exercises_20.size(); i++) {
                    indices_20[i] = i;
                }
            }
            shuffleArray(indices_20);
        }
    }

    public void setQuizzes1(ArrayList<Quiz> quizzes1) {
        this.quizzes1 = quizzes1;
    }

    public void setQuizzes2(ArrayList<Quiz> quizzes2) {
        this.quizzes2 = quizzes2;
    }

    public ArrayList<Quiz> getQuizzes1() {
        ArrayList<Quiz> quizzes = new ArrayList<>();
        for (int i = 0; i < this.quizzes1.size(); i++) {
            quizzes.add(quizzes1.get(indices_30[i]));
        }
        return quizzes;
    }

    public ArrayList<Quiz> getQuizzes2() {

        ArrayList<Quiz> quizzes = new ArrayList<>();
        for (int i = 0; i < this.quizzes2.size(); i++) {
            quizzes.add(quizzes2.get(indices_32[i]));
        }
        return quizzes;
    }

    public Quiz getQuiz1(int index) {
        return this.quizzes1.get(indices_31[index]);
    }

    public int numOfQuizzes1() {
        return 10;
    }

    public int numOfQuizzes2() {
        return this.quizzes2.size();
    }

    public Quiz getQuiz2(int index) {
        return this.quizzes2.get(index);
    }

    public void loadQuizzes1(Context context) {
        DBHelper dbHelper = DBHelper.getInstance(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        this.setQuizzes1(Quiz.load(context, db, this.getNumber(), 1));
        if (indices_30 == null) {
            indices_30 = new int[this.quizzes1.size()];
            for (int i = 0; i < this.quizzes1.size(); i++) {
                indices_30[i] = i;
            }
        }
        shuffleArray(indices_30);
        if (indices_31 == null) {
            indices_31 = new int[this.quizzes1.size()];
            for (int i = 0; i < this.quizzes1.size(); i++) {
                indices_31[i] = indices_30[i];
            }
        }
        shuffleArray(indices_31);
    }

    public void loadQuizzes2(Context context) {
        DBHelper dbHelper = DBHelper.getInstance(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        this.setQuizzes2(Quiz.load(context, db, this.getNumber(), 2));
        if (indices_32 == null) {
            indices_32 = new int[this.quizzes2.size()];
            for (int i = 0; i < this.quizzes2.size(); i++) {
                indices_32[i] = i;
            }
        }
        shuffleArray(indices_32);
    }

    public boolean wasQuiz1Taken() {
        boolean ret = true;
        for (int i = 0; i < this.numOfQuizzes1(); i++) {
            Quiz quiz = this.getQuiz1(i);
            ret = ret && (quiz.getPoint() != -1);
        }
        return ret;
    }

    public boolean wasQuiz2Taken() {
        boolean ret = true;
        for (Quiz quiz : this.quizzes2) {
            ret = ret && (quiz.getPoint() != -1);
        }
        return ret;
    }

    public boolean canCheckQuiz1() {
        boolean ret = true;
        for (int i = 0; i < this.numOfQuizzes1(); i++) {
            Quiz quiz = this.getQuiz1(i);
            ret = ret && quiz.canCheck();
        }
        return ret;
    }

    public void loadScore(Context context) {
        this.score = new Score(String.format("%02d", this.number));
        this.score.load(context);
    }

    public void setPointForSession1(Context context) {
        this.score.setPointForSession1();
        this.score.save(context);
    }

    public void setPointForSession2(Context context) {
        this.score.setPointForSession2();
        this.score.save(context);
    }

    public void setPointForQuiz1(Context context, int point) {
        this.score.setPointForQuiz1(point);
        this.score.save(context);
    }

    public void setPointForQuiz2(Context context, int point) {
        this.score.setPointForQuiz2(point);
        this.score.save(context);
    }

    public int getPoint() {
        return this.score.getPoint();
    }

    public int calculateStars() {
        return this.score.calculateStars();
    }

    public int getStars(Context context) {
        return this.score.getStars(context);
    }

    public static Lesson newInstance(Context context, int level, Cursor cursor) {
        Lesson lesson = new Lesson();
        lesson.setLevel(level);
        lesson.setNumber(cursor.getInt(cursor.getColumnIndex("Number")));
        lesson.setTitle(cursor.getString(cursor.getColumnIndex("Title")));
        lesson.setMainImage(cursor.getString(cursor.getColumnIndex("Main_Image")));
        lesson.setMainText(cursor.getString(cursor.getColumnIndex("Main_Text")));
        lesson.setMainAudio(cursor.getString(cursor.getColumnIndex("Main_Audio")));
        lesson.loadSection(context);
        lesson.loadScore(context);
        lesson.loadCompleted(context);

        return lesson;
    }

    public static ArrayList<Lesson> load(Context context, SQLiteDatabase db, int level) {
        ArrayList<Lesson> list = new ArrayList<>();
        {
            Cursor cursor = db.rawQuery(
                    "SELECT Number, Title, Main_Image, Main_Text, Main_Audio" +
                            " FROM lesson WHERE Number>" + String.valueOf((level - 1) * 10) +
                            " AND Number<=" + String.valueOf(level * 10),
                    null);

            Lesson lesson0 = null;
            if (cursor.moveToFirst()) {
                do {
                    Lesson lesson1 = Lesson.newInstance(context, level, cursor);
                    list.add(lesson1);
                    if (lesson0 != null) {
                        lesson0.next = lesson1;
                    }
                    lesson0 = lesson1;
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.level);
        dest.writeInt(this.number);
        dest.writeString(this.title);
        dest.writeString(this.mainImage);
        dest.writeString(this.mainText);
        dest.writeString(this.mainAudio);
        dest.writeParcelable(this.next, flags);
        dest.writeInt(this.section);
        dest.writeInt(this.listened_count);
        dest.writeByte(this.expanded ? (byte) 1 : (byte) 0);
        dest.writeByte(this.completed ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.listens_10);
        dest.writeTypedList(this.listens_11);
        dest.writeTypedList(this.listens_20);
        dest.writeTypedList(this.listens_21);
        dest.writeTypedList(this.compares_10);
        dest.writeTypedList(this.compares_11);
        dest.writeTypedList(this.compares_20);
        dest.writeTypedList(this.compares_21);
        dest.writeTypedList(this.exercises_10);
        dest.writeTypedList(this.exercises_11);
        dest.writeTypedList(this.exercises_20);
        dest.writeTypedList(this.quizzes1);
        dest.writeTypedList(this.quizzes2);
        dest.writeParcelable(this.score, flags);
    }

    protected Lesson(Parcel in) {
        this.level = in.readInt();
        this.number = in.readInt();
        this.title = in.readString();
        this.mainImage = in.readString();
        this.mainText = in.readString();
        this.mainAudio = in.readString();
        this.next = in.readParcelable(Lesson.class.getClassLoader());
        this.section = in.readInt();
        this.listened_count = in.readInt();
        this.expanded = in.readByte() != 0;
        this.completed = in.readByte() != 0;
        this.listens_10 = in.createTypedArrayList(Study.CREATOR);
        this.listens_11 = in.createTypedArrayList(Study.CREATOR);
        this.listens_20 = in.createTypedArrayList(Study.CREATOR);
        this.listens_21 = in.createTypedArrayList(Study.CREATOR);
        this.compares_10 = in.createTypedArrayList(Study.CREATOR);
        this.compares_11 = in.createTypedArrayList(Study.CREATOR);
        this.compares_20 = in.createTypedArrayList(Study.CREATOR);
        this.compares_21 = in.createTypedArrayList(Study.CREATOR);
        this.exercises_10 = in.createTypedArrayList(Exercise.CREATOR);
        this.exercises_11 = in.createTypedArrayList(Exercise.CREATOR);
        this.exercises_20 = in.createTypedArrayList(Exercise.CREATOR);
        this.quizzes1 = in.createTypedArrayList(Quiz.CREATOR);
        this.quizzes2 = in.createTypedArrayList(Quiz.CREATOR);
        this.score = in.readParcelable(Score.class.getClassLoader());
    }

    public static final Creator<Lesson> CREATOR = new Creator<Lesson>() {
        @Override
        public Lesson createFromParcel(Parcel source) {
            return new Lesson(source);
        }

        @Override
        public Lesson[] newArray(int size) {
            return new Lesson[size];
        }
    };
}

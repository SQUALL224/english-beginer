package com.talkenglish.beginner.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.talkenglish.beginner.util.SharedPref;

/**
 * Created by alex
 */
public class Score implements Parcelable {
    public static final int MAX_POINT = 500;
    public static final int DEFAULT_SCORE = 0;
    private int session1;
    private int session2;
    private int quiz1;
    private int quiz2;
    private int point;

    private String suffix;
    public Score() {
    }
    public Score(String suffix) {
        this.suffix = suffix;
    }
    public void load(Context context) {

        this.point = SharedPref.getInt(context, "point_" + this.suffix, DEFAULT_SCORE);
        this.session1 = 50;
        this.session2 = 50;
        this.quiz1 = 0;
        this.quiz2 = 0;
    }

    public void setPointForSession1() {
        this.session1 = 50;
    }

    public void setPointForSession2() {
        this.session2 = 50;
    }

    public void setPointForQuiz1(int quiz1) {
        this.quiz1 = quiz1;
    }

    public void setPointForQuiz2(int quiz2) {
        this.quiz2 = quiz2;
    }

    public int getPointForSession1() {
        return session1;
    }

    public int getPointForSession2() {
        return session2;
    }

    public int getPointForQuiz1() {
        return quiz1;
    }

    public int getPointForQuiz2() {
        return quiz2;
    }
    public int getPoint() {
        return this.point;
    }
    public int calculatePoint() {
        return this.quiz1+this.quiz2+this.session1+this.session2;
    }
    public int calculateStars() {
        int sum = this.calculatePoint();
        if (sum >= 440 && sum <= 500) {
            return 3;
        } else if (sum >= 360 && sum <= 439) {
            return 2;
        } else if (sum >= 280 && sum <= 359) {
            return 1;
        }
        return 0;
    }
    public int getStars(Context context) {
        boolean completed = SharedPref.getBoolean(context, String.format("COMPLETED_%s", this.suffix), false);
        if (completed == false) {
            return 0;
        }
        int sum = this.point;
        if (sum >= 440 && sum <= 500) {
            return 3;
        } else if (sum >= 360 && sum <= 439) {
            return 2;
        } else if (sum >= 280 && sum <= 359) {
            return 1;
        }
        return 0;
    }
    public void save(Context context) {
        int sum = this.calculatePoint();
        if (sum > point && calculateStars() > 0) {
            point = sum;
            SharedPref.putInt(context, "point_" + this.suffix, point);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.session1);
        dest.writeInt(this.session2);
        dest.writeInt(this.quiz1);
        dest.writeInt(this.quiz2);
        dest.writeInt(this.point);
        dest.writeString(this.suffix);
    }

    protected Score(Parcel in) {
        this.session1 = in.readInt();
        this.session2 = in.readInt();
        this.quiz1 = in.readInt();
        this.quiz2 = in.readInt();
        this.point = in.readInt();
        this.suffix = in.readString();
    }

    public static final Creator<Score> CREATOR = new Creator<Score>() {
        @Override
        public Score createFromParcel(Parcel source) {
            return new Score(source);
        }

        @Override
        public Score[] newArray(int size) {
            return new Score[size];
        }
    };
}

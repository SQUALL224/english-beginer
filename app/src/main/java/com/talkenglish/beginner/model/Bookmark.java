package com.talkenglish.beginner.model;

import android.content.Context;

import com.talkenglish.beginner.R;

/**
 * Created by alex on 8/10/2016.
 */
public class Bookmark {
    protected static final String TAG = "Bookmark";
    private Lesson lesson;
    private int type;

    public Bookmark(Lesson lesson, int type) {
        this.lesson = lesson;
        this.type = type;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public int getType() {
        return type;
    }

    public String getString() {
        if (type == Lesson.STUDY_SESSION1) {
            return "Study session 1/2";
        } else if (type == Lesson.STUDY_SESSION2) {
            return "Study session 2/2";
        } else if (type == Lesson.QUIZ) {
            return "Quiz";
        } else if (type == Lesson.FINAL_CHECK) {
            return "Final Check";
        } else {
            return " ";
        }
    }
    public int getTypeImageResId() {
        if (type == Lesson.STUDY_SESSION1) {
            return R.drawable.ic_bookmark_study_session1;
        } else if (type == Lesson.STUDY_SESSION2) {
            return R.drawable.ic_bookmark_study_session2;
        } else if (type == Lesson.QUIZ) {
            return R.drawable.ic_bookmark_quiz;
        } else if (type == Lesson.FINAL_CHECK) {
            return R.drawable.ic_bookmark_final_check;
        } else {
            return R.drawable.placeholder_circle_image;
        }
    }
    public String getTitle() {
        if (lesson == null) {
            return "";
        }
        return "#" + String.valueOf(lesson.getNumber()) + ": " + lesson.getTitle();
    }
    public String getMainImageUri(Context context) {
        if (lesson == null) {
            return "";
        }
        return lesson.getImageUri(context, lesson.getMainImage());
    }
}

package com.talkenglish.beginner.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by alex on 8/7/2016.
 */
public class LessonData {

    public static String IMAGE_BASE_URL = "http://www.skesl.com/apps/chobo/images/";
    public static String AUDIO_BASE_URL = "http://www.skesl.com/apps/chobo/audio/";
    private int number;
    private ArrayList<Data> files;

    public LessonData() {
        files = new ArrayList<>();
    }

    public LessonData(int number) {
        this();
        this.number = number;
        this.files.add(new Data(String.valueOf(number) + ".jpg", IMAGE_BASE_URL, String.format("l%02d", number)));
        this.files.add(new Data(String.format("%02d.mp3", number), AUDIO_BASE_URL));
    }

    public int numOfFiles() {
        if (files == null) {
            return 0;
        }
        return files.size();
    }

    public Data fileAt(int index) {
        if (files == null) {
            return null;
        }
        return files.get(index);
    }

    private Data makeImageData(String fileName) {
        String[] images = fileName.split("/");
        if (images.length == 1) {
            return new Data(fileName, IMAGE_BASE_URL, String.format("l%02d", this.number));
        } else {
            return new Data(images[1], IMAGE_BASE_URL, images[0]);
        }
    }

    private void parseCursor(Cursor cursor) {
        String image1 = cursor.getString(cursor.getColumnIndex("VocabListen10_Image"));
        if (image1 != null) {
            this.files.add(makeImageData(image1.toLowerCase()));
        }
        String audio1 = cursor.getString(cursor.getColumnIndex("VocabListen10_Audio"));
        if (audio1 != null) {
            this.files.add(new Data(audio1.toLowerCase(), AUDIO_BASE_URL));
        }
        String image2 = cursor.getString(cursor.getColumnIndex("Exercise10_Image"));
        if (image2 != null) {
            this.files.add(makeImageData(image2.toLowerCase()));
        }
        String audio2 = cursor.getString(cursor.getColumnIndex("Exercise10_Audio"));
        if (audio2 != null) {
            this.files.add(new Data(audio2.toLowerCase(), AUDIO_BASE_URL));
        }
        String image2_1 = cursor.getString(cursor.getColumnIndex("Exercise11_Image"));
        if (image2_1 != null) {
            this.files.add(makeImageData(image2_1.toLowerCase()));
        }
        String audio2_1 = cursor.getString(cursor.getColumnIndex("Exercise11_Audio"));
        if (audio2_1 != null) {
            this.files.add(new Data(audio2_1.toLowerCase(), AUDIO_BASE_URL));
        }
        String image3 = cursor.getString(cursor.getColumnIndex("VocabListen11_Image"));
        if (image3 != null) {
            this.files.add(makeImageData(image3.toLowerCase()));
        }
        String audio3 = cursor.getString(cursor.getColumnIndex("VocabListen11_Audio"));
        if (audio3 != null) {
            this.files.add(new Data(audio3.toLowerCase(), AUDIO_BASE_URL));
        }
        String image4 = cursor.getString(cursor.getColumnIndex("VocabListen20_Image"));
        if (image4 != null) {
            this.files.add(makeImageData(image4.toLowerCase()));
        }
        String audio4 = cursor.getString(cursor.getColumnIndex("VocabListen20_Audio"));
        if (audio4 != null) {
            this.files.add(new Data(audio4.toLowerCase(), AUDIO_BASE_URL));
        }
        String image5 = cursor.getString(cursor.getColumnIndex("Exercise20_Image"));
        if (image5 != null) {
            this.files.add(makeImageData(image5.toLowerCase()));
        }
        String audio5 = cursor.getString(cursor.getColumnIndex("Exercise20_Audio"));
        if (audio5 != null) {
            this.files.add(new Data(audio5.toLowerCase(), AUDIO_BASE_URL));
        }
        String image6 = cursor.getString(cursor.getColumnIndex("VocabListen21_Image"));
        if (image6 != null) {
            this.files.add(makeImageData(image6.toLowerCase()));
        }
        String audio6 = cursor.getString(cursor.getColumnIndex("VocabListen21_Audio"));
        if (audio6 != null) {
            this.files.add(new Data(audio6.toLowerCase(), AUDIO_BASE_URL));
        }
    }

    public LessonData load(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery(
                "SELECT VocabListen10_Image, VocabListen10_Audio, Exercise10_Image, Exercise10_Audio, " +
                        "Exercise11_Image, Exercise11_Audio, " +
                        "VocabListen11_Image, VocabListen11_Audio, VocabListen20_Image, VocabListen20_Audio, " +
                        "Exercise20_Image, Exercise20_Audio, VocabListen21_Image, VocabListen21_Audio " +
                        "FROM Study WHERE Number=" + String.valueOf(this.number) + ";",
                null);

        if (cursor.moveToFirst()) {
            do {
                parseCursor(cursor);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return this;
    }

    public static class Data {

        private String fileName;
        private String fileUrl;
        private String prefix;

        public Data(String fileName, String baseUrl) {
            this.fileName = fileName;
            this.fileUrl = baseUrl + fileName;
            this.prefix = null;
        }

        public Data(String fileName, String baseUrl, String prefix) {
            this.fileName = fileName;
            this.fileUrl = baseUrl + prefix + "/" + fileName;
            this.prefix = prefix;
        }

        public String getFileName() {
            if (this.prefix != null) {
                return prefix + "_" + fileName;
            }
            return fileName;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public String getPrefix() {
            return prefix;
        }
    }
}

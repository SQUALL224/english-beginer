package com.talkenglish.beginner.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;

/**
 * Created by alex on 7/20/2016.
 */
public class Listen extends Study {
    protected static final String TAG = "Listen";

    public Listen() {
        super();
    }

    public static Listen newInstance(Context context, int session, Cursor cursor) {
        String p = String.format("VocabListen%02x_", session);
        Listen listen = new Listen();
        listen.setOrder(cursor.getInt(cursor.getColumnIndex("Order")));
        listen.setImage(cursor.getString(cursor.getColumnIndex(p + "Image")));
        listen.setText(cursor.getString(cursor.getColumnIndex(p + "Text")));
        listen.setAudio(cursor.getString(cursor.getColumnIndex(p + "Audio")));

        return listen;
    }

    public static ArrayList<Study> load(Context context, SQLiteDatabase db, int number, int session) {
        ArrayList<Study> list = new ArrayList<>();
        {
            String p = String.format("VocabListen%02x_", session);
            Cursor cursor = db.rawQuery(
                    "SELECT `Order`, " + p + "Image, " + p + "Text, " + p + "Audio" +
                            " FROM study WHERE Number=" + String.valueOf(number) +
                            " AND " + p + "Image not null AND " + p + "Audio not null AND " + p + "Text not null;",
                    null);

            if (cursor.moveToFirst()) {
                do {
                    list.add(Listen.newInstance(context, session, cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    protected Listen(Parcel in) {
        super(in);
    }

    public static final Creator<Listen> CREATOR = new Creator<Listen>() {
        @Override
        public Listen createFromParcel(Parcel source) {
            return new Listen(source);
        }

        @Override
        public Listen[] newArray(int size) {
            return new Listen[size];
        }
    };
}

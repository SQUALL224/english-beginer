package com.talkenglish.beginner.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by alex on 7/20/2016.
 */
public class Quiz implements Parcelable {
    private static final String TAG = "Quiz";

    protected int order;
    protected Sentence sentence;
    protected String answer;
    protected String candidate = "";
    protected int point;

    public Quiz() {
        candidate = "";
        point = -1;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Sentence getSentence() {
        return sentence;
    }

    public void setSentence(String quiz) {
        this.sentence = new Sentence(quiz);
    }

    public String getCandidate() {
        if (this.candidate != null && !this.candidate.equals("")) {
            return candidate;
        }
        return Sentence.SPACE;
    }
    public boolean canCheck() {
        if (this.candidate == null) {
            return false;
        }
        if (this.candidate.trim().equals("")) {
            return false;
        }
        return true;
    }
    public boolean check() {
        if (!this.canCheck()) {
            this.point = -1;
            return false;
        }
        if (this.candidate.toLowerCase().trim().equals(this.answer.toLowerCase().trim())) {
            this.point = 1;
            return true;
        }
        this.point = 0;
        return false;
    }
    public int getPoint() {
        return this.point;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }
    public boolean isBlank(int pos) {
        return this.sentence.getPosOfBlank() == pos;
    }
    public int numOfItems() {
        try {
            return this.sentence.getItems().size();
        } catch (Exception e) {
            return 0;
        }
    }
    public QuizToken itemAt(int pos) {
        try {
            return this.sentence.getItems().get(pos);
        } catch (Exception e) {
            return null;
        }
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return String.valueOf(order) + " - " + sentence + " - " + answer;
    }

    public static Quiz newInstance(Context context, int session, Cursor cursor) {
        String p = String.valueOf(session);
        Quiz quiz = new Quiz();
        quiz.setOrder(cursor.getInt(cursor.getColumnIndex("Order")));
        quiz.setSentence(cursor.getString(cursor.getColumnIndex("Quiz" + p)));
        quiz.setAnswer(cursor.getString(cursor.getColumnIndex("Quiz" + p + "_Answers")));

        return quiz;
    }

    public static ArrayList<Quiz> load(Context context, SQLiteDatabase db, int number, int session) {
        ArrayList<Quiz> list = new ArrayList<>();
        {
            String p = String.valueOf(session);
            Cursor cursor = db.rawQuery(
                    "SELECT `Order`, Quiz" + p + ", Quiz" + p + "_Answers" +
                            " FROM quiz WHERE Number=" + String.valueOf(number) +
                            " AND Quiz" + p + " not null",
                    null);

            if (cursor.moveToFirst()) {
                do {
                    list.add(Quiz.newInstance(context, session, cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return list;
    }

    public static class QuizToken implements Parcelable {
        private String string;
        private boolean spaceLeft;
        private boolean spaceRight;

        public QuizToken(String string) {
            this.string = string;
            this.spaceLeft = true;
            this.spaceRight = true;
        }
        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }

        public boolean isSpaceLeft() {
            return spaceLeft;
        }

        public void setSpaceLeft(boolean spaceLeft) {
            this.spaceLeft = spaceLeft;
        }

        public boolean isSpaceRight() {
            return spaceRight;
        }

        public void setSpaceRight(boolean spaceRight) {
            this.spaceRight = spaceRight;
        }

        @Override
        public String toString() {
            return this.string;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.string);
            dest.writeByte(this.spaceLeft ? (byte) 1 : (byte) 0);
            dest.writeByte(this.spaceRight ? (byte) 1 : (byte) 0);
        }

        public QuizToken() {
            this.string = "";
            this.spaceLeft = true;
            this.spaceRight = true;
        }

        protected QuizToken(Parcel in) {
            this.string = in.readString();
            this.spaceLeft = in.readByte() != 0;
            this.spaceRight = in.readByte() != 0;
        }

        public static final Creator<QuizToken> CREATOR = new Creator<QuizToken>() {
            @Override
            public QuizToken createFromParcel(Parcel source) {
                return new QuizToken(source);
            }

            @Override
            public QuizToken[] newArray(int size) {
                return new QuizToken[size];
            }
        };
    }
    public static class Sentence implements Parcelable {
        private static final String REGULAR_CHARS = "_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.";
        private static final String BLANK = "__________";
        private static final String SPACE = "          ";
        String raw;
        int posOfBlank;
        ArrayList<QuizToken> items;

        public Sentence(String raw) {
            this.items = new ArrayList<>();
            String[] strings = raw.split(" ");
            for (int i = 0; i < strings.length; i ++) {
                this.items.addAll(extractWordsAndSpecs(strings[i]));
            }
            for (int i = 0; i < this.items.size(); i ++) {
                if (items.get(i).toString().equals(BLANK)) {
                    posOfBlank = i;
//                    items.get(i).replace("_", " ");
                    break;
                }
            }
            this.raw = raw;
        }
        @Override
        public String toString() {
            return raw;
        }

        public int getPosOfBlank() {
            return posOfBlank;
        }

        public ArrayList<QuizToken> getItems() {
            return items;
        }
        public void setItem(QuizToken item) {
            this.items.set(posOfBlank, item);
        }

        public String getRaw() {
            return this.raw;
        }
        private ArrayList<String> extractWordsAndSpecs_old(String string) {

            ArrayList<String> list = new ArrayList<>();
            int k = 0;
            for (int i = 0; i < string.length(); i++) {
                String substr = string.substring(i, i + 1);
                if (!REGULAR_CHARS.contains(substr)) {
                    if (i - 1 >= k) {
                        list.add(string.substring(k, i).trim());
                    }
                    list.add(substr.trim());
                    k = i + 1;
                }
            }
            String last = string.substring(k, string.length()).trim();
            if (last.length() > 0) {
                list.add(last);
            }
            return list;
        }
        public static  ArrayList<QuizToken> extractWordsAndSpecs(String string) {

            ArrayList<QuizToken> list = new ArrayList<>();

            if (string.contains(BLANK) && string.length() == BLANK.length()) {
                list.add(new QuizToken(BLANK));
                return list;
            } else if (!string.contains(BLANK)) {
                list.add(new QuizToken(string));
                return list;
            }
            int i = string.indexOf(BLANK);
            String string1 = string.substring(0, i);
            String string2 =string.substring(i, i+BLANK.length());
            String string3 = string.substring(i+BLANK.length(), string.length());
            if (!string1.equals("")) {
                QuizToken token = new QuizToken(string1);
                token.setSpaceRight(false);
                list.add(token);
            }
            list.add(new QuizToken(string2));
            if (!string3.equals("")) {
                QuizToken token = new QuizToken(string3);
                token.setSpaceLeft(false);
                list.add(token);
            }
            return list;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.raw);
            dest.writeInt(this.posOfBlank);
            dest.writeTypedList(this.items);
        }

        protected Sentence(Parcel in) {
            this.raw = in.readString();
            this.posOfBlank = in.readInt();
            this.items = in.createTypedArrayList(QuizToken.CREATOR);
        }

        public static final Creator<Sentence> CREATOR = new Creator<Sentence>() {
            @Override
            public Sentence createFromParcel(Parcel source) {
                return new Sentence(source);
            }

            @Override
            public Sentence[] newArray(int size) {
                return new Sentence[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.order);
        dest.writeParcelable(this.sentence, flags);
        dest.writeString(this.answer);
        dest.writeString(this.candidate);
        dest.writeInt(this.point);
    }

    protected Quiz(Parcel in) {
        this.order = in.readInt();
        this.sentence = in.readParcelable(Sentence.class.getClassLoader());
        this.answer = in.readString();
        this.candidate = in.readString();
        this.point = in.readInt();
    }

    public static final Creator<Quiz> CREATOR = new Creator<Quiz>() {
        @Override
        public Quiz createFromParcel(Parcel source) {
            return new Quiz(source);
        }

        @Override
        public Quiz[] newArray(int size) {
            return new Quiz[size];
        }
    };
}

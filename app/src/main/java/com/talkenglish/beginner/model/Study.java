package com.talkenglish.beginner.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alex on 7/20/2016.
 */
public class Study implements Parcelable {
    private static final String TAG = "Study";

    protected int order;
    protected String image;
    protected String text;
    protected String audio;

    public Study() {
    }

    @Override
    public String toString() {
        return String.valueOf(order) + " - " + text + " - " + image + " - " + audio;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image.trim().toLowerCase();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio.trim().toLowerCase();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.order);
        dest.writeString(this.image);
        dest.writeString(this.text);
        dest.writeString(this.audio);
    }

    protected Study(Parcel in) {
        this.order = in.readInt();
        this.image = in.readString();
        this.text = in.readString();
        this.audio = in.readString();
    }

    public static final Creator<Study> CREATOR = new Creator<Study>() {
        @Override
        public Study createFromParcel(Parcel source) {
            return new Study(source);
        }

        @Override
        public Study[] newArray(int size) {
            return new Study[size];
        }
    };
}

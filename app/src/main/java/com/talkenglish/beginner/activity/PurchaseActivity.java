package com.talkenglish.beginner.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.talkenglish.beginner.BuildConfig;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.util.CustomTypefaceSpan;
import com.talkenglish.beginner.util.FontCache;
import com.talkenglish.beginner.util.PurchaseInfo;

import java.util.Hashtable;

public class PurchaseActivity extends AppCompatActivity implements PurchaseInfo.OnPurchaseInfoListener{
    private static final String TAG = "PurchaseActivity";
//    private NavigationView mDrawer;
//    private DrawerLayout mDrawerLayout;
//    private ActionBarDrawerToggle drawerToggle;

    boolean clicked = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }
    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConn);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (PurchaseInfo.onActivityResult(this, requestCode, resultCode, data, mBillingService)) {

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        if (drawerToggle != null) {
//            drawerToggle.onConfigurationChanged(newConfig);
//        }
    }
//    @Override
//    public boolean onNavigationItemSelected(MenuItem menuItem) {
//        menuItem.setChecked(true);
//        int selectedId = menuItem.getItemId();
//        if (selectedId == R.id.menu_item_bookmark) {
//            Analytics.sendEvent(getApplication(), "Menu 'Bookmark' Selected", toString());
//            Intent i = new Intent(this, BookmarkActivity.class);
//            startActivity(i);
//            closeDrawer();
//        } else if (selectedId == R.id.menu_item_remove_ads) {
//            Analytics.sendEvent(getApplication(), "Menu 'Remove Ads' Selected", toString());
//            closeDrawer();
//        } else if (selectedId == R.id.menu_item_instructions) {
//            Analytics.sendEvent(getApplication(), "Menu 'Instructions' Selected", toString());
//            Intent i = new Intent(this, InstructionsActivity.class);
//            startActivity(i);
//            closeDrawer();
//        } else if (selectedId == R.id.menu_item_contact_us) {
//            Analytics.sendEvent(getApplication(), "Menu 'Contact Us' Selected", toString());
//            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
//                    "mailto", "talkenglish@talkenglish.com", null));
//            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " Android");
//            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
//            startActivity(Intent.createChooser(emailIntent, "Send email..."));
//            closeDrawer();
//        } else if (selectedId == R.id.menu_item_recommended_apps) {
//            Analytics.sendEvent(getApplication(), "Menu 'Recommended Apps' Selected", toString());
//            Intent i = new Intent(this, RecommendedAppsActivity.class);
//            startActivity(i);
//            closeDrawer();
//        } else if (selectedId == R.id.menu_item_visit_web) {
//            Analytics.sendEvent(getApplication(), "Menu 'Visit Website' Selected", toString());
//            Intent i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse("http://www.talkenglish.com"));
//            startActivity(i);
//            closeDrawer();
//        } else if (selectedId == R.id.menu_item_share_app) {
//            Analytics.sendEvent(getApplication(), "Menu 'Share App' Selected", toString());
//            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//            sharingIntent.setType("text/plain");
//            sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
//            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Beginner));
//            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_title)));
//            closeDrawer();
//        }
//
//        return true;
//    }

    public void onActionBarRightButtonClicked(View v) {
//        openDrawer();
        Analytics.sendEvent(getApplication(), "Hamburger Pressed", toString());
    }

    @Override
    public String toString() {
        return TAG;
    }

    public void onActionBarLeftButtonClicked(View v) {

        onBackPressed();
        Analytics.sendEvent(getApplication(), "Back Pressed", toString());
    }

    protected void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
    }

    protected void setActionBarLeftButtonEnabled(boolean enabled) {
        LinearLayout llActionBarLeftButton = (LinearLayout) findViewById(R.id.ll_actionbar_left_button);
        ImageView ivActionBarLeftButton = (ImageView) findViewById(R.id.iv_actionbar_left_button);

        if (enabled) {
            if (llActionBarLeftButton != null) {
                llActionBarLeftButton.setVisibility(View.VISIBLE);
            }
            if (ivActionBarLeftButton != null) {
                ivActionBarLeftButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (llActionBarLeftButton != null) {
                llActionBarLeftButton.setVisibility(View.INVISIBLE);
            }
            if (ivActionBarLeftButton != null) {
                ivActionBarLeftButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    protected void setActionBarRightButtonEnabled(boolean enabled) {
        LinearLayout llActionBarRightButton = (LinearLayout) findViewById(R.id.ll_actionbar_right_button);
        ImageView ivActionBarRightButton = (ImageView) findViewById(R.id.iv_actionbar_right_button);
        if (enabled) {
            if (llActionBarRightButton != null) {
                llActionBarRightButton.setVisibility(View.VISIBLE);
            }
            if (ivActionBarRightButton != null) {
                ivActionBarRightButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (llActionBarRightButton != null) {
                llActionBarRightButton.setVisibility(View.INVISIBLE);
            }
            if (ivActionBarRightButton != null) {
                ivActionBarRightButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    protected void setActionBarLeftButtonDrawable(int drawableRes) {
        ImageView ivActionBarLeftButton = (ImageView) findViewById(R.id.iv_actionbar_left_button);
        if (ivActionBarLeftButton != null) {
            ivActionBarLeftButton.setImageResource(drawableRes);
        }
    }

    protected void setActionBarRightButtonDrawable(int drawableRes) {
        ImageView ivActionBarRightButton = (ImageView) findViewById(R.id.iv_actionbar_right_button);
        if (ivActionBarRightButton != null) {
            ivActionBarRightButton.setImageResource(drawableRes);
        }
    }

//    protected void setupDrawer() {
//
//        mDrawer = (NavigationView) findViewById(R.id.main_drawer);
//        mDrawer.setNavigationItemSelectedListener(this);
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//
//        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.drawer_open, R.string.drawer_close);
//        mDrawerLayout.setDrawerListener(drawerToggle);
//
//        drawerToggle.syncState();
//        mDrawer.setItemIconTintList(null);
//
//        Menu m = mDrawer.getMenu();
//        for (int i = 0; i < m.size(); i++) {
//            MenuItem mi = m.getItem(i);
//
//            //for aapplying a font to subMenu ...
//            SubMenu subMenu = mi.getSubMenu();
//            if (subMenu != null && subMenu.size() > 0) {
//                for (int j = 0; j < subMenu.size(); j++) {
//                    MenuItem subMenuItem = subMenu.getItem(j);
//                    applyFontToMenuItem(subMenuItem);
//                }
//            }
//
//            //the method we have create in activity
//            applyFontToMenuItem(mi);
//        }
//    }

//    protected void openDrawer() {
//        mDrawerLayout.openDrawer(GravityCompat.END);
//    }
//
//    protected void closeDrawer() {
//        mDrawerLayout.closeDrawer(GravityCompat.END);
//    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = FontCache.getTypefaceNotoSans_Bold(this);
        SpannableString newTitle = new SpannableString(mi.getTitle());
        newTitle.setSpan(new CustomTypefaceSpan("", font), 0, newTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(newTitle);
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback);
        setActionBarRightButtonDrawable(R.drawable.ic_hamburger);
    }
    private void buildLayout() {
        Analytics.sendScreenName(getApplication(), "Purchase Screen");
        buildToolbar();
//        setupDrawer();
        setTitle("Remove Ads");

        RelativeLayout purchaseNow = (RelativeLayout) findViewById(R.id.ui_btn_purchase);
        RelativeLayout purchaseNow1 = (RelativeLayout) findViewById(R.id.ui_btn_purchase1);
        RelativeLayout purchaseNow3 = (RelativeLayout) findViewById(R.id.ui_btn_purchase3);

        purchaseNow.setTag(PurchaseInfo.SKU_PREMIUM);
        purchaseNow1.setTag(PurchaseInfo.SKU_PREMIUM1);
        purchaseNow3.setTag(PurchaseInfo.SKU_PREMIUM3);
        View.OnClickListener clickUpgrade = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clicked) return;
                clicked = true;
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        clicked = false;
                    }
                }, 1000);

                onUpgradeAppButtonClicked((String) v.getTag());
            }
        };

        purchaseNow.setOnClickListener(clickUpgrade);
        purchaseNow1.setOnClickListener(clickUpgrade);
        purchaseNow3.setOnClickListener(clickUpgrade);

        updateContent(PurchaseInfo.retrievePurchaseInfoList(this, this.getBillingService(), this));

    }
    private void updateContent(Hashtable<String, PurchaseInfo> purchaseInfos) {

        if(purchaseInfos != null) {
            PurchaseInfo p0 = purchaseInfos.get(PurchaseInfo.SKU_PREMIUM);
            if(p0 != null) {
                ((TextView) findViewById(R.id.ui_price)).setText(p0.priceString);
            }
            PurchaseInfo p1 = purchaseInfos.get(PurchaseInfo.SKU_PREMIUM1);
            if(p1 != null) {
                ((TextView) findViewById(R.id.ui_price1)).setText(p1.priceString);
            }
            PurchaseInfo p3 = purchaseInfos.get(PurchaseInfo.SKU_PREMIUM3);
            if(p3 != null) {
                ((TextView) findViewById(R.id.ui_price3)).setText(p3.priceString);
            }
        }

        int removeAd = PurchaseInfo.doRemoveAd(purchaseInfos);
        if ((removeAd&1) == 1) {
            ((TextView) findViewById(R.id.ui_price3)).setText("Purchased");
            findViewById(R.id.imageView3).setVisibility(View.GONE);
            findViewById(R.id.ui_btn_purchase3).setBackgroundColor(Color.parseColor("#CCCCCC"));
        }
        if (((removeAd>>2)&1) == 1) {
            ((TextView) findViewById(R.id.ui_price)).setText("Purchased");
            findViewById(R.id.imageView1).setVisibility(View.GONE);
            findViewById(R.id.ui_btn_purchase).setBackgroundColor(Color.parseColor("#CCCCCC"));
        }

//        if (purchaseInfos == null) {
//            findViewById(R.id.section_purchase).setVisibility(View.GONE);
//        }
//        else if (removeAd) {
//            findViewById(R.id.section_purchase).setVisibility(View.GONE);
//        } else {
//            findViewById(R.id.section_purchase).setVisibility(View.VISIBLE);
//        }
    }
    /**------------------------------------------------- IN-APP BILLING -------------------------------------------------**/

    // User clicked the "Upgrade to Premium" button.
    public void onUpgradeAppButtonClicked(String sku_type) {
        PurchaseInfo.purchase(this, mBillingService, sku_type);
    }

    IInAppBillingService mBillingService;
    Handler mHandler = new Handler();

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBillingService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBillingService = IInAppBillingService.Stub.asInterface(service);
            onBillingServiceUpdated(mBillingService);
        }
    };

    protected void onBillingServiceUpdated(IInAppBillingService service) {
        onPurchaseInfoRetrieved(PurchaseInfo.retrievePurchaseInfoList(this, mBillingService, this));
    }

    public IInAppBillingService getBillingService() {
        return mBillingService;
    }

    @Override
    public void onPurchaseInfoRetrieved(final Hashtable<String, PurchaseInfo> purchaseInfos) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
//                if (PurchaseInfo.doRemoveAd(purchaseInfos)) {
//                    hideBannerAd();
//                } else {
//                    showBannerAd();
//                }

                updateContent(purchaseInfos);
            }
        });
    }

    @Override
    public void onPurchaseInfoFailed() {

    }
}

package com.talkenglish.beginner.activity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.FileProvider;
import com.talkenglish.beginner.env.MultipleAudioPlayer;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.model.Study;
import com.talkenglish.beginner.statmodel.ListeningStat;

import java.io.IOException;
import java.util.ArrayList;


public class ListeningActivity extends ActionbarBaseActivity implements SeekBar.OnSeekBarChangeListener{
    private static final String TAG = "ListeningActivity";

    public static final int SESSION_LISTENING = 0x102;

    ImageView mainView;
    SeekBar indicator;
    ImageView ivPlay;
    ImageView ivPause;
    ImageView ivSkip;
    TextView tvTimeFrame;
    TextView tvCompleteToNext;
    TextView tvSkip;
    TextView tvSessionTitle;

    MultipleAudioPlayer audioPlayer;
    ListeningStat stat;
    private Lesson lesson;
    private int session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stat = new ListeningStat();
        buildLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MultipleAudioPlayer.stop(audioPlayer);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == ComparingActivity.SESSION_COMPARING) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            buildLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            buildLayout();
        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback_white);
        int type = Lesson.STUDY_SESSION1;
        if ((session&0x10) != 0) {
            type = Lesson.STUDY_SESSION1;
        } else if ((session&0x20) != 0) {
            type = Lesson.STUDY_SESSION2;
        }
        boolean bookmark = lesson.getBookmark(this, type);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    public void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.WHITE);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.STUDY_SESSION1;
        if ((session&0x10) != 0) {
            type = Lesson.STUDY_SESSION1;
        } else if ((session&0x20) != 0) {
            type = Lesson.STUDY_SESSION2;
        }
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {
        setContentView(R.layout.activity_listening);

        lesson = (Lesson) getIntent().getExtras().get("lesson");
        session = getIntent().getExtras().getInt("session");

        buildToolbar();

        mainView = (ImageView) findViewById(R.id.rl_session_main);
        indicator = (SeekBar) findViewById(R.id.progress_indicator);
        indicator.setMax(100);
        indicator.setOnSeekBarChangeListener(this);

        ivPlay = (ImageView) findViewById(R.id.iv_play);
        ivPause = (ImageView) findViewById(R.id.iv_pause);
        ivSkip = (ImageView) findViewById(R.id.iv_skip);
        tvTimeFrame = (TextView) findViewById(R.id.tv_time_frame);
        ivPlay.setVisibility(View.VISIBLE);
        ivPause.setVisibility(View.GONE);

        tvCompleteToNext = (TextView) findViewById(R.id.tv_complete_to_next_button);
        tvSkip = (TextView) findViewById(R.id.tv_skip_to_next_button);

        tvSessionTitle = (TextView) findViewById(R.id.session_study_title);

        tvSkip.setEnabled(!stat.completed);
        tvCompleteToNext.setEnabled(stat.completed);
        tvCompleteToNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ListeningActivity.this, ComparingActivity.class);
                i.putExtra("session", session);
                i.putExtra("lesson", lesson);
                gotoActivityForResult(i, ComparingActivity.SESSION_COMPARING);
                Analytics.sendEvent(getApplication(), "Complete Pressed in Study Session - Listening" + session, lesson.getTitle());

            }
        });
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListeningActivity.this, ComparingActivity.class);
                i.putExtra("session", session);
                i.putExtra("lesson", lesson);
                gotoActivityForResult(i, ComparingActivity.SESSION_COMPARING);
                Analytics.sendEvent(getApplication(), "Skip Pressed in Study Session - Listening" + session, lesson.getTitle());
            }
        });

        if ((session&0x10) != 0) {
            setTitle("Study Session 1/2");
        } else if ((session&0x20) != 0) {
            setTitle("Study Session 2/2");
        }
        loadData(lesson, session);

        if (session == 0x10 && lesson.getNumber() == 1) {
            EngLearningBeginnerApp.guideVideoPlayed = 0;
        }
        Analytics.sendScreenName(getApplication(), "Session Study Screen - Listening " + session);
    }

    public void setImage(String imageUrl) {
        if (imageUrl == null || imageUrl == "") {
            return;
        }
        Picasso.with(this)
                .load(imageUrl)
                .placeholder(R.drawable.placeholder_image)
                .into(mainView);
    }

    private void loadData(final Lesson lesson, final int session) {
        Study listen = lesson.listenAt(session, stat.currentPos);

        setImage(lesson.getImageUri(this, listen.getImage()));
        tvSessionTitle.setText(listen.getText());
        tvTimeFrame.setText(stat.durationLabel);
        ivPlay.setEnabled(true);
        ivSkip.setEnabled(true);
        indicator.setProgress((int)stat.progress*100);
        if (stat.paused) {
            ivPlay.setVisibility(View.VISIBLE);
            ivPause.setVisibility(View.GONE);
        } else {
            ivPlay.setVisibility(View.GONE);
            ivPause.setVisibility(View.VISIBLE);
        }
        if (stat.isAudioLoaded == false) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    ivPlay.setEnabled(false);
                    ivSkip.setEnabled(false);
                }

                @Override
                protected Void doInBackground(Void... voids) {

                    if (audioPlayer == null) {
                        loadAudio(lesson, session);
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (audioPlayer != null) {
                        stat.isAudioLoaded = true;
                        stat.currentPos = 0;
                        stat.durationLabel = audioPlayer.getDurationFormat();
                        tvTimeFrame.setText(audioPlayer.getDurationFormat());
                        ivPlay.setEnabled(true);
                        ivSkip.setEnabled(true);
                    }
                }
            }.execute();
        } else {
        }
    }

    private void loadAudio(final Lesson lesson, final int session) {
        if (lesson.isAudioInAssets()) {
            ArrayList<AssetFileDescriptor> descriptors = new ArrayList<>();
            try {
                for (int i = 0; i < lesson.numOfListens(session); i ++) {
                    Study listen = lesson.listenAt(session, i);
                    AssetFileDescriptor descriptor = getAssets().openFd("audio/" + listen.getAudio());
                    descriptors.add(descriptor);
                }
                audioPlayer = MultipleAudioPlayer.loadWithDescriptors(descriptors, this, new AudioProgressUpdateListener(lesson, session));
            } catch (IOException e) {
                e.printStackTrace();
                audioPlayer = null;
            }
        } else {
            ArrayList<String> paths = new ArrayList<>();
            try {
                for (int i = 0; i < lesson.numOfListens(session); i ++) {
                    Study listen = lesson.listenAt(session, i);
                    String path = FileProvider.getPath(this, listen.getAudio());
                    paths.add(path);
                }
                audioPlayer = MultipleAudioPlayer.loadWithPaths(paths, this, new AudioProgressUpdateListener(lesson, session));
            } catch (Exception e) {
                e.printStackTrace();
                audioPlayer = null;
            }
        }
    }

    public void onClickPlay(View v) {
        if (audioPlayer != null) {
            if (audioPlayer.isPlaying()) {
                audioPlayer.resume();
                ivPlay.setVisibility(View.GONE);
                ivPause.setVisibility(View.VISIBLE);
                stat.paused = false;
            } else {
                if (audioPlayer.start()) {
                    ivPlay.setVisibility(View.GONE);
                    ivPause.setVisibility(View.VISIBLE);
                    stat.paused = false;
                }
            }
        }
    }

    public void onClickPause(View v) {
        MultipleAudioPlayer.pause(audioPlayer);
        ivPause.setVisibility(View.GONE);
        ivPlay.setVisibility(View.VISIBLE);
        stat.paused = true;
    }

    public void onClickSkip(View v) {
        Analytics.sendEvent(getApplication(), "Skip Pressed in Study Session - Listening" + session, lesson.getTitle());
        if (audioPlayer != null) {
            stat.paused = false;
            ivPlay.setVisibility(View.GONE);
            ivPause.setVisibility(View.VISIBLE);
            audioPlayer.skip();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            if (audioPlayer != null) {
                stat.paused = false;
                ivPlay.setVisibility(View.GONE);
                ivPause.setVisibility(View.VISIBLE);
                audioPlayer.skipTo(progress);
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    class AudioProgressUpdateListener implements MultipleAudioPlayer.ProgressUpdateListener {
        Lesson lesson;
        int session;

        public AudioProgressUpdateListener(Lesson lesson, int session) {
            this.lesson = lesson;
            this.session = session;
        }

        @Override
        public void onProgress(float progress) {
            try {
                stat.currentPos = audioPlayer.getCurrentPos();
                stat.progress = progress;
                stat.durationLabel = audioPlayer.getDurationFormat();

                Study listen = lesson.listenAt(session, audioPlayer.getCurrentPos());
                indicator.setProgress((int)(progress*100));
                tvTimeFrame.setText(audioPlayer.getDurationFormat());
                setImage(lesson.getImageUri(ListeningActivity.this, listen.getImage()));
                tvSessionTitle.setText(listen.getText());

            } catch (Exception e) {

            }
        }

        @Override
        public void onComplete() {
            MultipleAudioPlayer.stop(audioPlayer);
            try {
                stat.currentPos = audioPlayer.getCurrentPos();
                stat.progress = 0;
                stat.paused = true;
                stat.durationLabel = audioPlayer.getDurationFormat();
                stat.completed = true;

                Study listen = lesson.listenAt(session, audioPlayer.getCurrentPos());
                indicator.setProgress(0);
                tvTimeFrame.setText(audioPlayer.getDurationFormat());
                setImage(lesson.getImageUri(ListeningActivity.this, listen.getImage()));
                tvSessionTitle.setText(listen.getText());
                ivPause.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
                tvCompleteToNext.setEnabled(stat.completed);
                tvSkip.setEnabled(!stat.completed);
            } catch (Exception e) {
            }
        }
    }
}

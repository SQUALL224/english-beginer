package com.talkenglish.beginner.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.talkenglish.beginner.BuildConfig;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.ad.InterstitialAdController;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.Purchase;
import com.talkenglish.beginner.env.RateApp;
import com.talkenglish.beginner.util.CustomTypefaceSpan;
import com.talkenglish.beginner.util.FontCache;
import com.talkenglish.beginner.util.PurchaseInfo;

import java.util.Hashtable;

public abstract class ActionbarBaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        PurchaseInfo.OnPurchaseInfoListener {

    protected final String TAG = "ActionbarBaseActivity";

//    private NavigationView mDrawer;
//    private DrawerLayout mDrawerLayout;
//    private ActionBarDrawerToggle drawerToggle;

    private InterstitialAdController adController;

    /** IN-APP BILLING **/
    IInAppBillingService billingService;
    Handler handler = new Handler();
    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            billingService = IInAppBillingService.Stub.asInterface(service);
            onBillingServiceUpdated(billingService);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            billingService = null;
        }
    };

    protected void onBillingServiceUpdated(IInAppBillingService service) {
        onPurchaseInfoRetrieved(PurchaseInfo.retrievePurchaseInfoList(this, billingService, this));
    }

    @Override
    public void onPurchaseInfoFailed() {

    }

    @Override
    public void onPurchaseInfoRetrieved(final Hashtable<String, PurchaseInfo> purchaseInfos) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if ((PurchaseInfo.doRemoveAd(purchaseInfos)&0x5) != 0) {
                    hideBannerAd();
                } else {
                    showBannerAd();
                    Purchase.checkPurchase(getApplication(),
                            ActionbarBaseActivity.this, new Intent(ActionbarBaseActivity.this, PurchaseActivity.class));
                }
            }
        });
    }
    /** IN-APP BILLING **/

    private void showBannerAd() {
        AdView mAdView = (AdView) findViewById(R.id.adView);
        if(mAdView == null) return;
        mAdView.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void hideBannerAd() {
        AdView mAdView = (AdView) findViewById(R.id.adView);
        if(mAdView != null) mAdView.setVisibility(View.GONE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        adController = new InterstitialAdController(this);
        buildToolbar();
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_hamburger);
        setActionBarRightButtonDrawable(R.drawable.ic_menu_bookmark_off);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        RateApp.checkRate(getApplication(), this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        if (drawerToggle != null) {
//            drawerToggle.onConfigurationChanged(newConfig);
//        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        int selectedId = menuItem.getItemId();
        if (selectedId == R.id.menu_item_bookmark) {
            Analytics.sendEvent(getApplication(), "Menu 'Bookmark' Selected", toString());
            if (!(this instanceof BookmarkActivity)) {
                Intent i = new Intent(this, BookmarkActivity.class);
                gotoActivity(i);
            }
//            closeDrawer();
        } else if (selectedId == R.id.menu_item_remove_ads) {
            Analytics.sendEvent(getApplication(), "Menu 'Remove Ads' Selected", toString());

            Intent i = new Intent(this, PurchaseActivity.class);
            gotoActivity(i);
//            closeDrawer();
        } else if (selectedId == R.id.menu_item_instructions) {
            Analytics.sendEvent(getApplication(), "Menu 'Instructions' Selected", toString());
            if (!(this instanceof InstructionsActivity)) {
                Intent i = new Intent(this, InstructionsActivity.class);
                gotoActivity(i);
            }
//            closeDrawer();
        } else if (selectedId == R.id.menu_item_contact_us) {
            Analytics.sendEvent(getApplication(), "Menu 'Contact Us' Selected", toString());
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "talkenglish@talkenglish.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " Android");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
//            closeDrawer();
        } else if (selectedId == R.id.menu_item_recommended_apps) {
            Analytics.sendEvent(getApplication(), "Menu 'Recommended Apps' Selected", toString());
            if (!(this instanceof RecommendedAppsActivity)) {
                Intent i = new Intent(this, RecommendedAppsActivity.class);
                gotoActivity(i);
            }
//            closeDrawer();
        } else if (selectedId == R.id.menu_item_visit_web) {
            Analytics.sendEvent(getApplication(), "Menu 'Visit Website' Selected", toString());
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("http://www.talkenglish.com"));
            startActivity(i);
//            closeDrawer();
        } else if (selectedId == R.id.menu_item_share_app) {
            Analytics.sendEvent(getApplication(), "Menu 'Share App' Selected", toString());
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Beginner));
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_title)));
//            closeDrawer();
        }

        return true;
    }

    public void onActionBarRightButtonClicked(View v) {

    }

    @Override
    public String toString() {
        return TAG;
    }

    public void onActionBarLeftButtonClicked(View v) {

        Analytics.sendEvent(getApplication(), "Back Pressed", toString());
    }

    public void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.parseColor("#2c2d32"));
    }

    protected void setActionBarLeftButtonEnabled(boolean enabled) {
        LinearLayout llActionBarLeftButton = (LinearLayout) findViewById(R.id.ll_actionbar_left_button);
        ImageView ivActionBarLeftButton = (ImageView) findViewById(R.id.iv_actionbar_left_button);

        if (enabled) {
            if (llActionBarLeftButton != null) {
                llActionBarLeftButton.setVisibility(View.VISIBLE);
            }
            if (ivActionBarLeftButton != null) {
                ivActionBarLeftButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (llActionBarLeftButton != null) {
                llActionBarLeftButton.setVisibility(View.INVISIBLE);
            }
            if (ivActionBarLeftButton != null) {
                ivActionBarLeftButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    protected void gotoActivity(final Intent intent) {
        if ((PurchaseInfo.doRemoveAd() & 0x5) != 0) {
            startActivity(intent);
        } else {
            adController.show(new Runnable() {
                @Override
                public void run() {
                    startActivity(intent);
                }
            });
        }
    }

    protected void gotoActivityForResult(final Intent intent, final int requestCode) {
        if ((PurchaseInfo.doRemoveAd()&0x5) != 0) {
            startActivityForResult(intent, requestCode);
        } else {
            adController.show(new Runnable() {
                @Override
                public void run() {
                    startActivityForResult(intent, requestCode);
                }
            });
        }
    }

    protected void setActionBarRightButtonEnabled(boolean enabled) {
        LinearLayout llActionBarRightButton = (LinearLayout) findViewById(R.id.ll_actionbar_right_button);
        ImageView ivActionBarRightButton = (ImageView) findViewById(R.id.iv_actionbar_right_button);
        if (enabled) {
            if (llActionBarRightButton != null) {
                llActionBarRightButton.setVisibility(View.VISIBLE);
            }
            if (ivActionBarRightButton != null) {
                ivActionBarRightButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (llActionBarRightButton != null) {
                llActionBarRightButton.setVisibility(View.INVISIBLE);
            }
            if (ivActionBarRightButton != null) {
                ivActionBarRightButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    protected void setActionBarLeftButtonDrawable(int drawableRes) {
        ImageView ivActionBarLeftButton = (ImageView) findViewById(R.id.iv_actionbar_left_button);
        if (ivActionBarLeftButton != null) {
            ivActionBarLeftButton.setImageResource(drawableRes);
        }
    }

    protected void setActionBarRightButtonDrawable(int drawableRes) {
        ImageView ivActionBarRightButton = (ImageView) findViewById(R.id.iv_actionbar_right_button);
        if (ivActionBarRightButton != null) {
            ivActionBarRightButton.setImageResource(drawableRes);
        }
    }

    protected void setupDrawer() {

//        mDrawer = (NavigationView) findViewById(R.id.main_drawer);
//        mDrawer.setNavigationItemSelectedListener(this);
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//
//        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.drawer_open, R.string.drawer_close);
//        mDrawerLayout.setDrawerListener(drawerToggle);
//
//        drawerToggle.syncState();
//        mDrawer.setItemIconTintList(null);
//
//        Menu m = mDrawer.getMenu();
//        for (int i = 0; i < m.size(); i++) {
//            MenuItem mi = m.getItem(i);
//
//            //for aapplying a font to subMenu ...
//            SubMenu subMenu = mi.getSubMenu();
//            if (subMenu != null && subMenu.size() > 0) {
//                for (int j = 0; j < subMenu.size(); j++) {
//                    MenuItem subMenuItem = subMenu.getItem(j);
//                    applyFontToMenuItem(subMenuItem);
//                }
//            }
//
//            //the method we have create in activity
//            applyFontToMenuItem(mi);
//        }
    }

    protected void openDrawer() {

        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }

//    protected void closeDrawer() {
//        mDrawerLayout.closeDrawer(GravityCompat.END);
//    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = FontCache.getTypefaceNotoSans_Bold(this);
        SpannableString newTitle = new SpannableString(mi.getTitle());
        newTitle.setSpan(new CustomTypefaceSpan("", font), 0, newTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(newTitle);
    }
}

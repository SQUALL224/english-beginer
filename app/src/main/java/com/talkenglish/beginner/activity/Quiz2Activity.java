package com.talkenglish.beginner.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.AudioPlayer;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.model.Quiz;
import com.talkenglish.beginner.statmodel.Quiz2Stat;
import com.talkenglish.beginner.util.SharedPref;
import com.talkenglish.beginner.widget.QuizOptionContainerLayout;
import com.talkenglish.beginner.widget.QuizSentenceView;

import java.util.ArrayList;

public class Quiz2Activity extends ActionbarBaseActivity {
    private static final String TAG = "Quiz2Activity";

    public static final int QUIZ2 = 0x107;

    protected int screenWidth;
    protected int screenHeight;

    private Lesson lesson;
    private ArrayList<Quiz> quizzes;

    private int session;

    QuizSentenceView qzsView;
    QuizOptionContainerLayout qoclContainer;
    LinearLayout llSentences;
    ImageView ivCheck;
    ImageView ivRetry;
    LinearLayout llCheckResult;
    FrameLayout qzrView;
    ImageView iv_quiz_result;
    TextView tvQuizResultText;
    TextView tvQuizResultPoint;
    LinearLayout llNext;
    TextView tvNext;

    LinearLayout rootView;

    Quiz2Stat stat;
    AudioPlayer audioPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stat = new Quiz2Stat();
        buildLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            buildLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            buildLayout();
        }
    }

    protected int getOrientation() {
        if (screenWidth > screenHeight) {
            return Configuration.ORIENTATION_LANDSCAPE;
        } else if (screenHeight > screenWidth) {
            return Configuration.ORIENTATION_PORTRAIT;
        }
        return Configuration.ORIENTATION_UNDEFINED;
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback_white);
        int type = Lesson.QUIZ;
        boolean bookmark = lesson.getBookmark(this, type);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.QUIZ;
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    public void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.WHITE);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {

        setContentView(R.layout.activity_quiz2);

        screenWidth = EngLearningBeginnerApp.screenWidth(this);
        screenHeight = EngLearningBeginnerApp.screenHeight(this);
        lesson = (Lesson) getIntent().getExtras().get("lesson");
        session = getIntent().getExtras().getInt("session");

        quizzes = lesson.getQuizzes2();

        qoclContainer = (QuizOptionContainerLayout) findViewById(R.id.qocl_container);
        llSentences = (LinearLayout) findViewById(R.id.ll_sentences);
        ivCheck = (ImageView) findViewById(R.id.iv_check);
        ivRetry = (ImageView) findViewById(R.id.iv_retry);
        llCheckResult = (LinearLayout) findViewById(R.id.ll_check_result);
        qzrView = (FrameLayout) findViewById(R.id.qzrView);
        iv_quiz_result = (ImageView) findViewById(R.id.iv_quiz1_result);
        tvQuizResultText = (TextView) findViewById(R.id.tv_quiz1_result_text);
        tvQuizResultPoint = (TextView) findViewById(R.id.tv_quiz1_result_point);
        tvQuizResultPoint.setVisibility(View.GONE);
        llNext = (LinearLayout) findViewById(R.id.ll_next);

        rootView = (LinearLayout) findViewById(R.id.rootView);

        buildToolbar();

        tvNext = (TextView) findViewById(R.id.tv_next_button);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tvNext.getText() != "Complete") {
                    if (stat.currentPos + 1 < lesson.numOfQuizzes2()) {
                        stat.next();
                    }
                    hideRibbonByAnim();
                }
                else
                {
                    if (lesson.calculateStars() == 0) {
                        Intent intent = new Intent();
                        intent.putExtra("lesson", lesson);
                        intent.putExtra("passed", false);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        Intent intent = new Intent();
                        lesson.setSection(Quiz2Activity.this, 4);
                        intent.putExtra("lesson", lesson);
                        intent.putExtra("passed", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    Analytics.sendEvent(getApplication(), "Next Pressed in Quiz2", lesson.getTitle());
                }
            }
        });

        setTitle("Quiz");

        loadData();

        Analytics.sendScreenName(getApplication(), "Quiz2 Screen" + lesson.toString());
    }

    private void refreshSentenceView() {
        Quiz quiz = lesson.getQuiz2(stat.currentPos);
        llSentences.removeAllViews();
        qzsView = new QuizSentenceView(this);
        qzsView.setEditable(true);
        qzsView.setQuizWordEnteredListener(new QuizSentenceView.QuizWordEnteredListener() {
            @Override
            public void onTextEntered(EditText editText) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                qzsView.refresh(getOrientation() == Configuration.ORIENTATION_LANDSCAPE ? screenWidth / 2 : screenWidth);
                ivCheck.setEnabled(qzsView.getQuiz().canCheck());
            }
        });
        qzsView.setEntry(getOrientation() == Configuration.ORIENTATION_LANDSCAPE ? screenWidth / 2 : screenWidth, quiz);
        qzsView.setBackgroundResource(R.drawable.clickable_overlay_transparent_bg);
        llSentences.addView(qzsView);
    }
    private void loadData() {
        rootView.setBackgroundColor(Color.parseColor("#00a24f"));
        ivRetry.setVisibility(View.GONE);
        ivCheck.setVisibility(View.VISIBLE);
        llCheckResult.setVisibility(View.GONE);
        qoclContainer.setEntry(getOrientation() == Configuration.ORIENTATION_LANDSCAPE ? screenWidth / 2 : screenWidth, quizzes);
        refreshSentenceView();

        ivCheck.setEnabled(!stat.busy && lesson.getQuiz2(stat.currentPos).canCheck());
        tvNext.setEnabled(false);
//        tvNext.setEnabled(lesson.wasQuiz2Taken());

        boolean guidePlayed = SharedPref.getBoolean(this, "guideVideoPlayed2", false);
        if (lesson.getNumber() == 1 && (EngLearningBeginnerApp.guideVideoPlayed&1)==0 && !guidePlayed) {
            EngLearningBeginnerApp.guideVideoPlayed++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent myIntent = new Intent(Quiz2Activity.this, VideoActivity.class);
                    myIntent.putExtra("video", "2");
                    startActivity(myIntent);
                    overridePendingTransition(R.anim.move_in_up, R.anim.move_out_fade);
                }
            }, 1000);
        }
    }

    public void onClickCheck(View v) {
        qzsView.checkResult();
        int point = qzsView.getQuiz().getPoint();
        int point0 = 0;
        if (lesson.wasQuiz2Taken()) {
            for (Quiz quiz : quizzes) {
                point0 += quiz.getPoint();
            }
            lesson.setPointForQuiz2(Quiz2Activity.this, point0 * 40);
        }
        showRibbonByAnim(point);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                hideRibbonByAnim();
//            }
//        }, 2000);
        Analytics.sendEvent(getApplication(), "Check Pressed in Quiz2", lesson.getTitle());
    }

    public void onClickRetry(View v) {
//        qzsView.checkResult();
//        int point = qzsView.getQuiz().getPoint();
//        int point0 = 0;
//        if (lesson.wasQuiz2Taken()) {
//            for (Quiz quiz : quizzes) {
//                point0 += quiz.getPoint();
//            }
//            lesson.setPointForQuiz2(Quiz2Activity.this, point0 * 40);
//        }
//        showRibbonByAnim(point);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
        rootView.setBackgroundColor(Color.parseColor("#00a24f"));
        ivCheck.setVisibility(View.VISIBLE);
        ivRetry.setVisibility(View.GONE);
//        Animation in = AnimationUtils.loadAnimation(this, R.anim.scale_in_fade);
//        qzrView.startAnimation(in);
        llCheckResult.setVisibility(View.GONE);
//            }
//        }, 2000);
//        Analytics.sendEvent(getApplication(), "Check Pressed in Quiz2", lesson.getTitle());
    }

    public void hideRibbonByAnim() {
//        Animation in = AnimationUtils.loadAnimation(this, R.anim.scale_out_fade);
//        qzrView.startAnimation(in);
//        in.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
                try {
                    AudioPlayer.stop(audioPlayer);
                    stat.busy = false;
                    qzrView.setVisibility(View.INVISIBLE);
                    rootView.setBackgroundColor(Color.parseColor("#00a24f"));
                    loadData();
                    if (lesson.wasQuiz2Taken()) {
                        tvNext.setText("Complete");
                        tvNext.setEnabled(true);
                        final RelativeLayout rlCompletedMark = (RelativeLayout) findViewById(R.id.rl_completed_mark);
                        final RelativeLayout rlFailedMark = (RelativeLayout) findViewById(R.id.rl_failed_mark);
                        String filename = "session_completed.mp3";
                        if (lesson.calculateStars() == 0) {
                            rlFailedMark.setVisibility(View.VISIBLE);
                            filename = "try_again.wav";
                        } else {
                            rlCompletedMark.setVisibility(View.VISIBLE);
                            filename = "session_completed.mp3";
                        }
                        AssetFileDescriptor descriptor = null;
                        AudioPlayer.stop(audioPlayer);
                        if (lesson.calculateStars() == 0) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    rlFailedMark.setVisibility(View.GONE);
                                    AudioPlayer.stop(audioPlayer);
                                }
                            }, 2500);
                        }
                        try {

                            descriptor = getAssets().openFd(filename);
                            long start = descriptor.getStartOffset();
                            long end = descriptor.getLength();
                            audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, lesson.calculateStars() == 0 ? null : new AudioPlayer.ProgressUpdateListener() {
                                @Override
                                public void onProgress(int current, int duration) {

                                }

                                @Override
                                public void onProgress(float progress) {

                                }

                                @Override
                                public void onComplete() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rlCompletedMark.setVisibility(View.GONE);
                                        }
                                    }, 1500);
                                }
                            });
                        } catch (Exception e) {
                            Log.e(TAG, "Opening asset file failed - congratulations.mp3");
                        }
                    }
                } catch (Exception e) {

                }
//            }

//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
    }
    private AudioPlayer playResult(Context context, boolean result) {
        AssetFileDescriptor descriptor = null;
        AudioPlayer.stop(audioPlayer);
        try {
            String filename = "";
            if (result) {
                filename = "tada.wav";
            } else {
                filename = "critical.wav";
            }
            descriptor = context.getAssets().openFd(filename);
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            return AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, null);

        } catch (Exception e) {
            Log.e(TAG, "Opening asset file failed - congratulations.mp3");
            return null;
        }
    }
    public void showRibbonByAnim(int point) {
        stat.busy = true;
        ivCheck.setEnabled(!stat.busy && lesson.getQuiz2(stat.currentPos).canCheck());
        qzrView.setVisibility(View.VISIBLE);

        if (point > 0) {
            audioPlayer = playResult(this, true);
//            qzrView.setImageResource(R.drawable.quiz2_correct);
            iv_quiz_result.setImageResource(R.drawable.quiz_great);
            tvQuizResultText.setText("Correct");
            rootView.setBackgroundColor(Color.parseColor("#0063a2"));
            qzrView.setBackgroundColor(Color.parseColor("#0063a2"));
        } else {
            audioPlayer = playResult(this, false);
//            qzrView.setImageResource(R.drawable.quiz2_incorrect);
            iv_quiz_result.setImageResource(R.drawable.quiz_try);
            tvQuizResultText.setText("Incorrect");
            rootView.setBackgroundColor(Color.parseColor("#757575"));
            qzrView.setBackgroundColor(Color.parseColor("#757575"));
        }
        ivCheck.setVisibility(View.GONE);
        ivRetry.setVisibility(View.VISIBLE);
//        Animation in = AnimationUtils.loadAnimation(this, R.anim.scale_in_fade);
//        qzrView.startAnimation(in);
        llCheckResult.setVisibility(View.VISIBLE);
        tvNext.setEnabled(true);
    }
}

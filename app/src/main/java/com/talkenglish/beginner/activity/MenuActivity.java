package com.talkenglish.beginner.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.talkenglish.beginner.BuildConfig;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.ad.InterstitialAdController;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.util.PurchaseInfo;

/**
 * Created by SQUALL on 10/13/2016.
 */

public class MenuActivity extends AppCompatActivity{

    private InterstitialAdController adController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_view);
        adController = new InterstitialAdController(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void buildLayout() {

    }

    public void OnClose(View v) {
        finish();
    }

    public void OnBookmark(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Bookmark' Selected", toString());
        Intent i = new Intent(this, BookmarkActivity.class);
        gotoActivity(i);
        finish();
    }

    public void OnRemoveAds(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Remove Ads' Selected", toString());

        Intent i = new Intent(this, PurchaseActivity.class);
        gotoActivity(i);
        finish();
    }

    public void OnInstructions(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Instructions' Selected", toString());
        Intent i = new Intent(this, InstructionsActivity.class);
        gotoActivity(i);
        finish();
    }

    public void OnContact(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Contact Us' Selected", toString());
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "talkenglish@talkenglish.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " Android");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
        finish();
    }

    public void OnRecommended(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Recommended Apps' Selected", toString());
        Intent i = new Intent(this, RecommendedAppsActivity.class);
        gotoActivity(i);
        finish();
    }

    public void OnVisit(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Visit Website' Selected", toString());
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("http://www.talkenglish.com"));
        startActivity(i);
        finish();
    }

    public void OnShare(View v) {
        Analytics.sendEvent(getApplication(), "Menu 'Share App' Selected", toString());
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Beginner));
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_title)));
        finish();
    }

    protected void gotoActivity(final Intent intent) {
        if ((PurchaseInfo.doRemoveAd() & 0x5) != 0) {
            startActivity(intent);
            finish();
        } else {
            adController.show(new Runnable() {
                @Override
                public void run() {
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}

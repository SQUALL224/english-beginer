package com.talkenglish.beginner.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.AudioPlayer;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.model.Quiz;
import com.talkenglish.beginner.util.FontCache;
import com.talkenglish.beginner.util.SharedPref;
import com.talkenglish.beginner.widget.NotoSBoldTextView;
import com.talkenglish.beginner.widget.Quiz1ResultView;
import com.talkenglish.beginner.widget.QuizOptionContainerLayout;
import com.talkenglish.beginner.widget.QuizOptionView;
import com.talkenglish.beginner.widget.QuizSentenceView;

import java.util.ArrayList;

public class Quiz1Activity extends ActionbarBaseActivity {
    private static final String TAG = "Quiz1Activity";

    public static final int QUIZ1 = 0x106;

    protected int screenWidth;
    protected int screenHeight;

    private Lesson lesson;
    private ArrayList<Quiz> quizzes = null;
    private int session;
    ArrayList<QuizSentenceView> qzsViews;
    QuizOptionContainerLayout qoclContainer;
    LinearLayout llSentences;
    RelativeLayout llDragger;
    ImageView ivCheck;
    ImageView ivRetry;
    LinearLayout llCheckResult;
    FrameLayout qzrView;
    ImageView ivQuizResult;
    NotoSBoldTextView tvQuizResultText;
    NotoSBoldTextView tvQuizResultPoint;
    LinearLayout llNext;
    TextView tvNext;
    LinearLayout rootView;

    AudioPlayer audioPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AudioPlayer.stop(audioPlayer);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == Quiz2Activity.QUIZ2) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            buildLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            buildLayout();
        }
    }

    protected int getOrientation() {
        if (screenWidth > screenHeight) {
            return Configuration.ORIENTATION_LANDSCAPE;
        } else if (screenHeight > screenWidth) {
            return Configuration.ORIENTATION_PORTRAIT;
        }
        return Configuration.ORIENTATION_UNDEFINED;
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback_white);
        int type = Lesson.QUIZ;
        boolean bookmark = lesson.getBookmark(this, type);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.QUIZ;
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {

        setContentView(R.layout.activity_quiz1);

        rootView = (LinearLayout) findViewById(R.id.rootView);

        screenWidth = EngLearningBeginnerApp.screenWidth(this);
        screenHeight = EngLearningBeginnerApp.screenHeight(this);
        lesson = (Lesson) getIntent().getExtras().get("lesson");
        session = getIntent().getExtras().getInt("session");
        quizzes = lesson.getQuizzes1();
        qoclContainer = (QuizOptionContainerLayout)findViewById(R.id.qocl_container);
        llSentences = (LinearLayout) findViewById(R.id.ll_sentences);
        llDragger = (RelativeLayout) findViewById(R.id.ll_dragger);
        ivCheck = (ImageView) findViewById(R.id.iv_check);
        ivRetry = (ImageView) findViewById(R.id.iv_retry);
        llCheckResult = (LinearLayout) findViewById(R.id.ll_check_result);
        qzrView = (FrameLayout) findViewById(R.id.qzrView);
        ivQuizResult = (ImageView) findViewById(R.id.iv_quiz1_result);
        tvQuizResultText = (NotoSBoldTextView) findViewById(R.id.tv_quiz1_result_text);
        tvQuizResultPoint = (NotoSBoldTextView) findViewById(R.id.tv_quiz1_result_point);
        llNext = (LinearLayout) findViewById(R.id.ll_next);

        buildToolbar();

        tvNext = (TextView) findViewById(R.id.tv_complete_to_next_button);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lesson.getNumber() == 1) {
                    EngLearningBeginnerApp.guideVideoPlayed = 0;
                }
                lesson.loadQuizzes2(Quiz1Activity.this);
                Intent i = new Intent(Quiz1Activity.this, Quiz2Activity.class);
                i.putExtra("lesson", lesson);
                i.putExtra("session", 0x31);
                gotoActivityForResult(i, Quiz2Activity.QUIZ2);
                Analytics.sendEvent(getApplication(), "Next Pressed in Quiz1", lesson.getTitle());

            }
        });

        setTitle("Quiz");
        loadData();

        Analytics.sendScreenName(getApplication(), "Quiz1 Screen" + lesson.toString());
    }

    public void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.WHITE);
    }

    private void loadData() {
        qoclContainer.setEntry(getOrientation() == Configuration.ORIENTATION_LANDSCAPE ? screenWidth / 2 : screenWidth,
                quizzes);
        refreshSentenceView();
        boolean guidePlayed = SharedPref.getBoolean(this, "guideVideoPlayed3", false);
        if (lesson.getNumber() == 1 && (EngLearningBeginnerApp.guideVideoPlayed&1)==0 && !guidePlayed) {
            EngLearningBeginnerApp.guideVideoPlayed ++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent myIntent = new Intent(Quiz1Activity.this, VideoActivity.class);
                    myIntent.putExtra("video", "3");
                    startActivity(myIntent);
                    overridePendingTransition(R.anim.move_in_up, R.anim.move_out_fade);
                }
            }, 1000);
        }
    }

    private void refreshSentenceView() {
        qzsViews = new ArrayList<>();
        llSentences.removeAllViews();
        for (int i = 0; i < lesson.numOfQuizzes1(); i ++) {
            Quiz quiz = lesson.getQuiz1(i);
            QuizSentenceView qsv = new QuizSentenceView(this);
            qsv.setEntry(getOrientation() == Configuration.ORIENTATION_LANDSCAPE ? screenWidth / 2 : screenWidth,
                    quiz);
            llSentences.addView(qsv);
            qzsViews.add(qsv);
        }
        ivCheck.setEnabled(lesson.canCheckQuiz1());
        tvNext.setEnabled(lesson.wasQuiz1Taken());
    }

    public void onClickCheck(View v) {
        int point = 0;
        for (QuizSentenceView qzsView : this.qzsViews) {
            qzsView.checkResult();
            point += qzsView.getQuiz().getPoint();
        }
        lesson.setPointForQuiz1(Quiz1Activity.this, point*20);
        showRibbonByAnim(point);
        Analytics.sendEvent(getApplication(), "Check Pressed in Quiz1", lesson.getTitle());
    }

    public void onClickRetry(View v) {
        hideRibbonByAnim();
    }

    public void hideRibbonByAnim() {
//        Animation in = AnimationUtils.loadAnimation(this, R.anim.scale_out_fade);
//        qzrView.startAnimation(in);
        ivRetry.setVisibility(View.GONE);
        ivCheck.setVisibility(View.VISIBLE);
        qzrView.setVisibility(View.GONE);
        rootView.setBackgroundColor(Color.parseColor("#ff00a24f"));
//        in.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                try {
//                    llCheckResult.setVisibility(View.GONE);
//                    loadData();
//                    AudioPlayer.stop(audioPlayer);
//                } catch (Exception e) {
//
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
    }

    private AudioPlayer playResult(Context context, int result) {
        AssetFileDescriptor descriptor = null;
        AudioPlayer.stop(audioPlayer);
        try {
            String filename = "";
            if (result == 2) {
                filename = "tada.wav";
            } else if (result == 1){
                filename = "tada1.wav";
            } else {
                filename = "failure.mp3";
            }
            descriptor = context.getAssets().openFd(filename);
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            return AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, null);

        } catch (Exception e) {
            Log.e(TAG, "Opening asset file failed - congratulations.mp3");
            return null;
        }
    }

    public void showRibbonByAnim(int point) {
//        qzrView.setPoint(lesson.numOfQuizzes1(), point);

        ivCheck.setVisibility(View.GONE);
        ivRetry.setVisibility(View.VISIBLE);
        tvNext.setEnabled(lesson.wasQuiz1Taken());
        if (point >= 0 && point < 5) {
            audioPlayer = playResult(this, 0);
            ivQuizResult.setImageResource(R.drawable.quiz_try);
            tvQuizResultText.setText("Keep trying");
            rootView.setBackgroundColor(Color.parseColor("#ff757575"));
            qzrView.setBackgroundColor(Color.parseColor("#ff757575"));
        } else if (point >= 5 && point < 9) {
            audioPlayer = playResult(this, 1);
            ivQuizResult.setImageResource(R.drawable.quiz_good);
            tvQuizResultText.setText("Good");
            rootView.setBackgroundColor(Color.parseColor("#ffa29000"));
            qzrView.setBackgroundColor(Color.parseColor("#ffa29000"));
        } else if (point >= 9) {
            audioPlayer = playResult(this, 2);
            ivQuizResult.setImageResource(R.drawable.quiz_great);
            tvQuizResultText.setText("Great!");
            rootView.setBackgroundColor(Color.parseColor("#ff0063a2"));
            qzrView.setBackgroundColor(Color.parseColor("#ff0063a2"));
        } else {
            audioPlayer = playResult(this, 0);
        }
        tvQuizResultPoint.setText(point + "/" + 10);
        qzrView.setVisibility(View.VISIBLE);
        llCheckResult.setVisibility(View.VISIBLE);
//        Animation in = AnimationUtils.loadAnimation(this, R.anim.scale_in_fade);
//        qzrView.startAnimation(in);
//        llCheckResult.setVisibility(View.VISIBLE);
    }

    public boolean checkMatches(Rect rect, View v) {
        boolean ret = false;
        int square = 0;
        for (QuizSentenceView qzsView : this.qzsViews) {
            Rect outRect = new Rect();
            int location[] = new int[2];
            qzsView.getDrawingRect(outRect);
            qzsView.getLocationOnScreen(location);
            outRect.offset(location[0], location[1]);
            if (outRect.intersect(rect)) {
                qzsView.setBackgroundColor(Color.LTGRAY);
                int square1 = outRect.width() * outRect.height();
                if (square < square1) {
                    square = square1;
                }
            } else {
                qzsView.setBackgroundColor(Color.TRANSPARENT);
            }
        }
        boolean isFound = false;
        for (QuizSentenceView qzsView : this.qzsViews) {
            Rect outRect = new Rect();
            int location[] = new int[2];
            qzsView.getDrawingRect(outRect);
            qzsView.getLocationOnScreen(location);
            outRect.offset(location[0], location[1]);
            if (outRect.intersect(rect)) {
                int square1 = outRect.width() * outRect.height();
                if (square == square1 && !isFound) {
                    if (v != null) {
                        Log.e("hihihi", "v is not null");
                        if (v instanceof QuizOptionView) {
                            QuizOptionView wov = (QuizOptionView) v;
                            qzsView.setBackgroundColor(Color.TRANSPARENT);
                            Quiz quiz = qzsView.getQuiz();
                            quiz.setCandidate(wov.getText().toString().trim().toLowerCase());

                            qzsView.refresh(getOrientation() == Configuration.ORIENTATION_LANDSCAPE ? screenWidth / 2 : screenWidth);
                        }
                    } else {
                        qzsView.setBackgroundColor(Color.LTGRAY);
                    }
                    isFound = true;
                    ret = true;
                } else {
                    qzsView.setBackgroundColor(Color.TRANSPARENT);
                }
            } else {
                qzsView.setBackgroundColor(Color.TRANSPARENT);
            }
        }
        return ret;
    }

    @SuppressLint("NewApi")
    public void startDragging(final View v, int wordIndex) {
        ArrayList<Quiz> quizs = lesson.getQuizzes1();
        Quiz quiz = quizs.get(wordIndex);

        ClipData.Item item = new ClipData.Item("Word " + String.valueOf(wordIndex));
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData dragData = new ClipData("Word " + String.valueOf(wordIndex), mimeTypes, item);

        // Instantiates the drag shadow builder.
        final Lesson1DragShadowBuilder myShadow = new Lesson1DragShadowBuilder(v, Quiz1Activity.this);

        // Starts the drag
        llDragger.startDrag(dragData, // the data to be dragged
                myShadow, // the drag shadow builder
                null, // no need to use local data
                0 // flags (not currently used, set to 0)
        );

        llDragger.setOnDragListener(new Quiz1DragListener(myShadow, v, quiz));
    }

    @SuppressLint("NewApi")
    class Quiz1DragListener implements View.OnDragListener {
        Lesson1DragShadowBuilder shadowBuilder;
        Quiz quiz;
        View draggingView;

        public Quiz1DragListener(Lesson1DragShadowBuilder shadowBuilder, View draggingView, Quiz quiz) {
            this.shadowBuilder = shadowBuilder;
            this.draggingView = draggingView;
            this.quiz = quiz;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    Log.d(TAG, "Action is DragEvent.ACTION_DRAG_STARTED");
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    Log.d(TAG, "Action is DragEvent.ACTION_DRAG_ENTERED");
                    break;
                case DragEvent.ACTION_DRAG_EXITED: {
                    Log.d(TAG, "Action is DragEvent.ACTION_DRAG_EXITED");

                    break;
                }
                case DragEvent.ACTION_DRAG_LOCATION: {
                    Log.d(TAG, "Action is DragEvent.ACTION_DRAG_LOCATION - " + v.getWidth() + ", " + v.getHeight());

                    int location[] = new int[2];

                    int cy = shadowBuilder.getView().getHeight();
                    int cx = shadowBuilder.getView().getWidth();
                    llDragger.getLocationOnScreen(location);

                    Rect rect = new Rect((int) event.getX() + location[0] - cx / 2, (int) event.getY() + location[1] - cy * 3 / 2,
                            (int) event.getX() + location[0] + cx - cx / 2, (int) event.getY() + location[1] + cy - cy * 3 / 2);

                    checkMatches(rect, null);

                    break;
                }
                case DragEvent.ACTION_DRAG_ENDED: {
                    Log.d(TAG, "Action is DragEvent.ACTION_DRAG_ENDED");
                    break;
                }
                case DragEvent.ACTION_DROP:
                    Log.d(TAG, "ACTION_DROP event");
                    int location[] = new int[2];

                    int cy = shadowBuilder.getView().getHeight();
                    int cx = shadowBuilder.getView().getWidth();
                    llDragger.getLocationOnScreen(location);

                    Rect rect = new Rect((int) event.getX() + location[0] - cx / 2, (int) event.getY() + location[1] - cy * 3 / 2,
                            (int) event.getX() + location[0] + cx - cx / 2, (int) event.getY() + location[1] + cy - cy * 3 / 2);

                    checkMatches(rect, draggingView);
                    ivCheck.setEnabled(lesson.canCheckQuiz1());
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    @SuppressLint("NewApi")
    class Lesson1DragShadowBuilder extends View.DragShadowBuilder {
        private Drawable shadow;

        public Lesson1DragShadowBuilder(View v, Context context) {
            super(v);

            Resources resources = context.getResources();
            shadow = resources.getDrawable(R.drawable.ic_answer_bg);
        }

        @Override
        public void onProvideShadowMetrics(Point size, Point touch) {
            int width = getView().getWidth();
            int height = getView().getHeight();

            shadow.setBounds(0, 0, width, height);
            size.set(width, height);
            touch.set((int) (width * 1.5f) / 2, height * 4 / 2);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            shadow.draw(canvas);
            int x = getView().getWidth();
            int y = getView().getHeight();
            String text = "";
            if (getView() instanceof QuizOptionView) {
                QuizOptionView wov = (QuizOptionView) getView();
                text = wov.getText().toString();
            }
            drawText(canvas, text, x / 2.0f, y / 2.0f, y / 2.0f);
        }

        private void drawText(Canvas canvas, String text, float x, float y, float textAreaHeight) {
            float textHeight = textAreaHeight * 0.9f;
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setTextSize(textHeight);
            paint.setTypeface(FontCache.getTypefaceNotoSans_Regular(Quiz1Activity.this));
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setAntiAlias(true);
            Rect textRect = new Rect();
            paint.getTextBounds(text, 0, text.length(), textRect);

            Paint paint1 = new Paint();
            paint1.setColor(Color.BLUE);
            canvas.drawText(text, x, y - 1 - (paint.descent() + paint.ascent()) / 2, paint);
        }
    }
}

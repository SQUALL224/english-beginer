package com.talkenglish.beginner.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.dialog.DownloadStatusDialog;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.AudioPlayer;
import com.talkenglish.beginner.env.FileProvider;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.util.ConnectionMonitor;
import com.talkenglish.beginner.widget.LessonStartMainView;

public class LessonStartActivity extends ActionbarBaseActivity {
    private static final String TAG = "LessonStartActivity";

    public static final int START_LESSON = 0x101;
    public static final int CHECK_FINAL = 0x109;

    private ImageView ivBackground;
    private LessonStartMainView mainView;
    private TextView tvNext;
    private AudioPlayer audioPlayer;
    private Lesson lesson;
    private boolean final0 = false;

    DownloadStatusDialog dialog = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AudioPlayer.stop(audioPlayer);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == CheckUnderstoodActivity.CHECK_VALIDATION) {
            AudioPlayer.stop(this.audioPlayer);
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            buildLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            buildLayout();
        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback_white);
        setActionBarRightButtonEnabled(true);
        int type = Lesson.FINAL_CHECK;
        boolean bookmark = lesson.getBookmark(this, type);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.FINAL_CHECK;
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        AudioPlayer.stop(this.audioPlayer);
        super.onBackPressed();
    }

    private void buildLayout() {
        setContentView(R.layout.activity_lesson_start);

        ivBackground = (ImageView) findViewById(R.id.img_background);
        ivBackground.setScaleType(ImageView.ScaleType.CENTER_CROP);

        lesson = (Lesson) getIntent().getExtras().get("lesson");
        final0 = (boolean) getIntent().getExtras().get("final");
        buildToolbar();
        tvNext = (TextView) findViewById(R.id.tv_next_button);
        tvNext.setEnabled(lesson.getListenedCount()>0);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioPlayer.stop(audioPlayer);

                Intent i = new Intent(LessonStartActivity.this, CheckUnderstoodActivity.class);
                i.putExtra("lesson", lesson);
                i.putExtra("final", final0);
                gotoActivityForResult(i, CheckUnderstoodActivity.CHECK_VALIDATION);
                Analytics.sendEvent(getApplication(), "Next Pressed", lesson.getTitle());

            }
        });
        if (final0) {
            setTitle("Final Check");
        } else {
            setTitle(lesson.getTitle());
        }
        loadData(lesson);

        Analytics.sendScreenName(getApplication(), "Lesson Start Screen");
    }

    public void setTitle(String title)
    {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.WHITE);
    }

    public void setBackgroundImage(String imageUrl) {
        if (imageUrl == null || imageUrl == "") {
            return;
        }
        Picasso.with(this)
                .load(imageUrl)
                .into(this.ivBackground);
    }

    private void loadData(final Lesson lesson) {
        setBackgroundImage(lesson.getImageUri(this, lesson.getMainImage()));
        TextView tvMainText = (TextView) findViewById(R.id.tv_main_text);
        mainView = (LessonStartMainView) findViewById(R.id.rl_lesson_main);
        tvMainText.setText(lesson.getMainText());
        mainView.setImage(lesson.getImageUri(this, lesson.getMainImage()));
        mainView.enablePlayButton(lesson.getListenedCount()<3);
        tvNext.setEnabled(lesson.getListenedCount()>0);
        mainView.setOnPlayClickListener(new LessonStartMainView.OnPlayClickListener() {
            @Override
            public void onClick(View v) {
                tvNext.setEnabled(true);
                playMainAudio();
            }
        });

        if (final0) {
            tvMainText.setVisibility(View.VISIBLE);
        } else {
            tvMainText.setVisibility(View.GONE);
        }
        final EngLearningBeginnerApp app = (EngLearningBeginnerApp)getApplication();
        if (!app.wasLessonPrepared(lesson.getNumber())) {
            if (!ConnectionMonitor.isNetworkAvailable(this)) {
                new AlertDialog.Builder(LessonStartActivity.this)
                        .setTitle("Error")
                        .setMessage("Please check your Internet connection.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                onBackPressed();
                            }
                        })
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        onBackPressed();
                    }
                }).show();
                return;
            }
            if (!app.isDownloaderRunning()) {
                app.startDownload();
            }
            dialog = DownloadStatusDialog.showDialog(this);
            app.setDownloadListener(new EngLearningBeginnerApp.DownloadListener() {
                @Override
                public void onComplete(int lesson_number) {
                    if (lesson_number == lesson.getNumber()) {
                        mainView.setImage(lesson.getImageUri(LessonStartActivity.this, lesson.getMainImage()));
                        DownloadStatusDialog.hideDialog(dialog);
                        app.setDownloadListener(null);
                        return;
                    } else {
                        dialog.setProgress(0);
                        app.setDownloadListener(this);

                    }
                }

                @Override
                public void onProgress(int max, int current) {
                    Log.e("xxxxx", String.valueOf(max) + " -> " + String.valueOf(current));
                    if (dialog != null) {
                        dialog.setProgress((float) current / (float) max);
                    } else {
                        dialog = DownloadStatusDialog.showDialog(LessonStartActivity.this);
                        dialog.setProgress((float) current / (float) max);
                    }
                }

                @Override
                public void onFail(String error) {
                    DownloadStatusDialog.hideDialog(dialog);
                    new AlertDialog.Builder(LessonStartActivity.this)
                            .setTitle("Error")
                            .setMessage("Please check your Internet connection.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onBackPressed();
                                }
                            })
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    onBackPressed();
                                }
                            }).show();
                }
            });
        }
    }

    public void playMainAudio() {
        AudioPlayer.stop(audioPlayer);
        if (lesson.isAudioInAssets()) {
            AssetFileDescriptor descriptor = null;
            try {
                descriptor = getAssets().openFd("audio/" + lesson.getMainAudio());
                long start = descriptor.getStartOffset();
                long end = descriptor.getLength();
                audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new AudioProgressUpdateListener());

                if (audioPlayer != null) {
                    lesson.increaseListenedCount();
                    mainView.enablePlayButton(lesson.getListenedCount()<3);
                }
            } catch (Exception e) {
                audioPlayer = null;
                new AlertDialog.Builder(LessonStartActivity.this)
                        .setMessage("Opening asset file failed - " + lesson.getMainAudio())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                Log.e(TAG, "Opening asset file failed - " + lesson.getMainAudio());
            }
        } else {
            audioPlayer = AudioPlayer.play(FileProvider.getPath(this, lesson.getMainAudio()), new AudioProgressUpdateListener());
            if (audioPlayer != null) {
                lesson.increaseListenedCount();
                mainView.enablePlayButton(lesson.getListenedCount()<3);
            }
        }
    }

    class AudioProgressUpdateListener implements AudioPlayer.ProgressUpdateListener {

        @Override
        public void onProgress(int current, int duration) {
            float progress;
            if (duration == 0) {
                progress = 0f;
            } else {
                progress = Math.max(0f, Math.min(1f, (float)current / (float)duration));
            }
            try {
                mainView.showProgressIndicator();
                mainView.setProgress(progress);

            } catch (Exception e) {

            }
        }

        @Override
        public void onProgress(float progress) {
            try {
                mainView.showProgressIndicator();
                mainView.setProgress(progress);

            } catch (Exception e) {

            }
        }

        @Override
        public void onComplete() {
            mainView.hideProgressIndicator();
            AudioPlayer.stop(audioPlayer);
        }
    }
}

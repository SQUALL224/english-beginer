package com.talkenglish.beginner.activity;
import com.talkenglish.beginner.env.AudioPlayer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.FileProvider;
import com.talkenglish.beginner.model.Exercise;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.statmodel.ExerciseStat;
import com.talkenglish.beginner.widget.NotoSBoldTextView;
import com.talkenglish.beginner.widget.SessionValidateMainView;

import java.util.ArrayList;

public class ExerciseActivity extends ActionbarBaseActivity {
    private static final String TAG = "ExerciseActivity";

    public static final int SESSION_EXERCISE = 0x104;

    SessionValidateMainView mainView;
    ImageView ivCheck;
    TextView tvStepLabel;

    TextView tvCompleteToNext;
    TextView tvSkip;

    AudioPlayer audioPlayer;
    AudioPlayer audioPlayer0;
    private Lesson lesson;
    private int session;
    ExerciseStat stat;

    NotoSBoldTextView tvSessionTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stat = new ExerciseStat();
        buildLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AudioPlayer.stop(audioPlayer);
        AudioPlayer.stop(audioPlayer0);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == ListeningActivity.SESSION_LISTENING) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            buildLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            buildLayout();
        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback_white);
        int type = Lesson.STUDY_SESSION1;
        if ((session&0x10) != 0) {
            type = Lesson.STUDY_SESSION1;
        } else if ((session&0x20) != 0) {
            type = Lesson.STUDY_SESSION2;
        }
        boolean bookmark = lesson.getBookmark(this, type);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.STUDY_SESSION1;
        if ((session&0x10) != 0) {
            type = Lesson.STUDY_SESSION1;
        } else if ((session&0x20) != 0) {
            type = Lesson.STUDY_SESSION2;
        }
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {
        setContentView(R.layout.activity_exercise);

        lesson = (Lesson) getIntent().getExtras().get("lesson");
        session = getIntent().getExtras().getInt("session");

        buildToolbar();

        mainView = (SessionValidateMainView) findViewById(R.id.rl_session_main);
        tvStepLabel = (TextView) findViewById(R.id.tv_step_label);
        ivCheck = (ImageView) findViewById(R.id.iv_check);
        final RelativeLayout rlCompletedMark = (RelativeLayout) findViewById(R.id.rl_completed_mark);
        tvSessionTitle = (NotoSBoldTextView) findViewById(R.id.session_study_title);
        tvSkip = (TextView) findViewById(R.id.tv_skip_to_next_button);
        tvSkip.setEnabled(true);
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (session == 0x11) {
                    lesson.setSection(ExerciseActivity.this, 2);
                    lesson.setPointForSession1(ExerciseActivity.this);

                    rlCompletedMark.setVisibility(View.VISIBLE);
                    AssetFileDescriptor descriptor = null;
                    AudioPlayer.stop(audioPlayer);
                    try {
                        String filename = "session_completed.mp3";
                        descriptor = getAssets().openFd(filename);
                        long start = descriptor.getStartOffset();
                        long end = descriptor.getLength();
                        audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new AudioPlayer.ProgressUpdateListener() {
                            @Override
                            public void onProgress(int current, int duration) {

                            }

                            @Override
                            public void onProgress(float progress) {

                            }

                            @Override
                            public void onComplete() {
                                Intent intent = new Intent();
                                intent.putExtra("lesson", lesson);
                                intent.putExtra("session", 0x20);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });

                    } catch (Exception e) {
                        Log.e(TAG, "Opening asset file failed - congratulations.mp3");
                    }
                } else {
                    lesson.loadListens(ExerciseActivity.this, session ^ 0x01);
                    Intent i = new Intent(ExerciseActivity.this, ListeningActivity.class);
                    i.putExtra("lesson", lesson);
                    i.putExtra("session", session ^ 0x01);
                    gotoActivityForResult(i, ListeningActivity.SESSION_LISTENING);
                }
                Analytics.sendEvent(getApplication(), "Skip Pressed in Study Session - Validating " + session, lesson.getTitle());
            }
        });
        tvCompleteToNext = (TextView) findViewById(R.id.tv_complete_to_next_button);
        tvCompleteToNext.setEnabled(false);
        tvCompleteToNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session == 0x11) {
                    lesson.setSection(ExerciseActivity.this, 2);
                    lesson.setPointForSession1(ExerciseActivity.this);

                    rlCompletedMark.setVisibility(View.VISIBLE);
                    AssetFileDescriptor descriptor = null;
                    AudioPlayer.stop(audioPlayer);
                    try {
                        String filename = "session_completed.mp3";
                        descriptor = getAssets().openFd(filename);
                        long start = descriptor.getStartOffset();
                        long end = descriptor.getLength();
                        audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new AudioPlayer.ProgressUpdateListener() {
                            @Override
                            public void onProgress(int current, int duration) {

                            }

                            @Override
                            public void onProgress(float progress) {

                            }

                            @Override
                            public void onComplete() {
                                Intent intent = new Intent();
                                intent.putExtra("lesson", lesson);
                                intent.putExtra("session", 0x20);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });

                    } catch (Exception e) {
                        Log.e(TAG, "Opening asset file failed - congratulations.mp3");
                    }
                } else {
                    lesson.loadListens(ExerciseActivity.this, session ^ 0x01);
                    Intent i = new Intent(ExerciseActivity.this, ListeningActivity.class);
                    i.putExtra("lesson", lesson);
                    i.putExtra("session", session ^ 0x01);
                    gotoActivityForResult(i, ListeningActivity.SESSION_LISTENING);
                }
                Analytics.sendEvent(getApplication(), "Complete Pressed in Study Session - Validating " + session, lesson.getTitle());

            }
        });

        mainView.setOnSelectedListener(new SessionValidateMainView.OnSelectedListener() {
            @Override
            public void onSelected(int selected) {
                stat.selected = selected;
                ivCheck.setEnabled(true);
            }
        });

        if ((session&0x10) != 0) {
            setTitle("Study Session 1/2");
        } else if ((session&0x20) != 0) {
            setTitle("Study Session 2/2");
        }
        loadData(lesson, session);

        Analytics.sendScreenName(getApplication(), "Session Study Screen - Validating " + session);
    }

    public void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.WHITE);
    }

    private void loadData(final Lesson lesson, final int session) {
        updateViews();
    }

    private void updateViews() {
        tvCompleteToNext.setEnabled(stat.completed);
        tvSkip.setEnabled(!stat.completed);
//        if (stat.completed) {
            findViewById(R.id.rl_next_container).setVisibility(View.VISIBLE);
//        } else {
//            findViewById(R.id.rl_next_container).setVisibility(View.INVISIBLE);
//        }
        Exercise exercise = lesson.exerciseAt(session, stat.currentPos);
        ArrayList<Exercise> exercises = lesson.getExercises(session);
        String uri1 = lesson.getImageUri(this, exercises.get(0).getImage());
        String uri2 = lesson.getImageUri(this, exercises.get(1).getImage());
        String uri3 = lesson.getImageUri(this, exercises.get(2).getImage());
        String uri4 = lesson.getImageUri(this, exercises.get(3).getImage());
        mainView.init();
        mainView.setImages(uri1, uri2, uri3, uri4);
//        mainView.setText(exercise.getText());
        mainView.setEnabled(!stat.busy);
        mainView.setSelectedIndex(stat.selected);
        if (stat.busy) {
            mainView.showResult(stat.selected, stat.correct);
        }
        tvStepLabel.setText(String.valueOf(stat.currentPos+1));
        ivCheck.setEnabled(stat.selected > -1 && !stat.busy);

        tvSessionTitle.setText(exercise.getText());
    }

    public void onClickCheck(View v) {
        ArrayList<Exercise> exercises = lesson.getExercises(session);
        stat.busy = true;
        if (exercises.get(stat.selected).getOrder() == lesson.exerciseAt(session, stat.currentPos).getOrder()) {
            stat.correct = true;
            audioPlayer0 = playResult(ExerciseActivity.this, true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!listenAudio()) {
                        stat.selected = -1;
                        stat.busy = false;
                        updateViews();
                    }
                }
            }, 800);
        } else {
            stat.correct = false;
            audioPlayer0 = playResult(ExerciseActivity.this, false);
        }
        updateViews();

        Analytics.sendEvent(getApplication(), "Check Pressed in Study Session - Validating " + session, lesson.getTitle());
    }

    private AudioPlayer playResult(Context context, boolean result) {
        AssetFileDescriptor descriptor = null;
        AudioPlayer.stop(audioPlayer);
        try {
            String filename = "";
            if (result) {
                filename = "tada.wav";
            } else {
                filename = "critical.wav";
            }
            descriptor = context.getAssets().openFd(filename);
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            return AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end,
                    result ? null : new ResultProgressUpdateListener(result));

        } catch (Exception e) {
            Log.e(TAG, "Opening asset file failed - congratulations.mp3");
            return null;
        }
    }

    public boolean listenAudio() {

        Exercise exercise = lesson.exerciseAt(session, stat.currentPos);
        AudioPlayer.stop(audioPlayer);
        if (lesson.isAudioInAssets()) {
            AssetFileDescriptor descriptor = null;
            try {
                descriptor = getAssets().openFd("audio/" + exercise.getAudio());
                long start = descriptor.getStartOffset();
                long end = descriptor.getLength();
                audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new ListeningProgressUpdateListener());

                if (audioPlayer != null) {
                }
            } catch (Exception e) {
                audioPlayer = null;
                new AlertDialog.Builder(ExerciseActivity.this)
                        .setMessage("Opening asset file failed - " + exercise.getAudio())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                Log.e(TAG, "Opening asset file failed - " + exercise.getAudio());
            }
        } else {
            audioPlayer = AudioPlayer.play(FileProvider.getPath(this, exercise.getAudio()), new ListeningProgressUpdateListener());
            if (audioPlayer != null) {
            }
        }
        return (audioPlayer != null);
    }

    class ResultProgressUpdateListener implements AudioPlayer.ProgressUpdateListener {
        boolean result;
        public ResultProgressUpdateListener(boolean result) {
            this.result = result;
        }

        @Override
        public void onProgress(int current, int duration) {
        }

        @Override
        public void onProgress(float progress) {
        }

        @Override
        public void onComplete() {
            AudioPlayer.stop(audioPlayer0);
            if (result) {
                if (!listenAudio()) {
                    stat.selected = -1;
                    stat.busy = false;
                    updateViews();
                }
            } else {
                stat.selected = -1;
                stat.busy = false;
                updateViews();
            }
        }
    }

    class ListeningProgressUpdateListener implements AudioPlayer.ProgressUpdateListener {

        @Override
        public void onProgress(int current, int duration) {
            float progress;
            if (duration == 0) {
                progress = 0f;
            } else {
                progress = Math.max(0f, Math.min(1f, (float) current / (float) duration));
            }
            try {
                mainView.showProgress(stat.selected, progress);

            } catch (Exception e) {

            }
        }

        @Override
        public void onProgress(float progress) {

            try {
                mainView.showProgress(stat.selected, progress);

            } catch (Exception e) {

            }
        }

        @Override
        public void onComplete() {
            mainView.showProgress(stat.selected, 1);
            AudioPlayer.stop(audioPlayer);
            if (stat.currentPos < 3) {
                stat.next();
            } else {
                stat.completed = true;
                stat.selected = -1;
                stat.busy = false;
            }
            updateViews();
        }
    }
}

package com.talkenglish.beginner.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.util.SharedPref;

public class VideoActivity extends Activity {

    private VideoView videoView;
    private LinearLayout llContainer;
    private String videoNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video);
        videoView = (VideoView) findViewById(R.id.videoView);
        llContainer = (LinearLayout) findViewById(R.id.ll_video_container);

        String videofile = (String)getIntent().getExtras().get("video");
        this.videoNumber = videofile;

        String path = "android.resource://" + getPackageName() + "/raw/task1";
        if (videofile != null && videofile.equals("2")) {
            path = "android.resource://" + getPackageName() + "/raw/task2";
        } else if (videofile != null && videofile.equals("3")) {
            path = "android.resource://" + getPackageName() + "/raw/task3";
        }
        videoView.setVideoURI(Uri.parse(path));
        videoView.start();
        findViewById(R.id.tv_play_button).setEnabled(false);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                findViewById(R.id.tv_play_button).setEnabled(true);
                videoView.seekTo(0);
                videoView.invalidate();
            }
        });
    }

    public void onClickOK(View v) {
        SharedPref.putBoolean(this, "guideVideoPlayed" + this.videoNumber, true);
        finish();
        overridePendingTransition(R.anim.move_in_fade, R.anim.move_out_down);
    }
    public void onClickPlay(View v) {
        videoView.start();
        findViewById(R.id.tv_play_button).setEnabled(false);
    }
    @Override
    public void onBackPressed() {
//        finish();
//        overridePendingTransition(R.anim.move_in_fade, R.anim.move_out_down);
    }
    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (llContainer == null) {
            llContainer = (LinearLayout) findViewById(R.id.ll_video_container);
        }
        LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) llContainer.getLayoutParams();
        String weightS = getString(R.string.video_width_weight);
        param.weight = Double.valueOf(weightS).floatValue();
        llContainer.setLayoutParams(param);

        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }
}

package com.talkenglish.beginner.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.model.Score;
import com.talkenglish.beginner.uimodel.LevelUI;
import com.talkenglish.beginner.util.SharedPref;
import com.talkenglish.beginner.widget.ItemCellLayout;

import java.util.List;

public class HomeActivity extends ActionbarBaseActivity {
    private static final String TAG = "HomeActivity";

    private List<LevelUI> levelUIs;
    private ListView lvLevels;

    private LinearLayout llLevels;
    private int level = 0;

    protected int screenWidth;
    protected int screenHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        screenWidth = EngLearningBeginnerApp.screenWidth(this);
        screenHeight = EngLearningBeginnerApp.screenHeight(this);

    }

    protected int getOrientation() {
        if (screenWidth > screenHeight) {
            return Configuration.ORIENTATION_LANDSCAPE;
        } else if (screenHeight > screenWidth) {
            return Configuration.ORIENTATION_PORTRAIT;
        }
        return Configuration.ORIENTATION_UNDEFINED;
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == LessonsListActivity.LESSONS_LIST) {
            Bundle bundle = data.getExtras();
            final int level = (int) bundle.get("level");
            this.level = level + 1;
            HomeActivity.setStudyingLevel(this, level+1);
            if (level >= 9) {
//                Intent i = new Intent(this, RecommendedAppsActivity.class);
//                gotoActivity(i);
            } else {
//                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
//
//                final LevelUI levelUI = LevelUI.getLevel(this, level + 1);
//                i.putExtra("title", levelUI.getTitle());
//                i.putExtra("level", levelUI.getLevel());
//                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        }
    }

    public static int getStudyingLevel(Context context) {
        return SharedPref.getInt(context, "studying_level", 1);
    }

    public static void setStudyingLevel(Context context, int level) {
        SharedPref.putInt(context, "studying_level", level);
    }

    private int getScore(int level) {
        if (level < 1 || level > 9) {
            return 0;
        }
        int stars = 0;
        for (int i = 1; i <= 10; i ++) {
            Score score = new Score(String.format("%02d", (level-1)*10+i));
            score.load(this);
            stars += score.getStars(this);
        }
        return stars;
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_hamburger);
        setActionBarRightButtonDrawable(R.drawable.ic_menu_bookmark_off);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
//        openDrawer();
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        Intent i = new Intent(HomeActivity.this, MenuActivity.class);
        startActivity(i);
    }

    private void buildLayout() {

        buildToolbar();

        levelUIs = LevelUI.getLevels(this);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Analytics.sendScreenName(getApplication(), "Main Screen");
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        int studyingLevel = HomeActivity.getStudyingLevel(this);

        final ImageView ivMainPage = (ImageView)findViewById(R.id.iv_main_menu);

        ItemCellLayout icll_basic = (ItemCellLayout) findViewById(R.id.icll_basic);
        icll_basic.setStatus(levelUIs.get(1));
        icll_basic.setIcon(R.drawable.ic_beginer);
        icll_basic.setTitle(getString(R.string.str_baic));
        icll_basic.setScore(getScore(levelUIs.get(1).getLevel()));
        icll_basic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Very Basic");
                LevelUI levelUI = levelUIs.get(1);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout iclleasy = (ItemCellLayout) findViewById(R.id.icll_easy);
        iclleasy.setStatus(levelUIs.get(2));
        iclleasy.setIcon(R.drawable.ic_easy);
        iclleasy.setTitle(getString(R.string.str_easy));
        iclleasy.setScore(getScore(levelUIs.get(2).getLevel()));
        iclleasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Easy");
                LevelUI levelUI = levelUIs.get(2);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout icllsimply = (ItemCellLayout) findViewById(R.id.icll_simply);
        icllsimply.setStatus(levelUIs.get(3));
        icllsimply.setIcon(R.drawable.ic_simply);
        icllsimply.setTitle(getString(R.string.str_simply));
        icllsimply.setScore(getScore(levelUIs.get(3).getLevel()));
        icllsimply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Beginner III");
                LevelUI levelUI = levelUIs.get(3);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout icllmedi = (ItemCellLayout) findViewById(R.id.icll_medi);
        icllmedi.setStatus(levelUIs.get(4));
        icllmedi.setIcon(R.drawable.ic_medi);
        icllmedi.setTitle(getString(R.string.str_medi));
        icllmedi.setScore(getScore(levelUIs.get(4).getLevel()));
        icllmedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Intermediate I");
                LevelUI levelUI = levelUIs.get(4);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout icllmiddle = (ItemCellLayout) findViewById(R.id.icll_middle);
        icllmiddle.setStatus(levelUIs.get(5));
        icllmiddle.setIcon(R.drawable.ic_middle);
        icllmiddle.setTitle(getString(R.string.str_hardly));
        icllmiddle.setScore(getScore(levelUIs.get(5).getLevel()));
        icllmiddle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Intermediate II");
                LevelUI levelUI = levelUIs.get(5);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout icllmedium = (ItemCellLayout) findViewById(R.id.icll_medium);
        icllmedium.setStatus(levelUIs.get(6));
        icllmedium.setIcon(R.drawable.ic_medium);
        icllmedium.setTitle(getString(R.string.str_diffi));
        icllmedium.setScore(getScore(levelUIs.get(6).getLevel()));
        icllmedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Advanced");
                LevelUI levelUI = levelUIs.get(6);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout icllhardly = (ItemCellLayout) findViewById(R.id.icll_hardly);
        icllhardly.setStatus(levelUIs.get(7));
        icllhardly.setIcon(R.drawable.ic_hardly);
        icllhardly.setTitle(getString(R.string.str_hardly));
        icllhardly.setScore(getScore(levelUIs.get(7).getLevel()));
        icllhardly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Advanced");
                LevelUI levelUI = levelUIs.get(7);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout iclldiffi = (ItemCellLayout) findViewById(R.id.icll_diffi);
        iclldiffi.setStatus(levelUIs.get(8));
        iclldiffi.setIcon(R.drawable.ic_diffi);
        iclldiffi.setTitle(getString(R.string.str_diffi));
        iclldiffi.setScore(getScore(levelUIs.get(8).getLevel()));
        iclldiffi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Advanced");
                LevelUI levelUI = levelUIs.get(8);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });

        ItemCellLayout icllfinal = (ItemCellLayout) findViewById(R.id.icll_final);
        icllfinal.setStatus(levelUIs.get(9));
        icllfinal.setIcon(R.drawable.ic_final);
        icllfinal.setTitle(getString(R.string.str_final));
        icllfinal.setScore(getScore(levelUIs.get(9).getLevel()));
        icllfinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(), "Main Page Item Pressed", "Advanced");
                LevelUI levelUI = levelUIs.get(9);
                Intent i = new Intent(HomeActivity.this, LessonsListActivity.class);
                i.putExtra("title", levelUI.getTitle());
                i.putExtra("level", levelUI.getLevel());
                HomeActivity.this.level = 0;
                HomeActivity.setStudyingLevel(HomeActivity.this, levelUI.getLevel());
                gotoActivityForResult(i, LessonsListActivity.LESSONS_LIST);
            }
        });
    }
}

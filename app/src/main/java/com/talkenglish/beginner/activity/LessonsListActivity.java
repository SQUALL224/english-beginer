package com.talkenglish.beginner.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.DBHelper;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.widget.LessonNormalPane;

import java.util.ArrayList;
import java.util.List;

public class LessonsListActivity extends ActionbarBaseActivity {
    private static final String TAG = "LessonsListActivity";
    public static final int LESSONS_LIST = 0x200;

    private ArrayList<Lesson> lessons;
    private ListView lvLessons;
    LessonsListAdapter adapter;
    private int number = -1;
    private int session = 0x00;

    LinearLayout parentStudyingPane;
    TextView tvTitle1;
    TextView tvStartLesson;
    ImageView ivStudySession1;
    ImageView ivStudySession2;
    ImageView ivQuiz;
    ImageView ivFinalCheck;
    ImageView ivStudySession10;
    ImageView ivStudySession20;
    ImageView ivQuiz0;
    ImageView ivFinalCheck0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons_list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == LessonStartActivity.START_LESSON) {
            Bundle bundle = data.getExtras();
            final Lesson entry = (Lesson) bundle.get("lesson");
            final int session = (int) bundle.get("session");
            if (session == 0x30) {
                this.number = entry.getNumber();
                this.session = 0x30;
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                entry.loadQuizzes1(LessonsListActivity.this);
                Intent i = new Intent(LessonsListActivity.this, Quiz1Activity.class);
                i.putExtra("lesson", entry);
                i.putExtra("session", 0x30);
                if (entry.getNumber() == 1) {
                    EngLearningBeginnerApp.guideVideoPlayed = 0;
                }
                gotoActivityForResult(i, Quiz1Activity.QUIZ1);
            } else if (session == 0x40) {
                int index = entry.getNumber();
                int level = entry.getLevel();
                entry.contract(this);
                if (index / 10 + 1 > level) {
                    Intent intent = new Intent();
                    intent.putExtra("level", level);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                }

            } else {
                this.number = entry.getNumber();
                this.session = 0x10;
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                entry.loadListens(LessonsListActivity.this, session);
                Intent i = new Intent(LessonsListActivity.this, ListeningActivity.class);
                i.putExtra("lesson", entry);
                i.putExtra("session", session);
                gotoActivityForResult(i, ListeningActivity.SESSION_LISTENING);
            }
        } else if (requestCode == ListeningActivity.SESSION_LISTENING) {
            Bundle bundle = data.getExtras();
            final Lesson entry = (Lesson) bundle.get("lesson");
            final int session = (int) bundle.get("session");
            if (session == 0x30) {
                this.number = entry.getNumber();
                this.session = session;
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
//                entry.loadQuizzes1(LessonsListActivity.this);
//                Intent i = new Intent(LessonsListActivity.this, Quiz1Activity.class);
//                i.putExtra("lesson", entry);
//                i.putExtra("session", 0x30);
//                gotoActivityForResult(i, Quiz1Activity.QUIZ1);
            } else {
                this.number = entry.getNumber();
                this.session = session;
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
//                entry.loadListens(LessonsListActivity.this, session);
//                Intent i = new Intent(LessonsListActivity.this, ListeningActivity.class);
//                i.putExtra("lesson", entry);
//                i.putExtra("session", session);
//                gotoActivityForResult(i, ListeningActivity.SESSION_LISTENING);
            }
        } else if (requestCode == Quiz1Activity.QUIZ1) {
            Bundle bundle = data.getExtras();
            final Lesson entry = (Lesson) bundle.get("lesson");
            final boolean passed = (boolean) bundle.get("passed");
            if (passed) {
                this.number = entry.getNumber();
                this.session = 0x40;
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
//                Intent i = new Intent(LessonsListActivity.this, LessonStartActivity.class);
//                i.putExtra("lesson", entry);
//                i.putExtra("final", true);
//                gotoActivityForResult(i, LessonStartActivity.START_LESSON);
            } else {
//                Toast.makeText(LessonsListActivity.this, "You didn't pass the quizs, try again please.", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == LessonStartActivity.CHECK_FINAL) {

        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback);
        setActionBarRightButtonDrawable(R.drawable.ic_hamburger);
    }

    @Override
    public String toString() {
        return TAG;
    }

    @Override
    public void onActionBarRightButtonClicked(View v) {
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setDataToLeftPane(Lesson lesson)
    {
        int section = lesson.getSection();

        tvTitle1.setText(lesson.getTitle());

        if (lesson.wasLessonCompleted(LessonsListActivity.this)) {
            tvStartLesson.setText("Completed! - " + String.format("%03d/500", lesson.getPoint()));
            ivStudySession1.setEnabled(true);
            ivStudySession2.setEnabled(true);
            ivQuiz.setEnabled(true);
            ivFinalCheck.setEnabled(true);
            ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
            ivStudySession2.setImageResource(R.drawable.ic_study_session2_finish);
            ivQuiz.setImageResource(R.drawable.ic_quiz_finish);
            ivFinalCheck.setImageResource(R.drawable.ic_final_check_finish);
            return;
        }
        if (section == 0) {
            tvStartLesson.setText("Start Lesson!");
            ivStudySession1.setEnabled(false);
            ivStudySession2.setEnabled(false);
            ivQuiz.setEnabled(false);
            ivFinalCheck.setEnabled(false);
        } else if (section == 1) {
            tvStartLesson.setText("Studying...");
            ivStudySession1.setEnabled(true);
            ivStudySession2.setEnabled(false);
            ivQuiz.setEnabled(false);
            ivFinalCheck.setEnabled(false);
            ivStudySession10.setVisibility(View.VISIBLE);
        } else if (section == 2) {
            tvStartLesson.setText("Studying...");
            ivStudySession1.setEnabled(true);
            ivStudySession2.setEnabled(true);
            ivQuiz.setEnabled(false);
            ivFinalCheck.setEnabled(false);
            ivStudySession20.setVisibility(View.VISIBLE);
            ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
        } else if (section == 3) {
            tvStartLesson.setText("Studying...");
            ivStudySession1.setEnabled(true);
            ivStudySession2.setEnabled(true);
            ivQuiz.setEnabled(true);
            ivFinalCheck.setEnabled(false);
            ivQuiz0.setVisibility(View.VISIBLE);
            ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
            ivStudySession2.setImageResource(R.drawable.ic_study_session2_finish);
        } else if (section == 4) {
            tvStartLesson.setText("Studying...");
            ivStudySession1.setEnabled(true);
            ivStudySession2.setEnabled(true);
            ivQuiz.setEnabled(true);
            ivFinalCheck.setEnabled(true);
            ivFinalCheck0.setVisibility(View.VISIBLE);
            ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
            ivStudySession2.setImageResource(R.drawable.ic_study_session2_finish);
            ivQuiz.setImageResource(R.drawable.ic_quiz_finish);
        }
    }

    private void buildLayout() {
        buildToolbar();

        final String title = getIntent().getExtras().getString("title");

        final int level = getIntent().getExtras().getInt("level");
        DBHelper dbHelper = DBHelper.getInstance(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        lessons = Lesson.load(this, db, level);

        lvLessons = (ListView) findViewById(R.id.lv_lessons);

        setTitle(title);
        adapter = new LessonsListAdapter(this, lessons);
        lvLessons.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < lessons.size(); i ++) {
                    final Lesson entry = lessons.get(i);
                    if (entry.isLessonStudying(LessonsListActivity.this)) {
                        lvLessons.smoothScrollToPosition(i+1);
                        break;
                    }
                }

            }
        }, 800);
//        setupDrawer();

        parentStudyingPane = (LinearLayout) findViewById(R.id.ll_studying_pane);
        tvTitle1 = (TextView) findViewById(R.id.tv_title11);
        tvStartLesson = (TextView) findViewById(R.id.tv_start_lesson);
        ivStudySession1 = (ImageView) findViewById(R.id.iv_study_session1);
        ivStudySession2 = (ImageView) findViewById(R.id.iv_study_session2);
        ivQuiz = (ImageView) findViewById(R.id.iv_quiz);
        ivFinalCheck = (ImageView) findViewById(R.id.iv_final_check);
        ivStudySession10 = (ImageView) findViewById(R.id.iv_study_session10);
        ivStudySession20 = (ImageView) findViewById(R.id.iv_study_session20);
        ivQuiz0 = (ImageView) findViewById(R.id.iv_quiz0);
        ivFinalCheck0 = (ImageView) findViewById(R.id.iv_final_check0);

        Analytics.sendScreenName(getApplication(), "Lesson's List Screen");
    }

    public class LessonsListAdapter extends ArrayAdapter<Lesson> {

        List<Lesson> lessons;

        Context parentView;

        int prevExpandPosition = 1;

        public LessonsListAdapter(Context context, List<Lesson> lessons) {
            super(context, R.layout.lessons_list_item);
            this.lessons = lessons;
            parentView = context;
        }

        public int getCount() {
            return lessons.size() + 1;
        }

        public Lesson getItem(int index) {
            if (index == 0) {
                return null;
            }
            return lessons.get(index - 1);
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(LessonsListActivity.this).inflate(R.layout.lessons_list_item, parent, false);
                holder.llNormalPane = (LessonNormalPane) convertView.findViewById(R.id.ll_normal_pane);
                holder.llStudyingPane = (LinearLayout) convertView.findViewById(R.id.ll_studying_pane);
                holder.llStudyingPaneContractor = (LinearLayout) convertView.findViewById(R.id.ll_studying_pane_contractor);
                holder.tvTitle1 = (TextView) convertView.findViewById(R.id.tv_title1);
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                {
                    holder.tvStartLesson = (TextView) convertView.findViewById(R.id.tv_start_lesson);
                    holder.ivStudySession1 = (ImageView) convertView.findViewById(R.id.iv_study_session1);
                    holder.ivStudySession2 = (ImageView) convertView.findViewById(R.id.iv_study_session2);
                    holder.ivQuiz = (ImageView) convertView.findViewById(R.id.iv_quiz);
                    holder.ivFinalCheck = (ImageView) convertView.findViewById(R.id.iv_final_check);
                    holder.ivStudySession10 = (ImageView) convertView.findViewById(R.id.iv_study_session10);
                    holder.ivStudySession20 = (ImageView) convertView.findViewById(R.id.iv_study_session20);
                    holder.ivQuiz0 = (ImageView) convertView.findViewById(R.id.iv_quiz0);
                    holder.ivFinalCheck0 = (ImageView) convertView.findViewById(R.id.iv_final_check0);
                }
                holder.ivExpand = (View) convertView.findViewById(R.id.item_expand);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (position == 0) {
                holder.llNormalPane.setVisibility(View.GONE);
                holder.llStudyingPane.setVisibility(View.GONE);
//                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
//                    ((LessonsListActivity)parentView).parentStudyingPane.setVisibility(View.INVISIBLE);
                return convertView;
            }

            final Lesson entry = getItem(position);

            entry.loadSection(LessonsListActivity.this);
            entry.loadScore(LessonsListActivity.this);
            holder.llNormalPane.setEnabled(entry.canStudy(LessonsListActivity.this));
            if (entry.isExpanded(LessonsListActivity.this)) {
                holder.llStudyingPane.setVisibility(View.VISIBLE);
                holder.llNormalPane.setVisibility(View.GONE);
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                {
                    if (entry.getNumber() == LessonsListActivity.this.number) {
                        anim_count = 0;
                        LessonsListActivity.this.number = -1;
                        if (LessonsListActivity.this.session == 0x10) {
                            showRibbonByAnim(holder.ivStudySession10);
                        } else if (LessonsListActivity.this.session == 0x20) {
                            showRibbonByAnim(holder.ivStudySession20);
                        } else if (LessonsListActivity.this.session == 0x30) {
                            showRibbonByAnim(holder.ivQuiz0);
                        } else if (LessonsListActivity.this.session == 0x40) {
                            showRibbonByAnim(holder.ivFinalCheck0);
                        }
                    }
                }
                else
                {
                    ((LessonsListActivity)parentView).parentStudyingPane.setVisibility(View.VISIBLE);
                    ((LessonsListActivity)parentView).setDataToLeftPane(entry);
                    if (entry.getNumber() == LessonsListActivity.this.number) {
                        anim_count = 0;
                        LessonsListActivity.this.number = -1;
                        if (LessonsListActivity.this.session == 0x10) {
                            showRibbonByAnim(((LessonsListActivity)parentView).ivStudySession10);
                        } else if (LessonsListActivity.this.session == 0x20) {
                            showRibbonByAnim(((LessonsListActivity)parentView).ivStudySession20);
                        } else if (LessonsListActivity.this.session == 0x30) {
                            showRibbonByAnim(((LessonsListActivity)parentView).ivQuiz0);
                        } else if (LessonsListActivity.this.session == 0x40) {
                            showRibbonByAnim(((LessonsListActivity)parentView).ivFinalCheck0);
                        }
                    }
                }
            } else {
                holder.llStudyingPane.setVisibility(View.GONE);
                holder.llNormalPane.setVisibility(View.VISIBLE);
//                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
//                    ((LessonsListActivity)parentView).parentStudyingPane.setVisibility(View.INVISIBLE);
            }
            if (entry.isLessonStudying(LessonsListActivity.this)) {
//                lvLessons.smoothScrollToPosition(position);
            }
            holder.llNormalPane.setTitle(entry.getTitle());
            holder.llNormalPane.setEntry(LessonsListActivity.this, entry);
            holder.tvTitle1.setText(entry.getTitle());

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                enableSections(holder, entry);

            holder.llNormalPane.setTag(position);
            holder.llNormalPane.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Lesson prevEntry = getItem(prevExpandPosition);
                    prevEntry.contract(LessonsListActivity.this);
                    int clickedPosition = (int)v.getTag();
                    Lesson clickedentry = getItem(clickedPosition);
                    clickedentry.expand(LessonsListActivity.this);
                    if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                    {
                        ((LessonsListActivity) parentView).setDataToLeftPane(clickedentry);
                        ((LessonsListActivity) parentView).parentStudyingPane.setVisibility(View.VISIBLE);
                    }
                    prevExpandPosition = position;
                    adapter.notifyDataSetChanged();
                    Analytics.sendEvent(getApplication(), "Expand pressed", clickedentry.getTitle());
                }
            });
            holder.llStudyingPaneContractor.setTag(position);
            holder.llStudyingPaneContractor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int clickedPosition = (int)v.getTag();
                    Lesson clickedentry = getItem(clickedPosition);
                    clickedentry.contract(LessonsListActivity.this);
                     if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                        ((LessonsListActivity)parentView).parentStudyingPane.setVisibility(View.INVISIBLE);
                    adapter.notifyDataSetChanged();
                    Analytics.sendEvent(getApplication(), "Contract pressed", clickedentry.getTitle());
                }
            });

            final View.OnClickListener startLessonListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    entry.setStudying(LessonsListActivity.this);
                    Intent i = new Intent(LessonsListActivity.this, LessonStartActivity.class);
                    i.putExtra("lesson", entry);
                    i.putExtra("final", false);
                    gotoActivityForResult(i, LessonStartActivity.START_LESSON);
                    Analytics.sendEvent(getApplication(), "Lesson Start Pressed", entry.getTitle());
                }
            };

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                holder.tvStartLesson.setOnClickListener(startLessonListener);
            else
                ((LessonsListActivity)parentView).tvStartLesson.setOnClickListener(startLessonListener);
            final View.OnClickListener session1Listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    entry.setStudying(LessonsListActivity.this);
                    entry.loadListens(LessonsListActivity.this, 0x10);
                    Intent i = new Intent(LessonsListActivity.this, ListeningActivity.class);
                    i.putExtra("lesson", entry);
                    i.putExtra("session", 0x10);
                    Analytics.sendEvent(getApplication(), "Study Session 1/2 Pressed", entry.getTitle());
                    gotoActivityForResult(i, ListeningActivity.SESSION_LISTENING);
                }
            };
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                holder.ivStudySession1.setOnClickListener(session1Listener);
                holder.ivStudySession10.setOnClickListener(session1Listener);
            }
            else {
                ((LessonsListActivity)parentView).ivStudySession1.setOnClickListener(session1Listener);
                ((LessonsListActivity)parentView).ivStudySession10.setOnClickListener(session1Listener);
            }
            final View.OnClickListener session2Listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    entry.setStudying(LessonsListActivity.this);
                    entry.loadListens(LessonsListActivity.this, 0x20);
                    Intent i = new Intent(LessonsListActivity.this, ListeningActivity.class);
                    i.putExtra("lesson", entry);
                    i.putExtra("session", 0x20);
                    gotoActivityForResult(i, ListeningActivity.SESSION_LISTENING);
                    Analytics.sendEvent(getApplication(), "Study Session 2/2 Pressed", entry.getTitle());
                }
            };

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                holder.ivStudySession2.setOnClickListener(session2Listener);
                holder.ivStudySession20.setOnClickListener(session2Listener);
            }
            else {
                ((LessonsListActivity)parentView).ivStudySession2.setOnClickListener(session2Listener);
                ((LessonsListActivity)parentView).ivStudySession20.setOnClickListener(session2Listener);
            }

            View.OnClickListener quizListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    entry.setStudying(LessonsListActivity.this);
                    entry.loadQuizzes1(LessonsListActivity.this);
                    Intent i = new Intent(LessonsListActivity.this, Quiz1Activity.class);
                    i.putExtra("lesson", entry);
                    i.putExtra("session", 0x30);
                    if (entry.getNumber() == 1) {
                        EngLearningBeginnerApp.guideVideoPlayed = 0;
                    }
                    gotoActivityForResult(i, Quiz1Activity.QUIZ1);
                    Analytics.sendEvent(getApplication(), "Quiz Pressed", entry.getTitle());
                }
            };

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                holder.ivQuiz.setOnClickListener(quizListener);
                holder.ivQuiz0.setOnClickListener(quizListener);
            }
            else {
                ((LessonsListActivity)parentView).ivQuiz.setOnClickListener(quizListener);
                ((LessonsListActivity)parentView).ivQuiz0.setOnClickListener(quizListener);
            }

            final View.OnClickListener finalListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    entry.setStudying(LessonsListActivity.this);
                    Intent i = new Intent(LessonsListActivity.this, LessonStartActivity.class);
                    i.putExtra("lesson", entry);
                    i.putExtra("final", true);
                    gotoActivityForResult(i, LessonStartActivity.START_LESSON);
                    Analytics.sendEvent(getApplication(), "Final Check", entry.getTitle());
                }
            };

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            {
                holder.ivFinalCheck.setOnClickListener(finalListener);
                holder.ivFinalCheck0.setOnClickListener(finalListener);
            }
            else {
                ((LessonsListActivity)parentView).ivFinalCheck.setOnClickListener(finalListener);
                ((LessonsListActivity)parentView).ivFinalCheck0.setOnClickListener(finalListener);
            }
            return convertView;
        }

        void enableSections(final ViewHolder holder, final Lesson lesson) {
            int section = lesson.getSection();

            if (lesson.wasLessonCompleted(LessonsListActivity.this)) {
                holder.tvStartLesson.setText("Completed! - " + String.format("%03d/500", lesson.getPoint()));
                holder.ivStudySession1.setEnabled(true);
                holder.ivStudySession2.setEnabled(true);
                holder.ivQuiz.setEnabled(true);
                holder.ivFinalCheck.setEnabled(true);
                holder.ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
                holder.ivStudySession2.setImageResource(R.drawable.ic_study_session2_finish);
                holder.ivQuiz.setImageResource(R.drawable.ic_quiz_finish);
                holder.ivFinalCheck.setImageResource(R.drawable.ic_final_check_finish);
                return;
            }
            if (section == 0) {
                holder.tvStartLesson.setText("Start Lesson!");
                holder.ivStudySession1.setEnabled(false);
                holder.ivStudySession2.setEnabled(false);
                holder.ivQuiz.setEnabled(false);
                holder.ivFinalCheck.setEnabled(false);
            } else if (section == 1) {
                holder.tvStartLesson.setText("Studying...");
                holder.ivStudySession1.setEnabled(true);
                holder.ivStudySession2.setEnabled(false);
                holder.ivQuiz.setEnabled(false);
                holder.ivFinalCheck.setEnabled(false);
                holder.ivStudySession10.setVisibility(View.VISIBLE);
            } else if (section == 2) {
                holder.tvStartLesson.setText("Studying...");
                holder.ivStudySession1.setEnabled(true);
                holder.ivStudySession2.setEnabled(true);
                holder.ivQuiz.setEnabled(false);
                holder.ivFinalCheck.setEnabled(false);
                holder.ivStudySession20.setVisibility(View.VISIBLE);
                holder.ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
            } else if (section == 3) {
                holder.tvStartLesson.setText("Studying...");
                holder.ivStudySession1.setEnabled(true);
                holder.ivStudySession2.setEnabled(true);
                holder.ivQuiz.setEnabled(true);
                holder.ivFinalCheck.setEnabled(false);
                holder.ivQuiz0.setVisibility(View.VISIBLE);
                holder.ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
                holder.ivStudySession2.setImageResource(R.drawable.ic_study_session2_finish);
            } else if (section == 4) {
                holder.tvStartLesson.setText("Studying...");
                holder.ivStudySession1.setEnabled(true);
                holder.ivStudySession2.setEnabled(true);
                holder.ivQuiz.setEnabled(true);
                holder.ivFinalCheck.setEnabled(true);
                holder.ivFinalCheck0.setVisibility(View.VISIBLE);
                holder.ivStudySession1.setImageResource(R.drawable.ic_study_session1_finish);
                holder.ivStudySession2.setImageResource(R.drawable.ic_study_session2_finish);
                holder.ivQuiz.setImageResource(R.drawable.ic_quiz_finish);
            }
        }

        int anim_count = 0;
        public void showRibbonByAnim(final View view) {
            view.setVisibility(View.VISIBLE);
//            Animation in = AnimationUtils.loadAnimation(LessonsListActivity.this, R.anim.scale_in_fade0);
//            view.startAnimation(in);
//            in.setAnimationListener(new Animation.AnimationListener() {
//                @Override
//                public void onAnimationStart(Animation animation) {
//                    hideRibbonByAnim(view);
//                }
//
//                @Override
//                public void onAnimationEnd(Animation animation) {
//
//                }
//
//                @Override
//                public void onAnimationRepeat(Animation animation) {
//
//                }
//            });
        }

        public void hideRibbonByAnim(final View view) {
            Animation in = AnimationUtils.loadAnimation(LessonsListActivity.this, R.anim.scale_out_fade0);
            view.startAnimation(in);
            in.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    try {
                        LessonsListActivity.this.number = -1;
                        LessonsListActivity.this.session = 0;
                        anim_count++;
                        if (anim_count < 15) {
                            showRibbonByAnim(view);
                        } else {
                            view.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }

        class ViewHolder {
            LessonNormalPane llNormalPane;
            LinearLayout llStudyingPane;
            LinearLayout llStudyingPaneContractor;
            TextView tvTitle1;
            TextView tvStartLesson;
            ImageView ivStudySession1;
            ImageView ivStudySession2;
            ImageView ivQuiz;
            ImageView ivFinalCheck;
            ImageView ivStudySession10;
            ImageView ivStudySession20;
            ImageView ivQuiz0;
            ImageView ivFinalCheck0;
            View ivExpand;
        }

        class ParentLeftHolder {

        }
    }
}

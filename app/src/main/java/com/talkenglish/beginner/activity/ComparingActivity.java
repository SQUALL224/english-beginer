package com.talkenglish.beginner.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.AudioPlayer;
import com.talkenglish.beginner.env.FileProvider;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.model.Study;
import com.talkenglish.beginner.statmodel.ComparingStat;
import com.talkenglish.beginner.util.SharedPref;
import com.talkenglish.beginner.widget.AlphaProgressIndicator;
import com.talkenglish.beginner.widget.NotoSBoldTextView;

import java.io.File;

public class ComparingActivity extends ActionbarBaseActivity {
    private static final String TAG = "ComparingActivity";

    private static final String RECORD_FILE_NAME = "recorded";
    public static final int SESSION_COMPARING = 0x103;

    ImageView mainView;
    ImageView ivListen;
    ImageView ivRecord;
    ImageView ivStop;
    ImageView ivCompare;
    TextView tvCompleteToNext;
    TextView tvSkip;

    TextView tvStepLabel;
    TextView tvStepTotalLabel;
    AlphaProgressIndicator listenIndicator;
    AlphaProgressIndicator compareIndicator;
    NotoSBoldTextView tvSessionTitle;

    AudioPlayer audioPlayer;
    private Lesson lesson;
    private int session;

    ComparingStat stat;

    private MediaRecorder voiceRecorder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stat = new ComparingStat();
        buildLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AudioPlayer.stop(audioPlayer);
        stopRecord();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == ExerciseActivity.SESSION_EXERCISE) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Analytics.sendEvent(getApplication(), "Orientation changed", String.valueOf(newConfig.orientation));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            buildLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            buildLayout();
        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback_white);
        int type = Lesson.STUDY_SESSION1;
        if ((session&0x10) != 0) {
            type = Lesson.STUDY_SESSION1;
        } else if ((session&0x20) != 0) {
            type = Lesson.STUDY_SESSION2;
        }
        boolean bookmark = lesson.getBookmark(this, type);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }
    @Override
    public String toString() {
        return TAG;
    }
    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.STUDY_SESSION1;
        if ((session&0x10) != 0) {
            type = Lesson.STUDY_SESSION1;
        } else if ((session&0x20) != 0) {
            type = Lesson.STUDY_SESSION2;
        }
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark_white : R.drawable.ic_menu_bookmark_white_off);
    }

    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {
        setContentView(R.layout.activity_comparing);

        lesson = (Lesson) getIntent().getExtras().get("lesson");
        session = getIntent().getExtras().getInt("session");

        buildToolbar();

        mainView = (ImageView) findViewById(R.id.rl_session_main);
        ivListen = (ImageView) findViewById(R.id.iv_listen);
        ivRecord = (ImageView) findViewById(R.id.iv_record);
        ivStop = (ImageView) findViewById(R.id.iv_stop_record);
        ivCompare = (ImageView) findViewById(R.id.iv_compare);
        tvStepLabel = (TextView) findViewById(R.id.tv_step_label);
        tvStepTotalLabel = (TextView) findViewById(R.id.tv_step_total_label);
        listenIndicator = (AlphaProgressIndicator) findViewById(R.id.listen_indicator);
        compareIndicator = (AlphaProgressIndicator) findViewById(R.id.compare_indicator);
        tvSkip = (TextView) findViewById(R.id.tv_skip_to_next_button);

        tvSessionTitle = (NotoSBoldTextView) findViewById(R.id.session_study_title);

        tvCompleteToNext = (TextView) findViewById(R.id.tv_complete_to_next_button);
        final RelativeLayout rlCompletedMark = (RelativeLayout) findViewById(R.id.rl_completed_mark);
        tvCompleteToNext.setEnabled(stat.completed);
        tvCompleteToNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (session == 0x21) {

                    lesson.setSection(ComparingActivity.this, 3);
                    lesson.setPointForSession2(ComparingActivity.this);

                    rlCompletedMark.setVisibility(View.VISIBLE);
                    AssetFileDescriptor descriptor = null;
                    AudioPlayer.stop(audioPlayer);
                    try {
                        String filename = "session_completed.mp3";
                        descriptor = getAssets().openFd(filename);
                        long start = descriptor.getStartOffset();
                        long end = descriptor.getLength();
                        audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new AudioPlayer.ProgressUpdateListener() {
                            @Override
                            public void onProgress(int current, int duration) {

                            }

                            @Override
                            public void onProgress(float progress) {

                            }

                            @Override
                            public void onComplete() {
                                Intent intent = new Intent();
                                intent.putExtra("lesson", lesson);
                                intent.putExtra("session", 0x30);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });
                    } catch (Exception e) {
                        Log.e(TAG, "Opening asset file failed - congratulations.mp3");
                    }
                } else {
                    Intent i = new Intent(ComparingActivity.this, ExerciseActivity.class);
                    lesson.loadExercises(ComparingActivity.this, session);
                    i.putExtra("session", session);
                    i.putExtra("lesson", lesson);
                    gotoActivityForResult(i, ExerciseActivity.SESSION_EXERCISE);
                }
                Analytics.sendEvent(getApplication(), "Complete Pressed in Study Session - Speaking " + session, lesson.getTitle());
            }
        });

        tvSkip.setEnabled(!stat.completed);
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (session == 0x21) {

                    lesson.setSection(ComparingActivity.this, 3);
                    lesson.setPointForSession2(ComparingActivity.this);

                    rlCompletedMark.setVisibility(View.VISIBLE);
                    AssetFileDescriptor descriptor = null;
                    AudioPlayer.stop(audioPlayer);
                    try {
                        String filename = "session_completed.mp3";
                        descriptor = getAssets().openFd(filename);
                        long start = descriptor.getStartOffset();
                        long end = descriptor.getLength();
                        audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new AudioPlayer.ProgressUpdateListener() {
                            @Override
                            public void onProgress(int current, int duration) {

                            }

                            @Override
                            public void onProgress(float progress) {

                            }

                            @Override
                            public void onComplete() {
                                Intent intent = new Intent();
                                intent.putExtra("lesson", lesson);
                                intent.putExtra("session", 0x30);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });
                    } catch (Exception e) {
                        Log.e(TAG, "Opening asset file failed - congratulations.mp3");
                    }
                } else {
                    Intent i = new Intent(ComparingActivity.this, ExerciseActivity.class);
                    lesson.loadExercises(ComparingActivity.this, session);
                    i.putExtra("session", session);
                    i.putExtra("lesson", lesson);
                    gotoActivityForResult(i, ExerciseActivity.SESSION_EXERCISE);
                }
                Analytics.sendEvent(getApplication(), "Skip Pressed in Study Session - Speaking " + session, lesson.getTitle());
            }
        });

        if ((session&0x10) != 0) {
            setTitle("Study Session 1/2");
        } else if ((session&0x20) != 0) {
            setTitle("Study Session 2/2");
        }
        loadData(lesson, session);

        boolean guidePlayed = SharedPref.getBoolean(this, "guideVideoPlayed1", false);
        if (lesson.getNumber() == 1 && (session== 0x10) && (EngLearningBeginnerApp.guideVideoPlayed&1) == 0 && !guidePlayed) {
            EngLearningBeginnerApp.guideVideoPlayed ++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent myIntent = new Intent(ComparingActivity.this, VideoActivity.class);
                    myIntent.putExtra("video", "1");
                    startActivity(myIntent);
                    overridePendingTransition(R.anim.move_in_up, R.anim.move_out_fade);
                }
            }, 1000);
        }
        Analytics.sendScreenName(getApplication(), "Session Study Screen - Speaking " + session);
    }

    public void setTitle(String title) {
        TextView tvTitle = (TextView) findViewById(R.id.tv_actionbar_title);
        if (tvTitle != null) {
            tvTitle.setText(title);
        }
        tvTitle.setTextColor(Color.WHITE);
    }

    private void loadData(final Lesson lesson, final int session) {
        updateViews();
    }

    private void updateViews() {
        final Study compare = lesson.compareAt(session, stat.currentPos);
        tvStepLabel.setText(String.format("%02d", stat.currentPos + 1));
        tvStepTotalLabel.setText(String.format("/%02d", lesson.numOfCompares(session)));
//        mainView.setText(compare.getText());
        tvSessionTitle.setText(compare.getText());
        setImage(lesson.getImageUri(this, compare.getImage()));
        if (stat.status == ComparingStat.RECORDING) {
            compareIndicator.setVisibility(View.INVISIBLE);
            compareIndicator.setProgress(0);
            listenIndicator.setVisibility(View.INVISIBLE);
            listenIndicator.setProgress(0);
            ivRecord.setVisibility(View.GONE);
            ivStop.setVisibility(View.VISIBLE);
            ivCompare.setEnabled(false);
        } else if (stat.status == ComparingStat.RECORDED) {
            compareIndicator.setVisibility(View.INVISIBLE);
            compareIndicator.setProgress(0);
            listenIndicator.setVisibility(View.INVISIBLE);
            listenIndicator.setProgress(0);
            ivRecord.setVisibility(View.VISIBLE);
            ivStop.setVisibility(View.GONE);
            ivCompare.setEnabled(true);
        } else if (stat.status == ComparingStat.LISTENING) {
            compareIndicator.setVisibility(View.INVISIBLE);
            compareIndicator.setProgress(0);
            listenIndicator.setVisibility(View.VISIBLE);
            ivRecord.setVisibility(View.VISIBLE);
            ivStop.setVisibility(View.GONE);
            ivCompare.setEnabled(false);
        } else if (stat.status == ComparingStat.COMPARING) {
            compareIndicator.setVisibility(View.VISIBLE);
            listenIndicator.setVisibility(View.INVISIBLE);
            listenIndicator.setProgress(0);
            ivRecord.setVisibility(View.VISIBLE);
            ivStop.setVisibility(View.GONE);
            ivCompare.setEnabled(true);
        } else {
            listenIndicator.setVisibility(View.INVISIBLE);
            compareIndicator.setVisibility(View.INVISIBLE);
            ivRecord.setVisibility(View.VISIBLE);
            ivStop.setVisibility(View.GONE);
            ivCompare.setEnabled(false);
        }
    }

    public void setImage(String imageUrl) {
        if (imageUrl == null || imageUrl == "") {
            return;
        }
        Picasso.with(this)
                .load(imageUrl)
                .placeholder(R.drawable.placeholder_image)
                .into(mainView);
    }

    public boolean listenAudio() {

        Study compare = lesson.compareAt(session, stat.currentPos);
        AudioPlayer.stop(audioPlayer);
        if (lesson.isAudioInAssets()) {
            AssetFileDescriptor descriptor = null;
            try {
                descriptor = getAssets().openFd("audio/" + compare.getAudio());
                long start = descriptor.getStartOffset();
                long end = descriptor.getLength();
                audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new ListeningProgressUpdateListener());

                if (audioPlayer != null) {
                }
            } catch (Exception e) {
                audioPlayer = null;
                new AlertDialog.Builder(ComparingActivity.this)
                        .setMessage("Opening asset file failed - " + compare.getAudio())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                Log.e(TAG, "Opening asset file failed - " + compare.getAudio());
            }
        } else {
            audioPlayer = AudioPlayer.play(FileProvider.getPath(this, compare.getAudio()), new ListeningProgressUpdateListener());
            if (audioPlayer != null) {
            }
        }
        return (audioPlayer != null);
    }
    private boolean continueComparing() {
        File recorded = getRecordFile();
        if (recorded == null || !recorded.exists() || recorded.length() == 0) {
            return false;
        }
        String path = recorded.getPath();
        audioPlayer = AudioPlayer.play(path, new ComparingProgressUpdateListener());
        if (audioPlayer != null) {
            return true;
        }
        return false;
    }
    private boolean startComparing() {
        Study compare = lesson.compareAt(session, stat.currentPos);
        AudioPlayer.stop(audioPlayer);
        if (lesson.isAudioInAssets()) {
            AssetFileDescriptor descriptor = null;
            try {
                descriptor = getAssets().openFd("audio/" + compare.getAudio());
                long start = descriptor.getStartOffset();
                long end = descriptor.getLength();
                audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new ComparingProgressUpdateListener());

                if (audioPlayer != null) {
                }
            } catch (Exception e) {
                audioPlayer = null;
                new AlertDialog.Builder(ComparingActivity.this)
                        .setMessage("Opening asset file failed - " + compare.getAudio())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                Log.e(TAG, "Opening asset file failed - " + compare.getAudio());
            }
        } else {
            audioPlayer = AudioPlayer.play(FileProvider.getPath(this, compare.getAudio()), new ComparingProgressUpdateListener());
            if (audioPlayer != null) {

            }
        }
        return audioPlayer != null;
    }
    private File getRecordFile() {
        File cacheDir = null;
        if (Build.VERSION.SDK_INT >= 8) cacheDir = getExternalCacheDir();
        if (cacheDir == null) cacheDir = getCacheDir();
        Log.v(TAG, "getRecordFile Dir: " + cacheDir);

        return new File(cacheDir, RECORD_FILE_NAME);
    }
    private boolean recordVoice() {
        AudioPlayer.stop(audioPlayer);
        File file = getRecordFile();
        if (file == null) return false;

        voiceRecorder = new MediaRecorder();
        voiceRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        if (Build.VERSION.SDK_INT >= 10) {
            voiceRecorder.setAudioSamplingRate(44100);
            voiceRecorder.setAudioEncodingBitRate(96000);
            voiceRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            voiceRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        } else {
            voiceRecorder.setAudioSamplingRate(8000);
            voiceRecorder.setAudioEncodingBitRate(12200);
            voiceRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            voiceRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        }
        voiceRecorder.setOutputFile(file.getPath());

        try {
            voiceRecorder.prepare();
        } catch (Throwable t) {
            t.printStackTrace();
            voiceRecorder = null;
            return false;
        }

        if (voiceRecorder != null) {
            voiceRecorder.start();
            return true;
        } else {
            return false;
        }
    }

    private void stopRecord() {
        if (voiceRecorder != null) {
            voiceRecorder.stop();
            voiceRecorder.release();
            voiceRecorder = null;
        }
    }
    public void onClickNext(View v) {
        stopRecord();
        AudioPlayer.stop(audioPlayer);
        stat.next();
        if (stat.currentPos >= lesson.numOfCompares(session)) {
            stat.init(0);
            stat.completed = true;
            tvCompleteToNext.setEnabled(stat.completed);
            tvSkip.setEnabled(!stat.completed);
        }
        updateViews();
    }
    public void onClickListen(View v) {
        stopRecord();
        if (listenAudio()) {
            stat.status = ComparingStat.LISTENING;
            updateViews();
        }
        Analytics.sendEvent(getApplication(), "Listen pressed", lesson.getTitle() + stat.currentPos);
    }

    public void onClickStopRecord(View v) {
        AudioPlayer.stop(audioPlayer);
        stopRecord();
        File recorded = getRecordFile();
        if (recorded == null || !recorded.exists() || recorded.length() == 0) {
            stat.status = ComparingStat.NOTHING;

        } else {
            stat.status = ComparingStat.RECORDED;
        }
        updateViews();
        Analytics.sendEvent(getApplication(), "Stop Recording pressed", lesson.getTitle() + stat.currentPos);
    }

    public void onClickRecord(View v) {
        stopRecord();
        compareIndicator.setVisibility(View.INVISIBLE);
        compareIndicator.setProgress(0);
        listenIndicator.setVisibility(View.INVISIBLE);
        listenIndicator.setProgress(0);
        if (recordVoice()) {
            stat.status = ComparingStat.RECORDING;
            updateViews();
        }
        Analytics.sendEvent(getApplication(), "Record voice pressed", lesson.getTitle() + stat.currentPos);
    }

    public void onClickCompare(View v) {
        stopRecord();
        compareStep = 1;
        if (startComparing()) {
            stat.status = ComparingStat.COMPARING;
            updateViews();
        }
        Analytics.sendEvent(getApplication(), "Compare pressed", lesson.getTitle() + stat.currentPos);
    }

    private static int compareStep = 1;
    class ComparingProgressUpdateListener implements AudioPlayer.ProgressUpdateListener {

        @Override
        public void onProgress(int current, int duration) {
            float progress;
            if (duration == 0) {
                progress = 0f;
            } else {
                progress = Math.max(0f, Math.min(1f, (float) current / (float) duration));
            }
            try {
                compareIndicator.setVisibility(View.VISIBLE);
                compareIndicator.setProgress((compareStep-1) / 2.0f + progress/2.0f);
                stat.compareProgress = (compareStep-1) / 2.0f + progress/2.0f;
                stat.status = ComparingStat.COMPARING;

            } catch (Exception e) {

            }
        }

        @Override
        public void onProgress(float progress) {

            try {
                compareIndicator.setVisibility(View.VISIBLE);
                compareIndicator.setProgress((compareStep-1) / 2.0f + progress/2.0f);
                stat.compareProgress = (compareStep-1) / 2.0f + progress/2.0f;
                stat.status = ComparingStat.COMPARING;

            } catch (Exception e) {

            }
        }

        @Override
        public void onComplete() {
            AudioPlayer.stop(audioPlayer);
            if (compareStep >= 2) {
                stat.compareCount ++;
                compareStep = 1;
                compareIndicator.setProgress(0);
                compareIndicator.setVisibility(View.INVISIBLE);
                stat.compareProgress = 0;
//                if (stat.compareCount >= 2) {
//                    stat.next();
//                    if (stat.currentPos >= lesson.numOfCompares(session)) {
//                        stat.currentPos = 0;
//                        stat.completed = true;
//                        tvCompleteToNext.setEnabled(stat.completed);
//                        tvSkip.setEnabled(!stat.completed);
//                    }
//                }
                updateViews();
            } else {
                compareIndicator.setProgress(compareStep / 2.0f);
                stat.compareProgress = compareStep / 2.0f;
                stat.status = ComparingStat.COMPARING;
                compareStep++;
                if (compareStep%2 == 0) {
                    continueComparing();
                } else {
                    startComparing();
                }
            }
        }
    }
    class ListeningProgressUpdateListener implements AudioPlayer.ProgressUpdateListener {

        @Override
        public void onProgress(int current, int duration) {
            float progress;
            if (duration == 0) {
                progress = 0f;
            } else {
                progress = Math.max(0f, Math.min(1f, (float) current / (float) duration));
            }
            try {
                listenIndicator.setVisibility(View.VISIBLE);
                listenIndicator.setProgress(progress);
                stat.listenProgress = progress;
                stat.status = ComparingStat.LISTENING;

            } catch (Exception e) {

            }
        }

        @Override
        public void onProgress(float progress) {

            try {
                listenIndicator.setVisibility(View.VISIBLE);
                listenIndicator.setProgress(progress);
                stat.listenProgress = progress;

            } catch (Exception e) {

            }
        }

        @Override
        public void onComplete() {
            listenIndicator.setProgress(1);
            listenIndicator.setVisibility(View.INVISIBLE);
            stat.listenProgress = 0;
            stat.status = ComparingStat.NOTHING;
            AudioPlayer.stop(audioPlayer);
        }
    }
}

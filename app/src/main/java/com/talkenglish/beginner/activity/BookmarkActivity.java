package com.talkenglish.beginner.activity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.EngLearningBeginnerApp;
import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.DBHelper;
import com.talkenglish.beginner.model.Bookmark;
import com.talkenglish.beginner.model.Lesson;
import com.talkenglish.beginner.util.CircleTransform;

import java.util.ArrayList;

public class BookmarkActivity extends ActionbarBaseActivity {
    private static final String TAG = "BookmarkActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback);
        setActionBarRightButtonDrawable(R.drawable.ic_hamburger);
    }
    @Override
    public String toString() {
        return TAG;
    }
    @Override
    public void onActionBarRightButtonClicked(View v) {
//        openDrawer();
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }
    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {
        buildToolbar();

        DBHelper dbHelper = DBHelper.getInstance(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Lesson> all = new ArrayList();
        for (int level = 1; level <= 9; level ++) {
            ArrayList<Lesson> lessons = Lesson.load(this, db, level);
            all.addAll(lessons);
        }
        final ArrayList<Bookmark> bookmarks = new ArrayList<>();
        for (Lesson entry : all) {
            if (entry.getBookmark(this, Lesson.STUDY_SESSION1)) {
                bookmarks.add(new Bookmark(entry, Lesson.STUDY_SESSION1));
            }
            if (entry.getBookmark(this, Lesson.STUDY_SESSION2)) {
                bookmarks.add(new Bookmark(entry, Lesson.STUDY_SESSION2));
            }
            if (entry.getBookmark(this, Lesson.QUIZ)) {
                bookmarks.add(new Bookmark(entry, Lesson.QUIZ));
            }
            if (entry.getBookmark(this, Lesson.FINAL_CHECK)) {
                bookmarks.add(new Bookmark(entry, Lesson.FINAL_CHECK));
            }
        }
        setTitle("Bookmark");
        GridView gvBookmarks = (GridView)findViewById(R.id.gv_bookmarks);
        gvBookmarks.setAdapter(new BookmarksListAdapter(this, bookmarks));
        gvBookmarks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Bookmark bookmark = (Bookmark)adapterView.getItemAtPosition(i);
                final Lesson lesson = bookmark.getLesson();
                if (lesson == null) {
                    return;
                }
                if (bookmark.getType() == Lesson.STUDY_SESSION1) {
                    lesson.loadListens(BookmarkActivity.this, 0x10);
                    Intent ii = new Intent(BookmarkActivity.this, ListeningActivity.class);
                    ii.putExtra("lesson", lesson);
                    ii.putExtra("session", 0x10);
                    gotoActivityForResult(ii, ListeningActivity.SESSION_LISTENING);
                    Analytics.sendEvent(getApplication(), "Bookmark Study Session 1/2 Selected", lesson.getTitle());
                } else if (bookmark.getType() == Lesson.STUDY_SESSION2) {
                    lesson.loadListens(BookmarkActivity.this, 0x20);
                    Intent ii = new Intent(BookmarkActivity.this, ListeningActivity.class);
                    ii.putExtra("lesson", lesson);
                    ii.putExtra("session", 0x20);
                    gotoActivityForResult(ii, ListeningActivity.SESSION_LISTENING);
                    Analytics.sendEvent(getApplication(), "Bookmark Study Session 2/2 Selected", lesson.getTitle());
                } else if (bookmark.getType() == Lesson.QUIZ) {
                    lesson.setStudying(BookmarkActivity.this);
                    lesson.loadQuizzes1(BookmarkActivity.this);
                    Intent ii = new Intent(BookmarkActivity.this, Quiz1Activity.class);
                    ii.putExtra("lesson", lesson);
                    ii.putExtra("session", 0x30);

                    if (lesson.getNumber() == 1) {
                        EngLearningBeginnerApp.guideVideoPlayed = 0;
                    }
                    gotoActivityForResult(ii, Quiz1Activity.QUIZ1);
                    Analytics.sendEvent(getApplication(), "Bookmark Quiz Selected", lesson.getTitle());
                } else if (bookmark.getType() == Lesson.FINAL_CHECK) {
                    Intent ii = new Intent(BookmarkActivity.this, LessonStartActivity.class);
                    ii.putExtra("lesson", lesson);
                    ii.putExtra("final", true);
                    gotoActivityForResult(ii, LessonStartActivity.START_LESSON);
                    Analytics.sendEvent(getApplication(), "Bookmark Final Check", lesson.getTitle());
                }
            }
        });

//        setupDrawer();
        Analytics.sendScreenName(getApplication(), "Bookmark Screen");
    }

    private void setupViews() {

    }
    public class BookmarksListAdapter extends ArrayAdapter<Bookmark> {

        ArrayList<Bookmark> bookmarks;

        public BookmarksListAdapter(Context context, ArrayList<Bookmark> bookmarks) {
            super(context, R.layout.bookmark_item_cell);
            this.bookmarks = bookmarks;
        }

        public int getCount() {
            return bookmarks.size();
        }

        public Bookmark getItem(int index) {
            return bookmarks.get(index);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(BookmarkActivity.this).inflate(R.layout.bookmark_item_cell, parent, false);
                holder.ivLessonMain = (ImageView) convertView.findViewById(R.id.iv_lesson_main);
                holder.tvLessonTitle = (TextView) convertView.findViewById(R.id.tv_lesson_title);
                holder.tvBookmarkType= (TextView) convertView.findViewById(R.id.tv_bookmark_type);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Bookmark entry = getItem(position);
            String imageUri = entry.getMainImageUri(BookmarkActivity.this);
            if (imageUri.equals("")) {
                holder.ivLessonMain.setImageResource(R.drawable.placeholder_image);
            } else {
                Picasso.with(BookmarkActivity.this)
                        .load(imageUri)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.placeholder_image)
                        .into(holder.ivLessonMain);
            }
            holder.tvLessonTitle.setText(entry.getTitle());
            holder.tvBookmarkType.setText(entry.getString());
            return convertView;
        }
        class ViewHolder {
            ImageView ivLessonMain;
            TextView tvLessonTitle;
            TextView tvBookmarkType;
        }
    }
}

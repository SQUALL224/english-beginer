package com.talkenglish.beginner.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;

public class InstructionsActivity extends ActionbarBaseActivity {
    private static final String TAG = "InstructionsActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }
    @Override
    public String toString() {
        return TAG;
    }
    @Override
    public void onStart() {
        super.onStart();
    }
    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback);
        setActionBarRightButtonDrawable(R.drawable.ic_hamburger);
    }
    @Override
    public void onActionBarRightButtonClicked(View v) {
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }
    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {

        buildToolbar();
        setupDrawer();
        loadData();
        Analytics.sendScreenName(getApplication(), "Instructions Screen");
    }
    private void loadData() {
        WebView wvFaqs = (WebView)findViewById(R.id.wv_faqs);
        WebSettings s = wvFaqs.getSettings();
        Resources res = getResources();
        float fontSizeP = res.getDimension(R.dimen.text_normal);
        float fontSize = fontSizeP / res.getDisplayMetrics().scaledDensity;
        s.setDefaultFontSize((int)fontSize);
//        s.setBuiltInZoomControls(true);
        s.setJavaScriptEnabled(true);
        s.setDomStorageEnabled(true);
        final JavaScriptInterface javaScriptInterface = new JavaScriptInterface(this);
        wvFaqs.addJavascriptInterface(javaScriptInterface, "AndroidFunction");
        wvFaqs.loadUrl("file:///android_asset/faqs_page.html");
    }
    public class JavaScriptInterface {
        Context context;
        JavaScriptInterface(Context context) {
            this.context = context;
        }
        @JavascriptInterface
        public void contactUs() {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto","talkenglish@talkenglish.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " Android");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
    }
}

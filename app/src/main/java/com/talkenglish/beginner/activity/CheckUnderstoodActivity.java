package com.talkenglish.beginner.activity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.env.AudioPlayer;
import com.talkenglish.beginner.model.Lesson;

public class CheckUnderstoodActivity extends ActionbarBaseActivity {
    private static final String TAG = "CheckUnderstoodActivity";
    public static final int CHECK_VALIDATION = 0x105;

    private Lesson lesson;
    private boolean final0 = false;
    private int checkedLevel = -1;
    private AudioPlayer audioPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_understood);
        if (savedInstanceState != null) {
            this.checkedLevel = savedInstanceState.getInt("checked_level", R.id.rb_level_1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("checked_level", this.checkedLevel);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
    }

    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback);
        if (final0) {
            setActionBarRightButtonEnabled(true);
            int type = Lesson.FINAL_CHECK;
            boolean bookmark = lesson.getBookmark(this, type);
            setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark : R.drawable.ic_menu_bookmark_off);
        }
    }
    @Override
    public String toString() {
        return TAG;
    }
    @Override
    public void onActionBarRightButtonClicked(View v) {
        int type = Lesson.FINAL_CHECK;
        boolean bookmark = !lesson.getBookmark(this, type);
        lesson.setBookmark(this, type, bookmark);
        setActionBarRightButtonDrawable(bookmark ? R.drawable.ic_menu_bookmark : R.drawable.ic_menu_bookmark_off);
    }
    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {

        lesson = (Lesson) getIntent().getExtras().get("lesson");
        final0 = (boolean) getIntent().getExtras().get("final");

        buildToolbar();
        if (final0) {
            setTitle("Final Check");
        } else {
            setTitle(lesson.getTitle());
        }
        loadData(lesson);

        Analytics.sendScreenName(getApplication(), "Check Validation Screen");
    }
    private void loadData(final Lesson lesson) {
        RadioGroup rgLevel = (RadioGroup) findViewById(R.id.rg_understood_level);
        rgLevel.check(checkedLevel);
        final ImageView ivLevel = (ImageView) findViewById(R.id.iv_understood_level);
        final LinearLayout llGotoQuiz = (LinearLayout) findViewById(R.id.ll_goto_quiz_container);
        final LinearLayout llGotoStudySession = (LinearLayout) findViewById(R.id.ll_goto_study_session_container);

        if (checkedLevel == R.id.rb_level_1 || checkedLevel == R.id.rb_level_2) {
            ivLevel.setVisibility(View.VISIBLE);
            ivLevel.setImageResource(R.drawable.understood_difficult);
            llGotoQuiz.setVisibility(View.INVISIBLE);
            llGotoStudySession.setVisibility(View.VISIBLE);
        } else if (checkedLevel == R.id.rb_level_3) {
            ivLevel.setVisibility(View.VISIBLE);
            ivLevel.setImageResource(R.drawable.understood_a_little);
            llGotoStudySession.setVisibility(View.VISIBLE);
            if (final0) {
                llGotoQuiz.setVisibility(View.VISIBLE);
            } else {
                llGotoQuiz.setVisibility(View.INVISIBLE);
            }
        } else if (checkedLevel == R.id.rb_level_4 || checkedLevel == R.id.rb_level_5) {
            ivLevel.setVisibility(View.VISIBLE);
            ivLevel.setImageResource(R.drawable.understood_too_easy);
            llGotoQuiz.setVisibility(View.VISIBLE);
            llGotoStudySession.setVisibility(View.VISIBLE);
        } else {
            ivLevel.setVisibility(View.INVISIBLE);
            llGotoQuiz.setVisibility(View.INVISIBLE);
            llGotoStudySession.setVisibility(View.INVISIBLE);
        }
        final TextView tvGotoStudySession = (TextView) findViewById(R.id.tv_goto_study_session_button);
        final TextView tvGotoQuiz = (TextView) findViewById(R.id.tv_goto_quiz_button);
        final RelativeLayout rlCompletedMark = (RelativeLayout) findViewById(R.id.rl_completed_mark);
        final ImageView ivStar1 = (ImageView) findViewById(R.id.iv_star1);
        final ImageView ivStar2 = (ImageView) findViewById(R.id.iv_star2);
        final ImageView ivStar3 = (ImageView) findViewById(R.id.iv_star3);
        if (final0) {
            tvGotoStudySession.setText("Repeat Study Session");
            tvGotoQuiz.setText("COMPLETE LESSON!");
        } else {
            tvGotoStudySession.setText("Go To Study Session");
            tvGotoQuiz.setText("Go To Quiz!");
        }
        tvGotoStudySession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                lesson.setSection(CheckUnderstoodActivity.this, 1);
                intent.putExtra("lesson", lesson);
                intent.putExtra("session", 0x10);
                setResult(RESULT_OK, intent);
                finish();
                Analytics.sendEvent(getApplication(),
                        (final0 ? "Repeat Study Session" : "Go To Study Session") + " Pressed", lesson.getTitle());
            }
        });
        tvGotoQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.sendEvent(getApplication(),
                        (final0 ? "COMPLETE LESSON!" : "Go To Quiz!") + " Pressed", lesson.getTitle());
                if (final0) {
                    lesson.complete(CheckUnderstoodActivity.this);
                    lesson.contract(CheckUnderstoodActivity.this);
                    if (lesson.getNext() != null) {
                        lesson.getNext().setStudying(CheckUnderstoodActivity.this);
                    }
                    if (lesson.getStars(CheckUnderstoodActivity.this) == 1) {
                        ivStar1.setVisibility(View.GONE);
                        ivStar2.setVisibility(View.GONE);
                        ivStar3.setVisibility(View.VISIBLE);
                    } else if (lesson.getStars(CheckUnderstoodActivity.this) == 2) {
                        ivStar1.setVisibility(View.GONE);
                        ivStar2.setVisibility(View.VISIBLE);
                        ivStar3.setVisibility(View.VISIBLE);
                    } else if (lesson.getStars(CheckUnderstoodActivity.this) == 3) {
                        ivStar1.setVisibility(View.VISIBLE);
                        ivStar2.setVisibility(View.VISIBLE);
                        ivStar3.setVisibility(View.VISIBLE);
                    }
                    rlCompletedMark.setVisibility(View.VISIBLE);
                    AssetFileDescriptor descriptor = null;
                    AudioPlayer.stop(audioPlayer);
                    try {
                        String filename = "completed.wav";
                        descriptor = getAssets().openFd(filename);
                        long start = descriptor.getStartOffset();
                        long end = descriptor.getLength();
                        audioPlayer = AudioPlayer.play(descriptor.getFileDescriptor(), (int) start, (int) end, new AudioPlayer.ProgressUpdateListener() {
                            @Override
                            public void onProgress(int current, int duration) {

                            }

                            @Override
                            public void onProgress(float progress) {

                            }

                            @Override
                            public void onComplete() {
                                Intent intent = new Intent();
                                intent.putExtra("lesson", lesson);
                                intent.putExtra("session", 0x40);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });

                    } catch (Exception e) {
                        Log.e(TAG, "Opening asset file failed - congratulations.mp3");
                    }
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("lesson", lesson);
                    lesson.setSection(CheckUnderstoodActivity.this, 3);
                    lesson.setPointForSession1(CheckUnderstoodActivity.this);
                    lesson.setPointForSession2(CheckUnderstoodActivity.this);
                    intent.putExtra("session", 0x30);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }
    public void onClickUnderstoodLevel(View v) {
        ImageView ivLevel = (ImageView)findViewById(R.id.iv_understood_level);
        LinearLayout llGotoQuiz = (LinearLayout)findViewById(R.id.ll_goto_quiz_container);
        final LinearLayout llGotoStudySession = (LinearLayout) findViewById(R.id.ll_goto_study_session_container);
        if (v.getId() == R.id.rb_level_1 || v.getId() == R.id.rb_level_2) {
            ivLevel.setVisibility(View.VISIBLE);
            ivLevel.setImageResource(R.drawable.understood_difficult);
            llGotoQuiz.setVisibility(View.INVISIBLE);
            llGotoStudySession.setVisibility(View.VISIBLE);
            checkedLevel = v.getId();
        } else if (v.getId() == R.id.rb_level_3) {
            ivLevel.setVisibility(View.VISIBLE);
            ivLevel.setImageResource(R.drawable.understood_a_little);
            llGotoStudySession.setVisibility(View.VISIBLE);
            if (final0) {
                llGotoQuiz.setVisibility(View.VISIBLE);
            } else {
                llGotoQuiz.setVisibility(View.INVISIBLE);
            }
            checkedLevel = v.getId();
        } else if (v.getId() == R.id.rb_level_4 || v.getId() == R.id.rb_level_5) {
            ivLevel.setVisibility(View.VISIBLE);
            ivLevel.setImageResource(R.drawable.understood_too_easy);
            llGotoQuiz.setVisibility(View.VISIBLE);
            llGotoStudySession.setVisibility(View.VISIBLE);
            checkedLevel = v.getId();
        }

    }
}

package com.talkenglish.beginner.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.env.Analytics;
import com.talkenglish.beginner.model.RecommendedAppInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecommendedAppsActivity extends ActionbarBaseActivity {
    private static final String TAG = "RecommendedAppsActivity";

    private ListView lvApps;
    private LinearLayout llDescription;
    AppsListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommended_apps);
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    private void buildToolbar() {
        setActionBarLeftButtonEnabled(true);
        setActionBarRightButtonEnabled(true);
        setActionBarLeftButtonDrawable(R.drawable.ic_toolbar_goback);
        setActionBarRightButtonDrawable(R.drawable.ic_hamburger);
    }
    @Override
    public String toString() {
        return TAG;
    }
    @Override
    public void onActionBarRightButtonClicked(View v) {
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }
    @Override
    public void onActionBarLeftButtonClicked(View v) {
        super.onActionBarLeftButtonClicked(v);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void buildLayout() {

        buildToolbar();

        lvApps = (ListView)findViewById(R.id.lv_apps);
        loadData();

        lvApps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RecommendedAppInfo entry = (RecommendedAppInfo)parent.getItemAtPosition(position);

                Analytics.sendEvent(getApplication(), "Recommended App Selected", entry.getAppTitle());
                final String appPackageName = entry.getAppPackage();//"talkenglish.com.standard";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        setTitle("Recommended Apps");
        setupDrawer();
        Analytics.sendScreenName(getApplication(), "Recommended Apps Screen");
    }
    private void loadData() {
        ArrayList<RecommendedAppInfo> apps = new ArrayList<RecommendedAppInfo>(
                Arrays.asList(RecommendedAppInfo.getRecommendedApps(this)));


        adapter = new AppsListAdapter(this, apps);
        lvApps.setAdapter(adapter);
    }
    class AppsListAdapter extends ArrayAdapter<RecommendedAppInfo> {

        List<RecommendedAppInfo> apps;

        public AppsListAdapter(Context context, List<RecommendedAppInfo> apps) {
            super(context, R.layout.recommended_app_info_cell, apps);
            this.apps = apps;
        }

        public int getCount() {
            return apps.size();
        }

        public RecommendedAppInfo getItem(int index) {
            return apps.get(index);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null){
                holder = new ViewHolder();
                convertView = LayoutInflater.from(RecommendedAppsActivity.this).inflate(R.layout.recommended_app_info_cell,
                        parent,
                        false);
                holder.ivAppIcon = (ImageView) convertView.findViewById(R.id.iv_app_icon);
                holder.tvAppTitle = (TextView) convertView.findViewById(R.id.tv_app_title);
                holder.tvAppDescription = (TextView) convertView.findViewById(R.id.tv_app_description);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            RecommendedAppInfo entry = getItem(position);

            holder.tvAppTitle.setText(entry.getAppTitle());
            holder.tvAppDescription.setText(entry.getAppDescription());
            holder.ivAppIcon.setImageResource(entry.getAppIcon());
            return convertView;
        }
        class ViewHolder {
            ImageView ivAppIcon;
            TextView tvAppTitle;
            TextView tvAppDescription;
        }
    }
}

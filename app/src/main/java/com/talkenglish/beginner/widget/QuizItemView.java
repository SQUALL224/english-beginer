package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkenglish.beginner.R;

public class QuizItemView extends RelativeLayout {
    public static final int STATUS_NOTHING = 0;
    public static final int STATUS_NORMAL = 1;
    public static final int STATUS_CORRECT = 2;
    public static final int STATUS_INCORRECT = 3;
    public static final int STATUS_AVAILABLE = 4;
    private RelativeLayout rlContainer;
    private TextView tvItem;
    public QuizEditText etItem;

    int status = STATUS_NOTHING;
    int bgColor = Color.TRANSPARENT;
    int fgColor =  Color.parseColor("#ffffff");

    public QuizItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public QuizItemView(Context context) {
        super(context);
        init();
    }
    private void init(){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.quiz_sentence_item, this, true);
        this.tvItem= (TextView) findViewById(R.id.tv_item);
        this.etItem = (QuizEditText) findViewById(R.id.et_item);
        this.rlContainer = (RelativeLayout) findViewById(R.id.rl_item_container);
        this.etItem.setVisibility(INVISIBLE);
    }
    public void setStatus(int status) {
        this.status = status;
        if (status == STATUS_NORMAL) {
            fgColor = Color.WHITE;
            rlContainer.setBackgroundResource(R.drawable.ic_quiz_blank_normal);

        } else if (status == STATUS_CORRECT) {
            fgColor = Color.parseColor("#02d669");
            rlContainer.setBackgroundResource(R.drawable.ic_quiz_blank_correct);

        } else if (status == STATUS_INCORRECT) {
            fgColor = Color.parseColor("#fffc00");
            rlContainer.setBackgroundResource(R.drawable.ic_quiz_blank_incorrect);
        } else if (status == STATUS_AVAILABLE) {
            fgColor = Color.WHITE;
        } else {
            bgColor = Color.TRANSPARENT;
            fgColor = Color.parseColor("#ffffff");
            rlContainer.setBackgroundColor(bgColor);
        }
        tvItem.setTextColor(fgColor);
    }
    public void setTextSize(int unit, float size) {
        tvItem.setTextSize(unit, size);
        etItem.setTextSize(unit, size);
    }
    public void setText(String text) {
        tvItem.setText(text);
    }
    public void setTextColor(int color){
        tvItem.setTextColor(color);
    }
}

package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.talkenglish.beginner.R;

/**
 * Created by inkyfox on 14. 12. 3..
 */
public class ProgressScroller extends View {

    public static interface OnProgressScrollListener {
        public void onProgressScroll(float progress);
    }

    private float mProgress;
    private Paint mPaint = new Paint();
    private int mBgColor;
    private int mColor;

    private OnProgressScrollListener mListener;

    public ProgressScroller(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public ProgressScroller(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProgressScroller(Context context) {
        super(context);
        init(context);
    }

    public void init(Context context) {
        mBgColor = 0xffd7e4bb;
        mColor = context.getResources().getColor(R.color.my_action_bar_dark_bg);
        mProgress = 0f;
    }

    public void setOnProgressScrollListener(OnProgressScrollListener l) {
        mListener = l;
    }

    public void setProgress(float progress) {
        mProgress = Math.max(0f, Math.min(1.0f, progress));
        invalidate();
    }

    public float getProgress() {
        return mProgress;
    }

    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        if(width == 0 || height == 0) return;

        int left = getPaddingLeft();
        int right = getPaddingRight();
        int top = getPaddingTop();
        int bottom = getPaddingBottom();
        int bgWidth = width - left - getPaddingRight();
        int barWidth = (int)(bgWidth * mProgress);
        mPaint.setAntiAlias(false);
        mPaint.setStyle(Paint.Style.FILL);

        mPaint.setColor(mBgColor);
        canvas.drawRect(left, top, width - right, height - bottom, mPaint);

        mPaint.setColor(mColor);
        canvas.drawRect(left, top, left + barWidth, height - bottom, mPaint);
    }

    private float mPreviousSent;

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if(mListener == null) return false;

        int left = getPaddingLeft();
        int right = getPaddingRight();
        int width = getWidth();

        if(e.getAction() == MotionEvent.ACTION_DOWN) {
            float p = (e.getX() - left) / (width - left - right);
            if(p >= 0f && p <= 1f) {
                dispatchOnProgressScroll(p);
                mPreviousSent = p;

                return true;
            }
        }
        else if(e.getAction() == MotionEvent.ACTION_MOVE) {
            float p = (e.getX() - left) / (width - left - right);
            if(p >= 0f && p <= 1f && Math.abs(p - mPreviousSent) > 0.05f) {
                dispatchOnProgressScroll(p);
                mPreviousSent = p;
            }
            return true;
        }

        return super.onTouchEvent(e);
    }

    private void dispatchOnProgressScroll(final float progress) {
        post(new Runnable() {
            @Override
            public void run() {
                mListener.onProgressScroll(progress);
            }
        });
    }
}

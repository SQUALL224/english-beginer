package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

import com.talkenglish.beginner.R;

public class UnderlineTextView extends TextView {
    public static final int STATUS_NOTHING = 0;
    public static final int STATUS_NORMAL = 1;
    public static final int STATUS_CORRECT = 2;
    public static final int STATUS_INCORRECT = 3;
    public static final int STATUS_AVAILABLE = 4;
    int status = STATUS_NOTHING;
    int edge = R.drawable.normal_quiz_text_view;
    int bgColor = Color.TRANSPARENT;
    int fgColor =  Color.parseColor("#333333");

    public UnderlineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public UnderlineTextView(Context context) {
        super(context);
    }

    public void setStatus(int status) {
        this.status = status;
        if (status == STATUS_NORMAL) {
            bgColor = Color.TRANSPARENT;
            fgColor = Color.parseColor("#ff333333");
            edge = R.drawable.normal_quiz_text_view;
            setBackgroundResource(edge);

        } else if (status == STATUS_CORRECT) {
            bgColor = Color.parseColor("#cbf3c6");
            fgColor = Color.parseColor("#4a9513");
            edge = R.drawable.correct_quiz_text_view;
            setBackgroundResource(edge);

        } else if (status == STATUS_INCORRECT) {
            bgColor = Color.parseColor("#eccdcd");
            fgColor = Color.parseColor("#c01212");
            edge = R.drawable.incorrect_quiz_text_view;
            setBackgroundResource(edge);

        } else if (status == STATUS_AVAILABLE) {
            bgColor = Color.parseColor("#ffef83");
            fgColor = bgColor;
            edge = R.drawable.normal_quiz_text_view;

        } else {
            bgColor = Color.TRANSPARENT;
            fgColor = Color.parseColor("#ff333333");
            setBackgroundColor(bgColor);
        }
        setTextColor(fgColor);
    }
}

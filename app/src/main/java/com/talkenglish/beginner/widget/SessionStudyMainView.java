package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.R;

public class SessionStudyMainView extends RelativeLayout {

    protected float width = 999.0f;
    protected float height = 700;
    protected float imageLeft = 99.0f;
    protected float imageRight = 101.0f;
    protected float imageTop = 80.0f;
    protected float imageBottom = 80.0f;

    protected ImageView ivMain;

    public SessionStudyMainView(Context context, AttributeSet attrs) {
        super(context, attrs);

//        setBackgroundResource(R.drawable.session_study_bg);
        init();
        if (isInEditMode()) {
            return;
        }
    }

    void init() {
        ivMain = new ImageView(getContext());
        ivMain.setImageResource(R.drawable.placeholder_image);
        ivMain.setBackgroundColor(Color.TRANSPARENT);
        ivMain.setScaleType(ImageView.ScaleType.FIT_XY);
        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(ivMain, params);
    }

    protected void resizeChildViews(int width, int height) {
        LayoutParams params = (LayoutParams)ivMain.getLayoutParams();

        params.leftMargin = 0; //(int) (this.imageLeft * width / this.width);
        params.rightMargin = 0; //(int) (this.imageLeft * width / this.width);
        params.topMargin = (int) (this.imageTop * height / this.height);
        params.bottomMargin = 0; //(int) (this.imageBottom * height / this.height);
        ivMain.setLayoutParams(params);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        if ((float) width / (float) height > this.width / this.height) {
            width = (int) (height * 1.0f * (float) this.width/ (float) this.height);
        } else {
            height = (int) (width * 1.0f * (float) this.height / (float) this.width);
        }
        resizeChildViews(width, height);
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        resizeChildViews(w, h);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    public void setImage(String imageUrl) {
        if (imageUrl == null || imageUrl == "") {
            return;
        }
        Picasso.with(getContext())
                .load(imageUrl)
                .placeholder(R.drawable.placeholder_image)
                .into(this.ivMain);
    }
}

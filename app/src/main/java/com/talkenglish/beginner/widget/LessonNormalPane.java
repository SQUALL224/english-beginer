package com.talkenglish.beginner.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.model.Lesson;

/**
 * Created by Alex on 7/2/2016.
 */
public class LessonNormalPane extends LinearLayout {

    Context context;
    TextView tvTitle;
    View vExpand;

    Lesson lesson;

    public LessonNormalPane(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public LessonNormalPane(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init(){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.lesson_normal_pane, this, true);
        this.tvTitle = (TextView) findViewById(R.id.tv_title);
        vExpand = (View) findViewById(R.id.item_expand);
        setClickable(true);
    }

    public void setTitle(String title) {
        this.tvTitle.setText(title);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.tvTitle.setEnabled(enabled);
        if(enabled)
            this.vExpand.setVisibility(VISIBLE);
        else
            this.vExpand.setVisibility(GONE);
    }
    public void setEntry(Context context, Lesson entry) {
        this.lesson = entry;
        if (entry == null) {
            return;
        }
        setTitle(entry.getTitle());
    }
    public Lesson getEntry() {
        return this.lesson;
    }
}

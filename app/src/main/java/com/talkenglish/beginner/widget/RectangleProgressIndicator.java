package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by alex
 */
public class RectangleProgressIndicator extends View {

    private float mDensity;

    private float progress = 0.0f;

    public RectangleProgressIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public RectangleProgressIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RectangleProgressIndicator(Context context) {
        super(context);
        init(context);
    }

    public void init(Context context) {
        mDensity = context.getResources().getDisplayMetrics().density;
    }

    public void setProgress(float progress) {
        this.progress = Math.max(0f, Math.min(1.0f, progress));
        invalidate();
    }

    public float getProgress() {
        return progress;
    }

    protected void onDraw(Canvas canvas) {
        float width = (float)getWidth() - 2;
        float height = (float)getHeight();
        float r = height / 2;

        if(width == 0 || height == 0) return;
        float barWidth = (float)(width * progress);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.TRANSPARENT);

        canvas.drawRect(new RectF(1, 0, width-1, height-1), paint);

        Paint paint0 = new Paint();
        paint0.setColor(Color.parseColor("#883364c5"));
        paint0.setStyle(Paint.Style.FILL);
        paint0.setAntiAlias(true);
        canvas.drawRect(1, 0, barWidth-1, height-1, paint0);


    }
}
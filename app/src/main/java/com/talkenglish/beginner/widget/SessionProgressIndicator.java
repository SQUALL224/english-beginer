package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by alex
 */
public class SessionProgressIndicator extends View {

    private float mDensity;

    private float progress = 0.00f;

    public SessionProgressIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public SessionProgressIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SessionProgressIndicator(Context context) {
        super(context);
        init(context);
    }

    public void init(Context context) {
        mDensity = context.getResources().getDisplayMetrics().density;
        progress = 0.00f;
    }

    public void setProgress(float progress) {
        this.progress = Math.max(0f, Math.min(1.0f, progress));
        invalidate();
    }

    public float getProgress() {
        return progress;
    }

    protected void onDraw(Canvas canvas) {
        float width = (float)getWidth() - 2;
        float height = (float)getHeight();
//        float r = height / 2;

        if(width == 0 || height == 0) return;
        float barWidth = (float)(width * progress);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#484848"));

        canvas.drawRect(new RectF(1, 0, width-1, height-1), paint);
//        canvas.drawRoundRect(new RectF(1, 0, width-1, height-1), r, r, paint);

        Path path0 = new Path();
        Path path1 = new Path();
        path0.moveTo(1, height-1);
        path0.lineTo(barWidth-1, height-1);

        path1.moveTo(1, height-1);
        path1.lineTo(barWidth-1, height-1);
        if (barWidth >= 2*height) {

            path0.arcTo(new RectF(barWidth - 1 - 2 * height, 0, barWidth - 1, height - 1), -90, 90);
            path0.lineTo(1 + height, 0);
            path0.arcTo(new RectF(1, 0, 2 * height - 1, height - 1), -90, -90);

//            path1.arcTo(new RectF(barWidth - 1 - 2 * r, 0, barWidth - 1, height - 1), 0, 90);
//            path1.lineTo(1 + r, height - 1);
//            path1.arcTo(new RectF(1, 0, 2 * r - 1, height - 1), 90, 90);
        } else {
            float alpha = (float) Math.abs(Math.acos((double) (height - barWidth / 2) / height));
            path0.arcTo(new RectF(barWidth - 1 - 2 * height, 0, barWidth - 1, height - 1), 0, -alpha * 180 / (float) Math.PI);
            path0.arcTo(new RectF(1, 0, 2 * height - 1, height - 1), 180 + alpha * 180 / (float) Math.PI, -alpha * 180 / (float) Math.PI);
//            path1.arcTo(new RectF(barWidth - 1 - 2 * r, 0, barWidth - 1, height - 1), 0, alpha * 180 / (float) Math.PI);
//            path1.arcTo(new RectF(1, 0, 2 * r - 1, height - 1), 180-alpha * 180 / (float) Math.PI, alpha * 180 / (float) Math.PI);
        }

        path0.close();
        path1.close();

        Paint paint0 = new Paint();
        paint0.setColor(Color.parseColor("#c90a4d"));
        paint0.setStyle(Paint.Style.FILL);
        paint0.setAntiAlias(true);
        canvas.drawPath(path0, paint0);
//        Paint paint1 = new Paint();
//        paint1.setColor(Color.parseColor("#0a44b9"));
//        paint1.setStyle(Paint.Style.FILL);
//        paint1.setAntiAlias(true);
//        canvas.drawPath(path1, paint1);


    }
}
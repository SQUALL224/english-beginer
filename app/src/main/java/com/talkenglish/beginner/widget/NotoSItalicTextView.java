package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.talkenglish.beginner.util.FontCache;

/**
 * Created by Great Summit on 4/9/2016.
 */
public class NotoSItalicTextView extends TextView {

    public NotoSItalicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NotoSItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NotoSItalicTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = FontCache.getTypefaceNotoSans_Italic(getContext());
        setTypeface(tf);
    }
}

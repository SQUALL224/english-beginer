package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.talkenglish.beginner.util.FontCache;

/**
 * Created by alex on 7/1/2016.
 */
public class NotoSTextView extends TextView {
    public NotoSTextView(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        init();
        if (isInEditMode()) {
            return;
        }
    }

    public NotoSTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        if (isInEditMode()) {
            return;
        }
    }

    public NotoSTextView(Context context) {
        super(context);
        init();
        if (isInEditMode()) {
            return;
        }
    }

    private void init() {
        Typeface tf = FontCache.getTypefaceNotoSans_Regular(getContext());
        setTypeface(tf);
    }
}

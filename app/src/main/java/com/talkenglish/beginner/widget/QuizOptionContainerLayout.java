package com.talkenglish.beginner.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.activity.Quiz1Activity;
import com.talkenglish.beginner.model.Quiz;

import java.util.ArrayList;

public class QuizOptionContainerLayout extends LinearLayout implements View.OnTouchListener {

    Context context;
    ArrayList<Quiz> quizs = null;
    int screenWidth = 0;
    boolean enabled = true;

    public QuizOptionContainerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setPadding((int) context.getResources().getDimension(R.dimen.lesson_main_image_horizontal_margin),
                (int) context.getResources().getDimension(R.dimen.lesson_main_image_vertical_padding),
                (int) context.getResources().getDimension(R.dimen.lesson_main_image_horizontal_margin),
                (int) context.getResources().getDimension(R.dimen.lesson_main_image_vertical_padding));
        setOrientation(VERTICAL);
        setGravity(Gravity.LEFT);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

        super.onLayout(changed, left, top, right, bottom);
    }

    void buildLayout() {
        if (this.quizs == null) {
            return;
        }
        removeAllViews();

        int maxWidth = this.screenWidth -
                (int) context.getResources().getDimension(R.dimen.lesson_main_image_horizontal_margin) * 4;

        LinearLayout llAlso = new LinearLayout(context);
        llAlso.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                (int)context.getResources().getDimension(R.dimen._27dp)));
        llAlso.setGravity(Gravity.LEFT);
        llAlso.setOrientation(HORIZONTAL);

        int widthSoFar = 0;
        for (int i = 0; i < quizs.size(); i++) {
            QuizOptionView wovItem = new QuizOptionView(context);
            wovItem.setTextColor(Color.parseColor("#ffffffff"));
            wovItem.setText(" " + quizs.get(i).getAnswer() + " ");
            LayoutParams params0 = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            if (i < quizs.size() - 1) {
                params0.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen._4dp), 0);
                wovItem.setLayoutParams(params0);
            }
            wovItem.measure(0, 0);
            wovItem.setTag(Integer.valueOf(i));
            widthSoFar += wovItem.getMeasuredWidth()
                    + ((i < quizs.size() - 1) ? (int) context.getResources().getDimension(R.dimen._4dp) : 0)
                    + ((i < quizs.size() - 1) ? 0 : (int) context.getResources().getDimension(R.dimen._4dp) * 2);

            wovItem.setOnTouchListener(this);
            if (widthSoFar >= maxWidth) {
                addView(llAlso);
                llAlso = new LinearLayout(context);
                LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)context.getResources().getDimension(R.dimen._27dp));
                params.setMargins(0, (int) context.getResources().getDimension(R.dimen._4dp), 0, 0);
                llAlso.setLayoutParams(params);
                llAlso.setOrientation(HORIZONTAL);
                llAlso.setGravity(Gravity.LEFT);
                llAlso.addView(wovItem);
                widthSoFar = wovItem.getMeasuredWidth();

            } else {
                llAlso.addView(wovItem);
            }
        }
        addView(llAlso);
    }

    public void setEntry(int screenWidth, final ArrayList<Quiz> quizs) {
        if (quizs == null) return;
        this.quizs = quizs;
        this.screenWidth = screenWidth;
        if (this.screenWidth == 0) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                @Override
                public void onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    QuizOptionContainerLayout.this.screenWidth = getMeasuredWidth();
                    buildLayout();
                }
            });
        } else {
            buildLayout();
        }
    }
    public void setEntry(final ArrayList<Quiz> quizs) {
        if (quizs == null) return;
        this.quizs = quizs;
        if (screenWidth == 0) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                @Override
                public void onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    screenWidth = getMeasuredWidth();
                    buildLayout();
                }
            });
        } else {
            buildLayout();
        }
    }

    public void refresh() {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
                screenWidth = getMeasuredWidth();
                buildLayout();
            }
        });
    }
    public void refresh(int screenWidth) {
        this.screenWidth = screenWidth;
        if (this.screenWidth == 0) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                @Override
                public void onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    QuizOptionContainerLayout.this.screenWidth = getMeasuredWidth();
                    buildLayout();
                }
            });
        } else {
            buildLayout();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if (this.context instanceof Quiz1Activity) {
                    Quiz1Activity lactivity = (Quiz1Activity) this.context;
                    lactivity.startDragging(v, (Integer) v.getTag());
                }
                return true;
            }
            default: {
                return true;
            }
        }
    }

    public void setEnabled(boolean enable) {
        this.enabled = enable;
    }
}

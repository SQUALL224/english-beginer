package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.talkenglish.beginner.R;

public class SessionValidateMainView extends RelativeLayout implements View.OnClickListener {

    protected float width = 950;
    protected float height = 666;

    protected float width0 = 475;
    protected float height0 = 333;
    protected float left1 = 0;
    protected float top1 = 0;
    protected float left2 = 475;
    protected float top2 = top1;
    protected float left3 = left1;
    protected float top3 = 333;
    protected float left4 = left2;
    protected float top4 = top3;

    protected SessionValidateSubView subView1;
    protected SessionValidateSubView subView2;
    protected SessionValidateSubView subView3;
    protected SessionValidateSubView subView4;

    protected int selected_index = -1;
    protected OnSelectedListener listener = null;

    protected boolean enabled = true;

    public SessionValidateMainView(Context context, AttributeSet attrs) {
        super(context, attrs);

//        setBackgroundResource(R.drawable.session_validate_bg);
        initViews();
        if (isInEditMode()) {
            return;
        }
    }

    @Override
    public void onClick(View view) {
        if (!enabled) return;
        SessionValidateSubView v = (SessionValidateSubView) view;
        if (v == subView1) {
            subView1.check(true);
            subView2.check(false);
            subView3.check(false);
            subView4.check(false);
            selected_index = 0;
            if (listener != null) {
                listener.onSelected(selected_index);
            }
        } else if (v == subView2) {
            subView1.check(false);
            subView2.check(true);
            subView3.check(false);
            subView4.check(false);
            selected_index = 1;
            if (listener != null) {
                listener.onSelected(selected_index);
            }
        } else if (v == subView3) {
            subView1.check(false);
            subView2.check(false);
            subView3.check(true);
            subView4.check(false);
            selected_index = 2;
            if (listener != null) {
                listener.onSelected(selected_index);
            }
        } else if (v == subView4) {
            subView1.check(false);
            subView2.check(false);
            subView3.check(false);
            subView4.check(true);
            selected_index = 3;
            if (listener != null) {
                listener.onSelected(selected_index);
            }
        }
    }

    public void setOnSelectedListener(OnSelectedListener listener) {
        this.listener = listener;
    }

    void initViews() {
        subView1 = new SessionValidateSubView(getContext());
        subView2 = new SessionValidateSubView(getContext());
        subView3 = new SessionValidateSubView(getContext());
        subView4 = new SessionValidateSubView(getContext());
        LayoutParams params0 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params0.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(subView1, params0);
        LayoutParams params1 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(subView2, params1);
        LayoutParams params2 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params2.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params2.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(subView3, params2);
        LayoutParams params3 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params3.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params3.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(subView4, params3);

        subView1.setOnClickListener(this);
        subView2.setOnClickListener(this);
        subView3.setOnClickListener(this);
        subView4.setOnClickListener(this);
    }

    public void init() {
        subView1.init();
        subView2.init();
        subView3.init();
        subView4.init();
    }
    protected void resizeChildViews(int width, int height) {

        LayoutParams params1 = (LayoutParams) subView1.getLayoutParams();
        params1.leftMargin = (int) (this.left1 * width / this.width);
        params1.rightMargin = (int) ((this.width - this.left1 - this.width0) * width / this.width);
        params1.topMargin = (int) (this.top1 * height / this.height);
        params1.bottomMargin = (int) ((this.height - this.top1 - this.height0) * height / this.height);
        subView1.setLayoutParams(params1);
        LayoutParams params2 = (LayoutParams) subView2.getLayoutParams();
        params2.leftMargin = (int) (this.left2 * width / this.width);
        params2.rightMargin = (int) ((this.width - this.left2 - this.width0) * width / this.width);
        params2.topMargin = (int) (this.top2 * height / this.height);
        params2.bottomMargin = (int) ((this.height - this.top2 - this.height0) * height / this.height);
        subView2.setLayoutParams(params2);
        LayoutParams params3 = (LayoutParams) subView3.getLayoutParams();
        params3.leftMargin = (int) (this.left3 * width / this.width);
        params3.rightMargin = (int) ((this.width - this.left3 - this.width0) * width / this.width);
        params3.topMargin = (int) (this.top3 * height / this.height);
        params3.bottomMargin = (int) ((this.height - this.top3 - this.height0) * height / this.height);
        subView3.setLayoutParams(params3);
        LayoutParams params4 = (LayoutParams) subView4.getLayoutParams();
        params4.leftMargin = (int) (this.left4 * width / this.width);
        params4.rightMargin = (int) ((this.width - this.left4 - this.width0) * width / this.width);
        params4.topMargin = (int) (this.top4 * height / this.height);
        params4.bottomMargin = (int) ((this.height - this.top4 - this.height0) * height / this.height);
        subView4.setLayoutParams(params4);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        /*if ((float) width / (float) height > this.width / this.height) {
            width = (int) (height * 1.0f * (float) this.width/ (float) this.height);
        } else {
            height = (int) (width * 1.0f * (float) this.height / (float) this.width);
        }*/
        resizeChildViews(width, height);
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        resizeChildViews(w, h);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getSelectedIndex() {
        return this.selected_index;
    }

    public void setSelectedIndex(int selected) {

        this.selected_index = selected;
        if (selected == 0) {
            subView1.check(true);
            subView2.check(false);
            subView3.check(false);
            subView4.check(false);
        } else if (selected == 1) {
            subView1.check(false);
            subView2.check(true);
            subView3.check(false);
            subView4.check(false);
        } else if (selected == 2) {
            subView1.check(false);
            subView2.check(false);
            subView3.check(true);
            subView4.check(false);
        } else if (selected == 3) {
            subView1.check(false);
            subView2.check(false);
            subView3.check(false);
            subView4.check(true);
        } else {
            subView1.check(false);
            subView2.check(false);
            subView3.check(false);
            subView4.check(false);
        }
    }

    public void setImages(String uri1, String uri2, String uri3, String uri4) {
        subView1.setImage(uri1);
        subView2.setImage(uri2);
        subView3.setImage(uri3);
        subView4.setImage(uri4);
    }
    public void showProgress(int selected, float progress) {
        if (selected == 0) {
            subView1.showProgress(progress);
        } else if (selected == 1) {
            subView2.showProgress(progress);
        } else if (selected == 2) {
            subView3.showProgress(progress);
        } else if (selected == 3) {
            subView4.showProgress(progress);
        }
    }
    public void showResult(int selected, boolean result) {
        if (selected == 0) {
            subView1.showResult(result);
        } else if (selected == 1) {
            subView2.showResult(result);
        } else if (selected == 2) {
            subView3.showResult(result);
        } else if (selected == 3) {
            subView4.showResult(result);
        }
    }

    public static interface OnSelectedListener {
        public void onSelected(int selected);
    }
}

package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.util.FontCache;

public class Quiz1ResultView extends View {

    private float width = 600f;
    private float height = 700f;
    private float imgtop = 175f;
    private float imgleft = 200f;
    private float imgbottom = 300f;
    private float textAreaLeft1 = 267;
    private float textAreaLeft2 = 328;
    private float textAreaTop = 536;
    private float textAreaCenter = 299;
    private float textAreaHeight = 40.0f;
    private int fullPointColor = Color.parseColor("#FFFFFFFF");
    private int realPointColor = Color.parseColor("#FFFFFFFF");
    private Typeface tf = null;

    private double fullPoint = 10;
    private double realPoint = 9;

    public Quiz1ResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.quiz_great_bg);
        setClickable(true);

        if (isInEditMode()) {
            return;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (int) (width * 1.0f * (float)this.height/(float)this.width);
        setMeasuredDimension(width, height);
    }
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float CX = getWidth();
        float CY = getHeight();
        float textAreaHeight = this.textAreaHeight * CY / this.height;
        float textAreaTop = this.textAreaTop * CY / this.height;
        float textAreaLeft1 = this.textAreaLeft1 * CX / this.width;
        float textAreaLeft2 = this.textAreaLeft2 * CX / this.width;
        float slashAreaLeft = this.textAreaCenter * CX / this.width;
        drawText(canvas, String.format("%d", (int)this.realPoint),
                textAreaLeft1,
                textAreaTop,
                textAreaHeight,
                this.realPointColor);
        drawText(canvas, "/",
                slashAreaLeft,
                textAreaTop,
                textAreaHeight,
                this.realPointColor);
        drawText(canvas, String.format("%d", (int)this.fullPoint),
                textAreaLeft2,
                textAreaTop,
                textAreaHeight,
                this.fullPointColor);
    }
    public void setPoint(double full, double real) {
        this.fullPoint = full;
        this.realPoint = real;
        if (real >= 0 && real < 5) {
            setBackgroundResource(R.drawable.quiz_keep_trying_bg);
        } else if (real >= 5 && real < 9) {
            setBackgroundResource(R.drawable.quiz_good_bg);
        } else if (real >= 9) {
            setBackgroundResource(R.drawable.quiz_great_bg);
        } else {
            setBackgroundResource(R.drawable.quiz_keep_trying_bg);
        }
        invalidate();
    }

    private void drawText(Canvas canvas, String text, float x, float y, float textAreaHeight, int color) {
        float textHeight = textAreaHeight;//*0.55f;
        Paint paint = new Paint();
        paint.setColor(color);
//        paint.setShadowLayer(3, 0, 0, Color.GRAY);
        paint.setTextSize(textHeight);
        if (tf == null) {
            tf = FontCache.getTypefaceNotoSans_Bold(getContext());
        }
        if (tf != null) {
            paint.setTypeface(tf);
        }
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setAntiAlias(true);
        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);

        canvas.drawText(text, x, y - (paint.descent()+paint.ascent())/2, paint);
    }
}

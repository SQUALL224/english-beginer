package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;

import com.talkenglish.beginner.R;

public class QuizOptionView extends NotoSTextView {

    public QuizOptionView(Context context) {
        super(context);
        setBackgroundResource(R.drawable.ic_answer_bg);

        setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen._12sp));
        setGravity(Gravity.CENTER);
        setSingleLine();
        setTextColor(Color.WHITE);
        setPadding((int)context.getResources().getDimension(R.dimen._4dp), 0, (int)context.getResources().getDimension(R.dimen._4dp), 0);

//        setDrawingCacheEnabled(true);
//        if (isInEditMode()) {
//            return;
//        }
    }
    public QuizOptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.ic_answer_bg);

        setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen._12sp));
        setGravity(Gravity.CENTER);
        setSingleLine();
        setTextColor(Color.WHITE);
        setPadding((int)context.getResources().getDimension(R.dimen._4dp), 0, (int)context.getResources().getDimension(R.dimen._4dp), 0);

        if (isInEditMode()) {
            return;
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}

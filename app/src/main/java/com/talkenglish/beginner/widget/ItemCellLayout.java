package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.uimodel.LevelUI;
import com.talkenglish.beginner.util.SharedPref;

public class ItemCellLayout extends LinearLayout {
    private int ic_drawable_id = R.drawable.ic_beginer;

    private int score = 0;
    private String title = "Fill in the Blanks";
    private String title_prefix = "";

    private ImageView ivIcon;
    private NotoSTextView tvTitle;
    private NotoSBoldTextView tvPoint;
    private ImageView ivCheck;
    private ImageView ivTransparent;

    private Context context;

    public ItemCellLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setFocusable(true);
        setClickable(true);
        setOrientation(VERTICAL);
        setBackgroundResource(R.drawable.clickable_overlay_white_bg);
        init();
        if (isInEditMode()) {
            return;
        }
    }

    void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.main_item_cell, this, true);

        tvTitle = (NotoSTextView) findViewById(R.id.tv_title);
        ivIcon = (ImageView) findViewById(R.id.iv_icon);
        tvPoint = (NotoSBoldTextView) findViewById(R.id.tv_cell_point);
        ivCheck = (ImageView) findViewById(R.id.check);
        ivTransparent = (ImageView) findViewById(R.id.iv_transparent);

        ivCheck.setVisibility(GONE);
        setTitle("");
        setIcon(R.drawable.ic_beginer);
        setScore(0);
    }

    public void setStatus(LevelUI level)
    {
        if(!level.canStudy()){
//            setBackgroundResource(R.drawable.ic_cell_disabled);
            setEnabled(false);
            ivTransparent.setVisibility(VISIBLE);
        }
        else
        {
            setEnabled(true);
            ivTransparent.setVisibility(GONE);

            if(SharedPref.getBoolean(context, level.levelStr, false)){
//                setBackgroundResource(R.drawable.ic_cell_normal);
                ivCheck.setVisibility(VISIBLE);
            }
            else
            {
                setBackgroundResource(R.drawable.home_item_active);
                ivCheck.setVisibility(GONE);

            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setIcon(int id) {
        this.ic_drawable_id = id;
        this.ivIcon.setImageResource(id);
    }

    public void setTitle(String title) {
        this.title = title;
        this.tvTitle.setText(title);
    }

    public void setScore(int score) {
        this.score = score;
        tvPoint.setText(String.format("%02d", score));
    }

    public void setFinished() {
        ivCheck.setVisibility(VISIBLE);
    }
}

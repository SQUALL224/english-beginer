package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.talkenglish.beginner.R;

public class ScrollDisplayerLayout extends LinearLayout {

    private ImageView ivScrollUp = null;
    private ImageView ivScrollDown = null;

    public ScrollDisplayerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.scroll_displayer_layout, this, true);
        ivScrollDown = (ImageView)findViewById(R.id.iv_scroll_down);
        ivScrollUp = (ImageView)findViewById(R.id.iv_scroll_up);
        if (isInEditMode()) {
            return;
        }
    }
    public void setVisivility(int visibility) {
        super.setVisibility(visibility);
    }
    public void show() {
        setVisibility(VISIBLE);
        ivScrollDown.setImageResource(R.drawable.scroll_displayer_down);
        ivScrollUp.setImageResource(R.drawable.scroll_displayer_up);
    }
    public void hide() {
        setVisibility(GONE);
        ivScrollDown.setImageResource(R.drawable.scroll_displayer_down);
        ivScrollUp.setImageResource(R.drawable.scroll_displayer_up);
    }
    public int checkScroller(Rect rect) {
        Rect outRect1 = new Rect();
        Rect outRect2 = new Rect();
        int square1 = 0;
        int square2 = 0;
        int location[] = new int[2];
        ivScrollUp.getDrawingRect(outRect1);
        ivScrollUp.getLocationOnScreen(location);
        outRect1.offset(location[0], location[1]);
        if (outRect1.intersect(rect)) {
            square1 = outRect1.width() * outRect1.height();
        } else {
            square1 = 0;
        }
        ivScrollDown.getDrawingRect(outRect2);
        ivScrollDown.getLocationOnScreen(location);
        outRect2.offset(location[0], location[1]);
        if (outRect2.intersect(rect)) {
            square2 = outRect2.width() * outRect2.height();
        } else {
            square2 = 0;
        }
        if (square1 == 0 && square2 == 0) {
            ivScrollDown.setImageResource(R.drawable.scroll_displayer_down);
            ivScrollUp.setImageResource(R.drawable.scroll_displayer_up);
            return 0;
        } else if (square1 >= square2) {
            ivScrollDown.setImageResource(R.drawable.scroll_displayer_down);
            ivScrollUp.setImageResource(R.drawable.scroll_displayer_up_matched);
            return 1;
        } else if (square1 < square2) {
            ivScrollDown.setImageResource(R.drawable.scroll_displayer_down_matched);
            ivScrollUp.setImageResource(R.drawable.scroll_displayer_up);
            return -1;
        }
        return 0;
    }
}

package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.talkenglish.beginner.util.FontCache;

/**
 * Created by alex on 7/1/2016.
 */
public class NotoSBoldItalicTextView extends TextView {
    public NotoSBoldItalicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NotoSBoldItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NotoSBoldItalicTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = FontCache.getTypefaceNotoSans_BoldItalic(getContext());
        setTypeface(tf);
    }
}

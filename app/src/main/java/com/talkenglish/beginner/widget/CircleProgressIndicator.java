package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;


/**
 * Created by alex
 */
public class CircleProgressIndicator extends View {

    private float mDensity;

    private float progress = 0.7f;

    private int fgColor = Color.parseColor("#ff006ac4");
    private int bgColor = Color.parseColor("#ffaaaaaa");

    public CircleProgressIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public CircleProgressIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CircleProgressIndicator(Context context) {
        super(context);
        init(context);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width;

        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }

    public void init(Context context) {
        mDensity = context.getResources().getDisplayMetrics().density;
        progress = 0f;
    }

    public void setProgress(float progress) {
        this.progress = Math.max(0f, Math.min(1.0f, progress));
        invalidate();
    }

    public float getProgress() {
        return progress;
    }

    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        int length = getWidth() > getHeight() ? getHeight() : getWidth();

        if(width == 0 || height == 0) return;
        float thickness = (float)width/20.0f;
        float radius = length / 2.0f - 1.0f-thickness/2;
        RectF rect = new RectF(getWidth() / 2 - radius, getHeight() / 2 - radius, getWidth() / 2 + radius, getHeight() / 2 + radius);

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(bgColor);
        paint.setStrokeWidth(thickness);
        paint.setStyle(Paint.Style.STROKE);

        Paint paint1 = new Paint();
        paint1.setAntiAlias(true);
        paint1.setColor(fgColor);
        paint1.setStrokeWidth(thickness);
        paint1.setStyle(Paint.Style.STROKE);
        canvas.drawArc(rect, 270, 360, false, paint);
        canvas.drawArc(rect, 270, this.progress*360.0f, false, paint1);

    }


}
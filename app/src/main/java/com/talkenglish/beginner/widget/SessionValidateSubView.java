package com.talkenglish.beginner.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.R;

public class SessionValidateSubView extends RelativeLayout {

    private float progress = 0.50f;
    ImageView mainImage;
    ImageView selectedFrame;
    ImageView correctFrame;
    ImageView incorrectFrame;
    RectangleProgressIndicator indicator;

    public SessionValidateSubView(Context context) {
        super(context);

        initViews();
        if (isInEditMode()) {
            return;
        }
    }
    public SessionValidateSubView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initViews();
        if (isInEditMode()) {
            return;
        }
    }

    private void initViews() {
        setClickable(true);

        mainImage = new ImageView(getContext());
        selectedFrame = new ImageView(getContext());
        correctFrame = new ImageView(getContext());
        incorrectFrame = new ImageView(getContext());
        indicator = new RectangleProgressIndicator(getContext());
        mainImage.setImageResource(R.drawable.placeholder_image);
        selectedFrame.setImageResource(R.drawable.frame_selected_bg);
        correctFrame.setImageResource(R.drawable.frame_correct_case);
        incorrectFrame.setImageResource(R.drawable.frame_incorrect_case);
        mainImage.setScaleType(ImageView.ScaleType.FIT_XY);
        selectedFrame.setScaleType(ImageView.ScaleType.FIT_XY);
        correctFrame.setScaleType(ImageView.ScaleType.FIT_XY);
        incorrectFrame.setScaleType(ImageView.ScaleType.FIT_XY);
        LayoutParams params0 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params0.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        LayoutParams params1 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        LayoutParams params4 = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params4.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params4.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params4.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        addView(mainImage, params0);
        addView(selectedFrame, params1);
        addView(correctFrame, params1);
        addView(incorrectFrame, params1);
        addView(indicator, params4);

        selectedFrame.setVisibility(INVISIBLE);
        correctFrame.setVisibility(INVISIBLE);
        incorrectFrame.setVisibility(INVISIBLE);
    }
    public void check(boolean enable) {
        if (enable) {
            selectedFrame.setVisibility(VISIBLE);
        } else {
            selectedFrame.setVisibility(INVISIBLE);
        }
    }
    public void setImage(String imageUrl) {
        if (imageUrl == null || imageUrl == "") {
            return;
        }
        Picasso.with(getContext())
                .load(imageUrl)
                .placeholder(R.drawable.placeholder_image)
                .into(this.mainImage);
    }
    public void showResult(boolean result) {
        if (result) {
            correctFrame.setVisibility(VISIBLE);
        } else {
            incorrectFrame.setVisibility(VISIBLE);
        }
    }
    public void showProgress(float progress) {
        indicator.setProgress(progress);
    }
    public void init() {
        selectedFrame.setVisibility(INVISIBLE);
        correctFrame.setVisibility(INVISIBLE);
        incorrectFrame.setVisibility(INVISIBLE);
        indicator.setProgress(0);
    }
}

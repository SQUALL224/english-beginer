package com.talkenglish.beginner.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.model.Quiz;

public class QuizSentenceView extends LinearLayout {

    Context context;
    Quiz quiz;
    QuizItemView blankView = null;
    boolean editable = false;
    int screenWidth = 0;
    QuizWordEnteredListener listener;

    public QuizSentenceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setPadding(0,
                (int) context.getResources().getDimension(R.dimen._4dp),
                0,
                0);
        setOrientation(VERTICAL);
        setGravity(Gravity.LEFT);
    }
    public QuizSentenceView(Context context) {
        super(context);
        this.context = context;
        setPadding(0,
                (int) context.getResources().getDimension(R.dimen._4dp),
                0,
                0);
        setOrientation(VERTICAL);
        setGravity(Gravity.LEFT);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

        super.onLayout(changed, left, top, right, bottom);
    }

    void buildLayout() {
        if (this.quiz == null) {
            return;
        }
        LinearLayout llVer = new LinearLayout(context);
        llVer.setGravity(Gravity.LEFT);
        llVer.setOrientation(VERTICAL);
        llVer.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        int maxWidth = this.screenWidth -
                (int) context.getResources().getDimension(R.dimen.lesson_main_image_horizontal_margin) * 4 -
                (int) context.getResources().getDimension(R.dimen._6dp);

        LinearLayout llAlso = new LinearLayout(context);
        llAlso.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                (int)context.getResources().getDimension(R.dimen._23dp)));
        llAlso.setGravity(Gravity.LEFT);
        llAlso.setOrientation(HORIZONTAL);
        llAlso.setPadding((int)context.getResources().getDimension(R.dimen._6dp), 0, 0, 0);

        ImageView ivItem = new ImageView(context);
        ivItem.setImageResource(R.drawable.ic_quiz_item);
        ivItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ivItem.setPadding(0,0,(int)context.getResources().getDimension(R.dimen._6dp),0);
        llAlso.addView(ivItem);

        int widthSoFar = 0;
        for (int i = 0; i < quiz.numOfItems(); i++) {
            final QuizItemView ultvItem = new QuizItemView(context);
            ultvItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen._13sp));
            ultvItem.setTextColor(Color.parseColor("#ffffffff"));

            if (quiz.isBlank(i)) {
                if (this.editable) {
                    ultvItem.etItem.setVisibility(View.VISIBLE);
                    ultvItem.etItem.setText(quiz.getCandidate().trim());
                    ultvItem.etItem.setListener(this.listener);

                    ultvItem.etItem.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            String str = s.toString();
                            if (str.trim().equals("")) {
                                quiz.setCandidate(null);
                            } else {
                                quiz.setCandidate(str);
                            }
                            ultvItem.setText(" " + quiz.getCandidate() + " ");
                        }
                    });
                    ultvItem.etItem.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                            if (event != null) {
                                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                        actionId == EditorInfo.IME_ACTION_NEXT ||
                                        actionId == EditorInfo.IME_ACTION_DONE ||
                                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                                    if (!event.isShiftPressed()) {

                                        Log.e("", "FINISHED Typing : " + textView.getText().toString());
                                        if (listener != null) {
                                            listener.onTextEntered(ultvItem.etItem);
                                        }

                                        return false; // consume.
                                    }
                                }
                            } else {
                                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                                    Log.e("", "Next/Done/Search Pressed");
                                    if (listener != null) {
                                        listener.onTextEntered(ultvItem.etItem);
                                    }
                                    return false;
                                }
                            }
                            return false;
                        }
                    });
                }
                if (quiz.getPoint() == 0) {
                    ultvItem.setStatus(QuizItemView.STATUS_INCORRECT);
                } else if (quiz.getPoint() == 1) {
                    ultvItem.setStatus(QuizItemView.STATUS_CORRECT);
                } else {
                    ultvItem.setStatus(QuizItemView.STATUS_NORMAL);
                }
                ultvItem.setText(" " + quiz.getCandidate() + " ");
                blankView = ultvItem;
            } else {
                ultvItem.setStatus(QuizItemView.STATUS_NOTHING);
                ultvItem.setText(quiz.itemAt(i).toString());
            }
            ultvItem.measure(0, 0);
            widthSoFar += ultvItem.getMeasuredWidth();

            QuizItemView tvItemSpace = new QuizItemView(context);
            tvItemSpace.setStatus(QuizItemView.STATUS_NOTHING);
            tvItemSpace.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen._13sp));
            if (i == quiz.numOfItems() - 1) {

            } else {
                if (quiz.itemAt(i).isSpaceRight() && quiz.itemAt(i+1).isSpaceLeft()) {
                    tvItemSpace.setText(" ");
                    tvItemSpace.setTextColor(Color.parseColor("#ffffffff"));
                    tvItemSpace.measure(0, 0);
                    widthSoFar += tvItemSpace.getMeasuredWidth();
                }
            }

            ultvItem.setTag(i);
            if (widthSoFar >= maxWidth) {
                llVer.addView(llAlso);
                llAlso = new LinearLayout(context);
                LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        (int)context.getResources().getDimension(R.dimen._23dp));
                llAlso.setPadding((int)context.getResources().getDimension(R.dimen._6dp), 0, 0, 0);
                params.setMargins(0, 0, 0, 0);
                llAlso.setLayoutParams(params);
                llAlso.setOrientation(HORIZONTAL);
                llAlso.setGravity(Gravity.LEFT);

                llAlso.addView(ultvItem,
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));


                widthSoFar = ultvItem.getMeasuredWidth();
                if (i == quiz.numOfItems() - 1) {

                } else {
                    llAlso.addView(tvItemSpace,
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    widthSoFar += tvItemSpace.getMeasuredWidth();
                }
            } else {
                llAlso.addView(ultvItem,
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                if (i == quiz.numOfItems() - 1) {

                } else {
                    llAlso.addView(tvItemSpace,
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                }
            }
        }
        llVer.addView(llAlso);

        removeAllViews();
        addView(llVer);
    }
    public Quiz getQuiz() {
        return this.quiz;
    }
    public void checkResult() {
        this.quiz.check();
        if (quiz.getPoint() == 0) {
            blankView.setStatus(QuizItemView.STATUS_INCORRECT);
        } else if (quiz.getPoint() == 1) {
            blankView.setStatus(QuizItemView.STATUS_CORRECT);
        } else {
            blankView.setStatus(QuizItemView.STATUS_NORMAL);
        }
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    public void setEntry(int screenWidth, final Quiz quiz) {
        if (quiz == null) return;
        this.quiz = quiz;
        this.screenWidth = screenWidth;
        if (this.screenWidth == 0) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                @Override
                public void onGlobalLayout() {

                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    QuizSentenceView.this.screenWidth = getMeasuredWidth();
                    buildLayout();
                }
            });
        } else {
            buildLayout();
        }
    }
    public void refresh(int screenWidth) {
        this.screenWidth = screenWidth;
        if (this.screenWidth == 0) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                @Override
                public void onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    QuizSentenceView.this.screenWidth = getMeasuredWidth();
                    buildLayout();
                }
            });
        } else {
            buildLayout();
        }
    }

    public void setQuizWordEnteredListener(QuizSentenceView.QuizWordEnteredListener listener) {
        this.listener = listener;
    }
    public static interface QuizWordEnteredListener {
        public void onTextEntered(EditText editText);
    }
}

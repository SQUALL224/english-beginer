package com.talkenglish.beginner.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class AspectRatioVideoView extends VideoView {
    private float width = 1280;
    private float height = 720;

    public AspectRatioVideoView(Context context) {
        this(context, null);
    }

    public AspectRatioVideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AspectRatioVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        if ((float) width / (float) height > this.width / this.height) {
            width = (int) (height * 1.0f * (float) this.width/ (float) this.height);
        } else {
            height = (int) (width * 1.0f * (float) this.height / (float) this.width);
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }

    /**
     * Source code originally from:
     * http://clseto.mysinablog.com/index.php?op=ViewArticle&articleId=2992625
     *
     * @param videoWidth
     * @param videoHeight
     */
    public void setVideoSize(int videoWidth, int videoHeight) {
        // Set the new video size
        width = videoWidth;
        height = videoHeight;

        /**
         * If this isn't set the video is stretched across the
         * SurfaceHolders display surface (i.e. the SurfaceHolder
         * as the same size and the video is drawn to fit this
         * display area). We want the size to be the video size
         * and allow the aspectratio to handle how the surface is shown
         */
        getHolder().setFixedSize(videoWidth, videoHeight);

        requestLayout();
        invalidate();
    }
}

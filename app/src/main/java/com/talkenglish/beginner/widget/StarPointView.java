package com.talkenglish.beginner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.util.FontCache;

public class StarPointView extends View {

    private float width = 60.0f;
    private float height = 12.0f;
    private float textAreaLeft1 = 20.0f;
    private float textAreaLeft2 = 40.0f;
    private float textAreaTop = 1.0f;
    private float textAreaHeight = 10.0f;
    private float slashAreaLeft = 35.0f;
    private int fullPointColor = Color.parseColor("#FFFFFF");
    private int realPointColor = Color.parseColor("#EEEEEE");
    private Typeface tf = null;

    private double fullPoint = 0;
    private double realPoint = 0;

    public StarPointView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.star_point_bg_normal);
        setClickable(true);

        if (isInEditMode()) {
            return;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (int) (width * 1.0f * (float)this.height/(float)this.width);
        setMeasuredDimension(width, height);
    }
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float CX = getWidth();
        float CY = getHeight();
        float textAreaHeight = this.textAreaHeight * CY / this.height;
        float textAreaTop = this.textAreaTop * CY / this.height;
        float textAreaLeft1 = this.textAreaLeft1 * CX / this.width;
        float textAreaLeft2 = this.textAreaLeft2 * CX / this.width;
        float slashAreaLeft = this.slashAreaLeft * CX / this.width;
        drawText(canvas, String.format("%02d", (int)this.realPoint),
                textAreaLeft1,
                textAreaTop + textAreaHeight / 2,
                textAreaHeight, this.realPointColor, true);
        drawText(canvas, "/",
                slashAreaLeft,
                textAreaTop + textAreaHeight / 2,
                textAreaHeight, this.fullPointColor, false);
        drawText(canvas, String.format("%02d", (int)this.fullPoint),
                textAreaLeft2,
                textAreaTop + textAreaHeight / 2,
                textAreaHeight, this.fullPointColor, false);
    }
    public void setPoint(double full, double real) {
        this.fullPoint = full;
        this.realPoint = real;
        invalidate();
    }

    private void drawText(Canvas canvas, String text, float x, float y, float textAreaHeight, int color, boolean isBold) {
        float textHeight = textAreaHeight;//*0.55f;
        Paint paint = new Paint();
        paint.setColor(color);
//        paint.setShadowLayer(3, 0, 0, Color.GRAY);
        paint.setTextSize(textHeight);
        if(isBold)
        {
            tf = FontCache.getTypefaceNotoSans_Bold(getContext());
        }
        else
        {
            tf = FontCache.getTypefaceNotoSans_Regular(getContext());
        }
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setAntiAlias(true);
        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);

        canvas.drawText(text, x, y - 1 - (paint.descent()+paint.ascent())/2, paint);
    }
}

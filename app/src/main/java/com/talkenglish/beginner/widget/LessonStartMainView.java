package com.talkenglish.beginner.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.talkenglish.beginner.R;

public class LessonStartMainView extends RelativeLayout implements View.OnClickListener {

    protected float width = 992.0f;
    protected float height = 800;
    protected float imageLeft = 147.5f;
    protected float imageRight = imageLeft;
    protected float imageTop = 80.5f;
    protected float imageBottom = 80.5f;
    protected float imageWidth = 698.0f;
    protected float imageHeight = imageWidth;

    protected float playWidth = 175.0f;
    protected float playHeight = 175.0f;
    protected float playLeft = 408.5f;
    protected float playRight = playLeft;
    protected float playTop = 320.0f;
    protected float playBottom = 320.0f;

    protected float indicatorW = playWidth+2;
    protected float indicatorH = indicatorW;
    protected float indicatorT = playTop-1;
    protected float indicatorL = playLeft-1;
    protected float indicatorR = indicatorL;
    protected float indicatorB = playBottom-1;

    protected ImageView ivMain;
    protected ImageView ivPlay;
    protected CircleProgressIndicator indicator;

    protected boolean enable = true;
    protected OnPlayClickListener listener = null;

    public LessonStartMainView(Context context, AttributeSet attrs) {
        super(context, attrs);

//        setBackgroundResource(R.drawable.lesson_start_bg);
        init();
        if (isInEditMode()) {
            return;
        }
    }

    void init() {
        ivMain = new ImageView(getContext());
        ivMain.setScaleType(ImageView.ScaleType.FIT_XY);
//        ivMain.setImageResource(R.drawable.l01);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(ivMain, params);

        ivPlay = new ImageView(getContext());
        ivPlay.setImageResource(R.drawable.ic_play_button);
        ivPlay.setClickable(true);
        RelativeLayout.LayoutParams params0 = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params0.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params0.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(ivPlay, params0);

        indicator = new CircleProgressIndicator(getContext());
        indicator.setClickable(false);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        addView(indicator, params1);
        indicator.setVisibility(INVISIBLE);
        ivPlay.setOnClickListener(this);
    }

    protected void resizeChildViews(int width, int height) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)ivMain.getLayoutParams();

        params.leftMargin = 0; //(int) (this.imageLeft * width / this.width);
        params.rightMargin = 0; //(int) (this.imageLeft * width / this.width);
        params.topMargin = (int) (this.imageTop * height / this.height);
        params.bottomMargin = (int) (this.imageBottom * height / this.height);
        ivMain.setLayoutParams(params);

        RelativeLayout.LayoutParams params0 = (RelativeLayout.LayoutParams)ivPlay.getLayoutParams();
        params0.leftMargin = (int) (this.playLeft * width / this.width);
        params0.rightMargin = (int) (this.playLeft * width / this.width);
        params0.topMargin = (int) (this.playTop * height / this.height);
        params0.bottomMargin = (int) (this.playBottom * height / this.height);
        ivPlay.setLayoutParams(params0);

        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)indicator.getLayoutParams();
        params1.leftMargin = (int) (this.indicatorL * width / this.width);
        params1.rightMargin = (int) (this.indicatorL * width / this.width);
        params1.topMargin = (int) (this.indicatorT * height / this.height);
        params1.bottomMargin = (int) (this.indicatorB * height / this.height);
        indicator.setLayoutParams(params1);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {

        resizeChildViews(w, h);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }
    @Override
    public void onClick(View v) {
        showProgressIndicator();
        setProgress(0);
        if (this.listener != null) {
            this.listener.onClick(v);
        }
    }
    public void setImage(String imageUrl) {
        if (imageUrl == null || imageUrl == "") {
            return;
        }
        Picasso.with(getContext())
                .load(imageUrl)
                .into(this.ivMain);
    }
    public void showProgressIndicator() {
        this.indicator.setVisibility(View.VISIBLE);
    }
    public void hideProgressIndicator() {
        this.indicator.setVisibility(View.INVISIBLE);
    }
    public void setProgress(float progress) {
        this.indicator.setProgress(progress);
    }
    public void enablePlayButton(boolean enable) {
        this.enable = enable;
        if (this.enable) {
            this.ivPlay.setVisibility(View.VISIBLE);
        } else {
            this.ivPlay.setVisibility(View.INVISIBLE);
        }
    }
    public void setOnPlayClickListener(OnPlayClickListener listener) {
        if (listener == null) {
            return;
        }
        this.listener = listener;
    }
    public static interface OnPlayClickListener {
        public void onClick(View v);
    }
}

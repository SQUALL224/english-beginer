package com.talkenglish.beginner.widget;

import android.content.Context;
import android.util.AttributeSet;

public class LessonStartMainLView extends LessonStartMainView {

    public LessonStartMainLView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = (int) (height * 1.0f * (float) this.width / (float) this.height);
        resizeChildViews(width, height);
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }

}

package com.talkenglish.beginner.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * Created by alex on 9/9/2016.
 */
public class QuizEditText extends EditText {

    QuizSentenceView.QuizWordEnteredListener listener;
    public QuizEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public QuizEditText(Context context) {
        super(context);
    }
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            dispatchKeyEvent(event);
            if (listener != null) {
                listener.onTextEntered(this);
            }
            return super.onKeyPreIme(keyCode, event);
        }
        return super.onKeyPreIme(keyCode, event);
    }
    public void setListener(QuizSentenceView.QuizWordEnteredListener listener) {
        this.listener = listener;
    }
}

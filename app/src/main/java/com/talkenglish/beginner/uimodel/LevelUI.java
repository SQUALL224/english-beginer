package com.talkenglish.beginner.uimodel;

import android.content.Context;

import com.talkenglish.beginner.R;
import com.talkenglish.beginner.util.SharedPref;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 7/3/2016.
 */
public class LevelUI {
    public static final String EMPTY = "EMPTY";
    public static final String NORMAL = "NORMAL";

    private int level;
    private String title;
    private boolean enable;
    private Context context;
    public String levelStr;

    public LevelUI(Context context, int level, boolean enable, String title, String preferences) {
        this.context = context;
        this.level = level;
        this.title = title;
        this.enable = enable;
        this.levelStr = preferences;
    }

    public int getLevel() {
        return this.level;
    }
    public String getTitle() {
        return this.title;
    }
    public boolean canStudy() {
        return this.enable;
    }

//    @Override
//    public String toString() {
//        if (this.buttonDrawableId == 0) {
//            return EMPTY;
//        } else {
//            return NORMAL;
//        }
//    }

    public static LevelUI getLevel(Context context, int level) {
        if (level <= 0 || level >= 10) {
            return null;
        }
        if (level == 1) {
            return (new LevelUI(context, 1, true, "Very Berry Basic", "COMPLETED_10"));
        } else if (level == 2) {
            return (new LevelUI(context, 2, SharedPref.getBoolean(context, "COMPLETED_10", false),
                    "Easy Peasy", "COMPLETED_20"));
        } else if (level == 3) {
            return (new LevelUI(context, 3, SharedPref.getBoolean(context, "COMPLETED_20", false),
                    "Simply Cool", "COMPLETED_30"));
        } else if (level == 4) {
            return (new LevelUI(context, 4, SharedPref.getBoolean(context, "COMPLETED_30", false),
                    "Medi-ogre", "COMPLETED_40"));
        } else if (level == 5) {
            return (new LevelUI(context, 5, SharedPref.getBoolean(context, "COMPLETED_40", false),
                    "Middle Fiddle", "COMPLETED_50"));
        } else if (level == 6) {
            return (new LevelUI(context, 6, SharedPref.getBoolean(context, "COMPLETED_50", false),
                    "Mediummy", "COMPLETED_60"));
        } else if (level == 7) {
            return (new LevelUI(context, 7, SharedPref.getBoolean(context, "COMPLETED_60", false),
                    "Hardly Hollow", "COMPLETED_70"));
        } else if (level == 8) {
            return (new LevelUI(context, 8, SharedPref.getBoolean(context, "COMPLETED_70", false),
                    "Diffi-colt", "COMPLETED_80"));
        } else if (level == 9) {
            return (new LevelUI(context, 9, SharedPref.getBoolean(context, "COMPLETED_80", false),
                    "Finally Advanced", "COMPLETED_90"));
        }
        return null;
    }
    public static List<LevelUI> getLevels(Context context) {
        List<LevelUI> levelUIs = new ArrayList<>();
        levelUIs.add(new LevelUI(context, 0, false,"", ""));
        levelUIs.add(new LevelUI(context, 1, true, "Very Berry Basic",  "COMPLETED_10"));
        levelUIs.add(new LevelUI(context, 2, SharedPref.getBoolean(context, "COMPLETED_10", false),
                "Easy Peasy", "COMPLETED_20"));
        levelUIs.add(new LevelUI(context, 3, SharedPref.getBoolean(context, "COMPLETED_20", false),
                "Simply Cool",  "COMPLETED_30"));
        levelUIs.add(new LevelUI(context, 4, SharedPref.getBoolean(context, "COMPLETED_30", false),
                "Medi-ogre",  "COMPLETED_40"));
        levelUIs.add(new LevelUI(context, 5, SharedPref.getBoolean(context, "COMPLETED_40", false),
                "Middle Fiddle",  "COMPLETED_50"));
        levelUIs.add(new LevelUI(context, 6, SharedPref.getBoolean(context, "COMPLETED_50", false),
                "Mediummy",  "COMPLETED_60"));
        levelUIs.add(new LevelUI(context, 7, SharedPref.getBoolean(context, "COMPLETED_60", false),
                "Hardly Hollow",  "COMPLETED_70"));
        levelUIs.add(new LevelUI(context, 8, SharedPref.getBoolean(context, "COMPLETED_70", false),
                "Diffi-colt",  "COMPLETED_80"));
        levelUIs.add(new LevelUI(context, 9, SharedPref.getBoolean(context, "COMPLETED_80", false),
                "Finally Advanced",  "COMPLETED_90"));
        levelUIs.add(new LevelUI(context, 0, false,"", ""));
        return levelUIs;
    }
}
